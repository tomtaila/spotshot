alter table SHOT add DELETED BOOLEAN;
create table FRIEND;
alter table FRIEND add UUID STRING;
alter table FRIEND add USER_UUID STRING;
alter table FRIEND add USER_NAME STRING;
alter table FRIEND add FRIEND_UUID STRING;
alter table FRIEND add FRIEND_USER_NAME STRING;