package tmtm.SpotShot.model;

import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ttaila on 4/20/15.
 */
public class Friend extends SugarRecord<Friend> implements Comparable<Friend> {

    private String uuid;
    private String userUuid;
    private String userUserName;
    private String friendUuid;
    private String friendUserName;
    private boolean request;

    public Friend(){}

    public Friend(String uuid, String userUuid, String userUserName, String friendUuid, String friendUserName, boolean request) {
        this.uuid = uuid;
        this.userUuid = userUuid;
        this.userUserName = userUserName;
        this.friendUuid = friendUuid;
        this.friendUserName = friendUserName;
        this.request = request;
    }

    public void setFriendUuid(String friendUuid) {
        this.friendUuid = friendUuid;
    }

    public String getFriendUserName() {
        return friendUserName;
    }

    public void setFriendUserName(String friendUserName) {
        this.friendUserName = friendUserName;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getUserUserName() {
        return userUserName;
    }

    public void setUserUserName(String userUserName) {
        this.userUserName = userUserName;
    }

    public String getFriendUuid() {
        return friendUuid;
    }

    public boolean isRequest() {
        return request;
    }

    public void setRequest(boolean request) {
        this.request = request;
    }

    @Override
    public int compareTo(Friend another) {
        if(this.request && !another.request) {
            return 1;
        } else {
            return this.friendUserName.compareToIgnoreCase(another.friendUserName);
        }
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if(obj instanceof Friend) {
            Friend other = (Friend) obj;
            result = this.getUuid().equals(other.getUuid());
        }
        return result;
    }

    public static Friend buildFromJson(JSONObject friendJson) {
        Friend friend = null;
        try {
            friend = new Friend(friendJson.getString("uuid"), friendJson.getString("user_uuid"), friendJson.getString("user_name"), friendJson.getString("friend_uuid"), friendJson.getString("friend_name"), friendJson.getBoolean("request"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return friend;
    }

}
