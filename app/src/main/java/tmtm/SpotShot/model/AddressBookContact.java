package tmtm.SpotShot.model;

import com.orm.SugarRecord;

/**
 * Created by mwszedybyl on 8/12/15.
 */
public class AddressBookContact extends SugarRecord<Friend> implements Comparable<Friend>  {
    String myName = "";
    String myNumber = "";

    public String getName() {
        return myName;
    }

    public void setName(String name) {
        myName = name;
    }

    public String getPhoneNum() {
        return myNumber;
    }

    public void setPhoneNum(String number) {
        myNumber = number;
    }

    @Override
    public int compareTo(Friend another)
    {
        return 0;
    }
}