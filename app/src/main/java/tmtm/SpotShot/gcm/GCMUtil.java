package tmtm.SpotShot.gcm;

import android.app.Activity;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;

import tmtm.SpotShot.Settings;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.common.AppInfoUtil;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 8/1/15.
 */
public class GCMUtil {

    private static GCMClientManager pushClientManager;
    private static final String PROJECT_NUMBER = "1029352445541";

    public static void registerOrUpdateGCM(final Activity activity, final User user) {
        // GCM
        pushClientManager = new GCMClientManager(activity, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                // SEND async device registration to your back-end server
                // linking user with device registration id
                // POST https://my-back-end.com/devices/register?user_id=123&device_id=abc&platform=android&app_version=10
                RequestParams params = new RequestParams();
                params.put("reg_uuid", registrationId);
                params.put("user_uuid", user.getUuid());
                params.put("app_version", AppInfoUtil.getAppVersion(SpotShotApplication.getInstance()));
                params.put("platform", "android");
                SpotShotRestClient.post(URLHelper.REGISTER_DEVICE_URL, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        SpotShotApplication.getSharedPreferences().edit().putBoolean(Settings.IGNORE_PUSH_NOTIFICATIONS_KEY, false).commit();
                        // If the response is JSONObject
                    }
                });
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
                // If there is an error registering, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off when retrying.
            }
        });
    }

}
