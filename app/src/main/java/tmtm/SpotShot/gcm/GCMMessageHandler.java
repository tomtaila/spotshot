package tmtm.SpotShot.gcm;

import com.google.android.gms.gcm.GcmListenerService;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import tmtm.SpotShot.NotificationHelper;
import tmtm.SpotShot.Settings;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.ViewPagerActivity;
import tmtm.SpotShot.activities.chat.ChatActivity;
import tmtm.SpotShot.activities.chat.ChatFragment;
import tmtm.SpotShot.activities.friends.AcceptFriendsActivity;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Friend;
import tmtm.SpotShot.model.Message;

public class GCMMessageHandler extends GcmListenerService {
    public static final int FRIEND_REQUEST_RESPONSE_CODE = 0;
    public static final int FRIEND_ACCEPTED_RESPONSE_CODE = 1;
    public static final int FRIEND_DELETED_RESPONSE_CODE = 2;
    public static final int FRIEND_CANCELLED_RESPONSE_CODE = 3;
    public static final int MESSAGE_RECEIVED_RESPONSE_CODE = 4;
    public static final int MESSAGES_VIEWED_RESPONSE_CODE = 5;

    public static final String TAG = "GCMMessageHandler";

    public static final String ACTION = "tmtm.SpotShot.gcm.GCMMessageHandler";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        if(!SpotShotApplication.getSharedPreferences().getBoolean(Settings.IGNORE_PUSH_NOTIFICATIONS_KEY, true)) {
            JSONObject payload;
            try {
                payload = new JSONObject(data.getString("payload"));
                switch (payload.getInt("response_code")) {
                    case FRIEND_REQUEST_RESPONSE_CODE:
                        handleFriendRequest(data);
                        break;
                    case FRIEND_ACCEPTED_RESPONSE_CODE:
                        handleAcceptFriend(data);
                        break;
                    case FRIEND_DELETED_RESPONSE_CODE:
                        handleDeleteFriend(data);
                        break;
                    case FRIEND_CANCELLED_RESPONSE_CODE:
                        handleCancelledRequest(data);
                        break;
                    case MESSAGE_RECEIVED_RESPONSE_CODE:
                        handleMessageReceived(data);
                        break;
                    case MESSAGES_VIEWED_RESPONSE_CODE:
                        handleMessageViewed(data);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void fireBroadcast(Bundle data) {
        // Construct an Intent tying it to the ACTION (arbitrary event namespace)
        Intent in = new Intent(ACTION);
        // Put extras into the intent as usual
        in.putExtra("data", data);
        // Fire the broadcast with intent packaged
        LocalBroadcastManager.getInstance(this).sendBroadcast(in);
        // or sendBroadcast(in) for a normal broadcast;
    }

    // Creates notification based on title and body received
    private void createNotification(String title, String body, Intent notificationIntent, Class<?> notificationIntentClass) {
        NotificationHelper.notify(title, body, NotificationHelper.MESSAGE_NOTIFICATION_ID, notificationIntent, notificationIntentClass);

        // Vibrate for 500 milliseconds
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
    }


    /** This should handle the message sent to a user when someone has accepted their friend request */
    private void handleAcceptFriend(final Bundle data) {
        Log.d(TAG, "handleAcceptFriend() start");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    // Update the friend request in the local db to have request = false
                    JSONObject payload = new JSONObject(data.getString("payload"));
                    JSONObject friendJson = new JSONObject(payload.getString("friend"));
                    Friend friend = Friend.buildFromJson(friendJson);
                    DbHelper.commitOrUpdateFriend(friend);

                    // Create a Chat record in the local db to reflect the one server side
                    JSONObject chatJson = new JSONObject(payload.getString("chat"));
                    Chat chat = Chat.buildFromJson(chatJson);
                    DbHelper.commitOrUpdateChat(chat);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void param) {
                Intent notificationIntent = new Intent(SpotShotApplication.getInstance(), ViewPagerActivity.class);
                createNotification(data.getString("title"), data.getString("body"), notificationIntent, ViewPagerActivity.class);
                ViewPagerActivity.currentFragment = ViewPagerActivity.USER_FRAGMENT_POS;
                fireBroadcast(data);
                Log.d(TAG, "handleAcceptFriend() end");
            }
        }.execute();
    }

    /** This should handle the message sent to a user when someone has cancelled the friend request they sent them*/
    private void handleCancelledRequest(final Bundle data) {
        Log.d(TAG, "handleCancelledFriend() start");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    JSONObject payload = new JSONObject(data.getString("payload"));
                    JSONObject friendToBeDeletedJson = new JSONObject(payload.getString("friend"));
                    Friend friendToBeDeleted = Friend.buildFromJson(friendToBeDeletedJson);
                    DbHelper.removeFriend(friendToBeDeleted);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void param) {
                fireBroadcast(data);
                Log.d(TAG, "handleCancelledFriend() end");
            }
        }.execute();
    }

    /** This should handle the message sent to a user when someone has deleted the user as a friend*/
    private void handleDeleteFriend(final Bundle data) {
        Log.d(TAG, "handleDeleteFriend() start");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    JSONObject payload = new JSONObject(data.getString("payload"));
                    JSONObject friendToBeDeletedJson = new JSONObject(payload.getString("friend_two"));
                    Friend friendToBeDeleted = Friend.buildFromJson(friendToBeDeletedJson);
                    DbHelper.removeFriend(friendToBeDeleted);

                    JSONObject chatToBeDeletedJson = new JSONObject(payload.getString("chat"));
                    Chat chatToBeDeleted = Chat.buildFromJson(chatToBeDeletedJson);
                    DbHelper.removeChat(chatToBeDeleted);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void param) {
                fireBroadcast(data);
                Log.d(TAG, "handleDeleteFriend() end");
            }
        }.execute();
    }

    /** This should handle the message sent to a user when someone has sent them a friend request*/
    private void handleFriendRequest(final Bundle data) {
        Log.d(TAG, "handleFriendRequest() start...");

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    // store the friend request in the local db
                    JSONObject response = new JSONObject(data.getString("payload"));
                    JSONObject friendJson = response.getJSONObject("friend");
                    DbHelper.commitOrUpdateFriend(Friend.buildFromJson(friendJson));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void param) {
                Intent notificationIntent = new Intent(SpotShotApplication.getInstance(), AcceptFriendsActivity.class);
                createNotification(data.getString("title"), data.getString("body"), notificationIntent, AcceptFriendsActivity.class);
                fireBroadcast(data);
                Log.d(TAG, "handleFriendRequest() end...");
            }
        }.execute();
    }

    /** This should handle the message sent to a user when someone has sent them a message*/
    private void handleMessageReceived(final Bundle data) {
        Log.d(TAG, "handleMessageReceived() start...");
        new AsyncTask<Void, Void, Void>() {
            private Chat chat;

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    // store the friend request in the local db
                    JSONObject response = new JSONObject(data.getString("payload"));
                    JSONObject messageJson = response.getJSONObject("message");
                    Message message = Message.buildFromJson(messageJson);
                    DbHelper.commitOrUpdateMessage(Message.buildFromJson(messageJson));
                    chat = DbHelper.getChat(message.getChatUuid());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void param) {
                if(chat != null) {
                    if(!ChatFragment.onScreen) {
                        Intent notificationIntent = new Intent(SpotShotApplication.getInstance(), ChatActivity.class);
                        notificationIntent.putExtra("chat", chat);
                        createNotification(data.getString("title"), data.getString("body"), notificationIntent, ChatActivity.class);
                    }
                    fireBroadcast(data);
                    Log.d(TAG, "handleMessageReceived() end...");
                }
            }
        }.execute();
    }

    private void handleMessageViewed(final Bundle data) {
        Log.d(TAG, "handleMessageViewed() start...");
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    // store the friend request in the local db
                    JSONObject response = new JSONObject(data.getString("payload"));
                    String chatUuid = response.getString("chat_uuid");
                    Chat chat = DbHelper.getChat(chatUuid);
                    DbHelper.markSentMessagesAsViewed(chat);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void param) {
                fireBroadcast(data);
                Log.d(TAG, "handleMessageViewed() end...");
            }
        }.execute();
    }

}