package tmtm.SpotShot.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import tmtm.SpotShot.R;

/**
 * Created by ttaila on 6/11/15.
 */
public class FillingRing extends View {

    private int color;
    private float strokeWidth;
    private Paint paint;
    private float progressPercentage = 0f;
    private RectF containingRect;

    public FillingRing(Context context) {
        super(context);
    }

    public FillingRing(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FillingRing,
                0, 0);

        try {
            color = a.getColor(R.styleable.FillingRing_filling_ring_color, Color.WHITE);
            strokeWidth = a.getDimension(R.styleable.FillingRing_filling_ring_stroke_width, 1.0f);
            init();
        } finally {
            a.recycle();
        }
    }

    public FillingRing(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the ring
        canvas.drawArc(containingRect, 270, ((3.6f)*progressPercentage), false, paint);
    }

    // Should be set to a value between 0 and 100 (inclusive) i.e. 0 <= progressPercentage <= 100
    public void setProgressPercentage(float progressPercentage) {
        if(progressPercentage < 0) {
            progressPercentage = 0;
        } else if(progressPercentage > 100) {
            progressPercentage = 100;
        }

        this.progressPercentage = progressPercentage;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = Math.min(heightSize, widthSize);
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(heightSize, widthSize);
        } else {
            //Be whatever you want
            width = Math.min(heightSize, widthSize);
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = Math.min(heightSize, widthSize);
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(heightSize, widthSize);
        } else {
            //Be whatever you want
            height = Math.min(heightSize, widthSize);
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
        if(containingRect == null) {
            containingRect = new RectF(0+(strokeWidth/2), 0+(strokeWidth/2), width-(strokeWidth/2), height-(strokeWidth/2));
        }
    }


}
