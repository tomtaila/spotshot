package tmtm.SpotShot;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

/**
 * Created by ttaila on 7/19/15.
 */
public class NotificationHelper {

    public static final int MESSAGE_NOTIFICATION_ID = 3133426;
    public static final int UPLOADING_SHOT_NOTIFICATION_ID = 3133427;

    public static void notify(String title, String body, int notificationId, Intent intent, Class<?> intentClass) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(SpotShotApplication.getInstance())
                        .setSmallIcon(R.drawable.logo_32)
                        .setTicker(title)
                        .setContentTitle(title)
                        .setContentText(body);

        if(intent != null && intentClass != null) {
            // The stack builder object will contain an artificial back stack for the started Activity.
            // This ensures that navigating backward from the Activity leads out of your application to the Home screen.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(SpotShotApplication.getInstance());
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(intentClass);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(intent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
        }

        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) SpotShotApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        // Builds the notification and issues it.
        mNotifyMgr.notify(notificationId, notification);
    }

}
