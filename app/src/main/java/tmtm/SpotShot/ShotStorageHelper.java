package tmtm.SpotShot;

import android.content.SharedPreferences;

import java.io.File;

import tmtm.SpotShot.model.Shot;

/**
 * Created by mwszedybyl on 8/2/15.
 */
public class ShotStorageHelper
{
    private static SharedPreferences.Editor editor;
    private static SharedPreferences prefs;

    public static void storeShotMedia(Shot shot)
    {
        editor = SpotShotApplication.getInstance().getSharedPreferences().edit();
        prefs = SpotShotApplication.getInstance().getSharedPreferences();

        for (int i = 0; i < 10; i++)
        {
            String shotUrl = prefs.getString("shot" + i, null);
            if (shotUrl == null)
            {
                editor.putString("shot" + i, shot.getUrl());
                editor.putInt(Settings.SHOT_POSITION, i);
                editor.commit();

                break;
            }
        }

        int lastPosition = prefs.getInt(Settings.SHOT_POSITION, 0);
        if(lastPosition == 9){
            String shotUrl = prefs.getString("shot"+0, "");
            deleteMediaShot(shotUrl);
            editor.putString("shot" + 0, shot.getUrl());
            editor.putInt(Settings.SHOT_POSITION, 0);
        } else {
            String shotUrl = prefs.getString("shot"+lastPosition, "");
            deleteMediaShot(shotUrl);
            lastPosition++;
            editor.putString("shot" + lastPosition, shot.getUrl());
            editor.putInt(Settings.SHOT_POSITION, lastPosition);
        }
    }

    public static void deleteMediaShot(String shotUrl){
        File fdelete = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, shotUrl);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted");
            } else {
                System.out.println("file not Deleted");
            }
        }
    }






}
