package tmtm.SpotShot.analytics;

/**
 * Created by ttaila on 7/21/15.
 */
public class AnalyticsUtil {

    private static AnalyticsUtil instance;

    private AnalyticsUtil() {
        //
    }

    public static AnalyticsUtil getInstance() {
        if(instance == null) {
            instance = new AnalyticsUtil();
        }
        return instance;
    }

}
