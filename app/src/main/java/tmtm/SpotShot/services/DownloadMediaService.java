package tmtm.SpotShot.services;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;

import com.amazonaws.services.s3.model.S3ObjectInputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import tmtm.SpotShot.AWSHelper;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.model.Shot;

/**
 * Created by mwszedybyl on 7/28/15.
 */
public class DownloadMediaService extends IntentService
{

    // Required no arg constructor
    public DownloadMediaService() {
        super("DownloadMediaService");
    }

    String videoUrl;
    S3ObjectInputStream inputStream;
    OutputStream outputStream = null;
    File videoFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH + System.currentTimeMillis()+".mp4");

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do work here (Upload the file)
        Shot shot = intent.getParcelableExtra("shot");
        try
        {
            if(!shot.isVideo())
            {
                Bitmap bitmap = AWSHelper.getRemoteImageAsBitmap(shot.getUrl());

            } else
            {
                inputStream = AWSHelper.getRemoteVideo(videoUrl);
                try {
                    // write the inputStream to a FileOutputStream
                    outputStream =  new FileOutputStream(videoFile);

                    int read = 0;
                    byte[] bytes = new byte[1024];

                    while ((read = inputStream.read(bytes)) != -1) {
                        outputStream.write(bytes, 0, read);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally {
            shot.setMediaDownloaded(true);
            DbHelper.commitOrUpdateShot(shot);
        }
    }



}



