package tmtm.SpotShot.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.File;

import tmtm.SpotShot.AWSHelper;
import tmtm.SpotShot.NotificationHelper;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 7/18/15.
 */
public class UploadMediaService extends IntentService {

    // Required no arg constructor
    public UploadMediaService() {
        super("UploadMediaService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public UploadMediaService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do work here (Upload the file)
        String filePath = intent.getStringExtra("filePath");
        Shot shot = intent.getParcelableExtra("shot");
        boolean isPrivate = intent.getBooleanExtra("isPrivate", false);
        File file = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, filePath);
        if(file.exists()) {
            // Do upload
            SpotShotApplication.getShotsUploading().add(shot);

            ConnectivityManager connMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
            {
                // fetch data
                try
                {
                    NotificationHelper.notify("Uploading shot...", "", NotificationHelper.UPLOADING_SHOT_NOTIFICATION_ID, null, null);
                    AWSHelper.storeMediaToS3(file, shot, isPrivate);
                } catch (Exception e)
                {
                    e.printStackTrace();
                } finally {
                    shot.setSuccessfullyUploaded(true);
                }
            }
        }
    }
}
