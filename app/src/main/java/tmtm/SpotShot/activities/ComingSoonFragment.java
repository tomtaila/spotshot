package tmtm.SpotShot.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tmtm.SpotShot.R;

/**
 * Created by ttaila on 7/11/15.
 */
public class ComingSoonFragment extends Fragment {

    public View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.coming_soon_fragment, parent, false);
        setupViews();
        return rootView;
    }

    private void setupViews(){

    }

}
