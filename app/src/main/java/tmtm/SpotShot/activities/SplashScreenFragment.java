package tmtm.SpotShot.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.activities.chat.ServerChatUtil;
import tmtm.SpotShot.activities.shot.ServerShotUtil;
import tmtm.SpotShot.activities.shot.ShotListFragment;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Friend;

/**
 * Created by ttaila on 7/22/15.
 */
public class SplashScreenFragment extends Fragment {

    private static final String TAG = "ChatFragment";

    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new SplashScreenTask().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_splash_screen, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private class SplashScreenTask extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute(){
            DbHelper.removeExpiredMedia();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ServerShotUtil serverShotUtil = new ServerShotUtil(getActivity());
            serverShotUtil.getShots(LocationHelper.getLastLocation(), SpotShotApplication.getSharedPreferences().getInt("maxRadius", 50), ShotListFragment.SORT_BY_TOP_FLAG);
            serverShotUtil.getShots(LocationHelper.getLastLocation(), SpotShotApplication.getSharedPreferences().getInt("maxRadius", 50), ShotListFragment.SORT_BY_NEWEST_FLAG);
            getFriendRecordsAndUpdate();
            ServerChatUtil.getChats(SpotShotApplication.getCurrentUser(), null);
            // TODO commit or update shots

            // Delete all shots in db older than 24 hours
            DbHelper.removeExpiredShots();
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            Intent i = new Intent(getActivity(), ViewPagerActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(i);
        }
    }

    private void getFriendRecordsAndUpdate() {
        RequestParams params = new RequestParams();
        params.put("user_uuid", SpotShotApplication.getCurrentUser().getUuid());
        SpotShotRestClient.post(URLHelper.GET_FRIENDS_AND_REQUESTS_URL, params, new JsonHttpResponseHandler() {
            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers,final JSONArray response) {
                // If the response is JSONObject
                new Runnable() {
                    @Override
                    public void run() {
                        List<Friend> friends = new ArrayList<>();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Friend f = Friend.buildFromJson(jsonObject);
                                if(f != null) {
                                    friends.add(f);
                                }
                            }
                            DbHelper.commitOrUpdateFriends(friends);
                            DbHelper.removeInvalidFriends(friends, SpotShotApplication.getCurrentUser());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }.run();
            }
        });
    }

}
