package tmtm.SpotShot.activities.chat;

import tmtm.SpotShot.model.Message;

/**
 * Created by ttaila on 8/5/15.
 */
public interface CreateMessageListener {

    void onMessageCreated(Message m);

}
