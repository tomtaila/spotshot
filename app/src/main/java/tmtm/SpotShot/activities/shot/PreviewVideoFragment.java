package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.VideoView;

import com.bugsnag.android.Bugsnag;

import java.io.File;
import java.util.UUID;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.activities.chat.ServerChatUtil;
import tmtm.SpotShot.model.Message;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.services.UploadMediaService;

/**
 * Created by ttaila on 7/12/15.
 */
public class PreviewVideoFragment extends Fragment implements OnShotCreatedListener {

    private final String TAG = "PreviewVideoFragment";
    private static final int VIDEO_SENT_CODE = 1;
    private static final int VIDEO_CANCELED_CODE = 2;

    private View rootView;
    private VideoView videoView;
    private ImageButton cancelBtn;
    private ImageButton sendMediaBtn;
    private boolean isPrivateShot;

    private int position = 0;

    private String filePath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.fragment_preview_video, parent, false);
        Bundle extras = getActivity().getIntent().getExtras();

        filePath = extras.getString("filePath");
        isPrivateShot = extras.getBoolean("isPrivate");

        setupViews();

        return rootView;
    }

    private void setupViews() {
        videoView = (VideoView) rootView.findViewById(R.id.video_view);
        playVideo();

        cancelBtn = (ImageButton) rootView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Cancel button pressed");

                getActivity().setResult(VIDEO_CANCELED_CODE);
                getActivity().onBackPressed();
            }
        });

        sendMediaBtn = (ImageButton) rootView.findViewById(R.id.send_media_btn);
        sendMediaBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bugsnag.leaveBreadcrumb("Upload media button pressed");
                Log.i(TAG, "send video button pressed");
                sendVideo(SpotShotApplication.getSharedPreferences().getBoolean("anonymousOn", false));

                    getActivity().setResult(VIDEO_SENT_CODE);
                    getActivity().onBackPressed();

            }
        });
    }

    private void playVideo() {
        Uri uri = Uri.parse(new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, filePath).getPath());
        videoView.setVideoURI(uri);

        videoView.requestFocus();
        //we also set an setOnPreparedListener in order to know when the video file is ready for playback
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {
                //if we have a position on savedInstanceState, the video playback should start from here
                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                } else {
                    //if we come from a resumed activity, video playback will be paused
                    videoView.pause();
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });
    }

    public void sendVideo(boolean anonymous){
        Log.d(TAG, "start send video");

        Location location = new Location("");
        if(LocationHelper.getLastLocation() != null) {
            location = LocationHelper.getLastLocation();
        }
        //Create shot
        Shot shot = new Shot(UUID.randomUUID().toString(), SpotShotApplication.getCurrentUser().getUuid(), location.getLatitude(), location.getLongitude(), filePath, System.currentTimeMillis(), "caption default", true);
        shot.setNsfwTag(0);
        shot.setAnonymous(anonymous);
        shot.setUsername(SpotShotApplication.getCurrentUser().getName());
        if(isPrivateShot)
        {
            Message message = getActivity().getIntent().getParcelableExtra("message");
            message.setBody(filePath);
            message.setTimeSent(System.currentTimeMillis());
            ServerChatUtil.createMessage(message, null);
        }

        Intent intent = new Intent(getActivity(), UploadMediaService.class);
        intent.putExtra("filePath", filePath);
        intent.putExtra("shot", shot);
        intent.putExtra("isPrivate", isPrivateShot);

        getActivity().startService(intent);
    }

    @Override
    public void onShotCreatedComplete(Shot shot) {
        // TODO ?? Do we even need this to do anything
    }
}
