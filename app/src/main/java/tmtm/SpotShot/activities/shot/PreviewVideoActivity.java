package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.os.Bundle;

import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by ttaila on 7/12/15.
 */
public class PreviewVideoActivity extends SingleFragmentActivity {

    @Override
    public Fragment createFragment() {
        PreviewVideoFragment fragment = new PreviewVideoFragment();
        Bundle args = new Bundle();

        args.putString("filePath" , getIntent().getStringExtra("filePath"));
        args.putString("isPrivate" , getIntent().getStringExtra("isPrivate"));
        fragment.setArguments(args);
        return fragment;
    }

}
