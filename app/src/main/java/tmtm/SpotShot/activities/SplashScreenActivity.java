package tmtm.SpotShot.activities;

import android.app.Fragment;

/**
 * Created by ttaila on 7/22/15.
 */
public class SplashScreenActivity extends SingleFragmentActivity {
    @Override
    public Fragment createFragment() {
        return new SplashScreenFragment();
    }
}
