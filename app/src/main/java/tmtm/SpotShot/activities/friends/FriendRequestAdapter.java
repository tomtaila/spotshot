package tmtm.SpotShot.activities.friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

import tmtm.SpotShot.R;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Friend;

/**
 * Created by ttaila on 8/3/15.
 */
public class FriendRequestAdapter extends ArrayAdapter<Friend> {

    private List<Friend> friendRequests;

    public FriendRequestAdapter(Context context, int resource, List<Friend> friendRequests) {
        super(context, resource);
        this.friendRequests = friendRequests;
    }

    @Override
    public Friend getItem(int position) {
        return friendRequests.get(position);
    }

    @Override
    public int getCount() {
        return friendRequests.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.add_friend_by_username_item, null);
            holder = new ViewHolder(convertView);
            holder.userNameTV = (TextView) convertView.findViewById(R.id.user_nameTV);
            holder.friendBtn = (Button) convertView.findViewById(R.id.friend_btn);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        populateView(holder, position);

        return convertView;
    }

    private void populateView(final ViewHolder holder, final int position) {
        final Friend friend = getItem(position);
        holder.userNameTV.setText(friend.getUserUserName());
        holder.friendBtn.setText("Accept Friend");
        holder.friendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams params = new RequestParams();
                params.put("user_uuid", friend.getUserUuid());
                params.put("friend_uuid", friend.getFriendUuid());
                params.put("request", true);
                params.put("new_friend_uuid", UUID.randomUUID().toString());
                params.put("chat_uuid", UUID.randomUUID().toString());
                SpotShotRestClient.post(URLHelper.ACCEPT_FRIEND_URL, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // If the response is JSONObject
                        try {
                            if (response.getBoolean("success")) {
                                // Store in local db
                                JSONObject jsonFriend = response.getJSONObject("friendBA");
                                Friend friend = new Friend(jsonFriend.getString("uuid"), jsonFriend.getString("user_uuid"), jsonFriend.getString("user_name"), jsonFriend.getString("friend_uuid"), jsonFriend.getString("friend_name"), jsonFriend.getBoolean("request"));
                                DbHelper.commitOrUpdateFriend(friend);
                                JSONObject jsonFriendAB = response.getJSONObject("friendAB");
                                Friend requestToBeRemoved = new Friend(jsonFriendAB.getString("uuid"), jsonFriendAB.getString("user_uuid"), jsonFriendAB.getString("user_name"), jsonFriendAB.getString("friend_uuid"), jsonFriendAB.getString("friend_name"), jsonFriendAB.getBoolean("request"));
                                DbHelper.removeFriend(requestToBeRemoved);
                                JSONObject jsonChat = response.getJSONObject("chat");
                                Chat chat = new Chat(jsonChat.getString("uuid"), jsonChat.getString("user_a_uuid"), jsonChat.getString("user_a_name"), jsonChat.getString("user_b_uuid"), jsonChat.getString("user_b_name"));
                                DbHelper.commitOrUpdateChat(chat);
                                friendRequests.remove(position);
                                Toast.makeText(getContext(), "Friend Accepted!", Toast.LENGTH_SHORT).show();
                                notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userNameTV;
        private Button friendBtn;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }


}
