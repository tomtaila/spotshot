package tmtm.SpotShot.activities.friends;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.model.AddressBookContact;

/**
 * Created by mwszedybyl on 8/12/15.
 */
public class ContactsAdapter extends ArrayAdapter<AddressBookContact>
{

    private List<AddressBookContact> contacts;

    public ContactsAdapter(Context context, int resource, List<AddressBookContact> contacts) {
        super(context, resource);
        this.contacts = contacts;
    }

    @Override
    public AddressBookContact getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.add_friend_from_address_book, null);
            holder = new ViewHolder(convertView);
            holder.userNameTV = (TextView) convertView.findViewById(R.id.user_nameTV);
            holder.phoneNumberTV = (TextView) convertView.findViewById(R.id.phone_numberTV);
            holder.sendMessageButton = (Button) convertView.findViewById(R.id.send_message_btn);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        populateView(holder, position);

        return convertView;
    }

    private void populateView(final ViewHolder holder, final int position) {
        final AddressBookContact contact = getItem(position);
        holder.userNameTV.setText(contact.getName());
        holder.phoneNumberTV.setText(contact.getPhoneNum());
//        holder.sendMessageButton.setText("Add");
        holder.sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String number = contact.getPhoneNum();
             Intent i = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, "message"));// The number on which you want to send SMS
                i.putExtra("sms_body", "Heyownload spotshot now!");
                getContext().startActivity(i);

            }
        });
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userNameTV;
        private TextView phoneNumberTV;
        private Button sendMessageButton;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }


}
