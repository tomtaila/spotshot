package tmtm.SpotShot.activities.friends;

import android.app.Fragment;

import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by ttaila on 8/4/15.
 */
public class FriendsActivity extends SingleFragmentActivity {

    @Override
    public Fragment createFragment() {
        return new FriendsFragment();
    }

}
