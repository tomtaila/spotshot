package tmtm.SpotShot.activities.friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Friend;
import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 8/2/15.
 */
public class AddFriendByUsernameAdapter extends ArrayAdapter<User> {

    private List<User> userList;

    public AddFriendByUsernameAdapter(Context context, int resource, List<User> userList) {
        super(context, resource);
        this.userList = userList;
    }

    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.add_friend_by_username_item, null);
            holder = new ViewHolder(convertView);
            holder.userNameTV = (TextView) convertView.findViewById(R.id.user_nameTV);
            holder.friendBtn = (Button) convertView.findViewById(R.id.friend_btn);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        User user = getItem(position);
        populateView(holder, user, position);

        return convertView;
    }

    private void populateView(final ViewHolder holder, final User user, final int position) {
        holder.userNameTV.setText(user.getName());
        String friendStatus = user.getFriendStatus();
        if(friendStatus != null) {
            switch (friendStatus) {
                case "not_friends":
                    holder.friendBtn.setText("Add Friend");
                    holder.friendBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            User currentUser = SpotShotApplication.getCurrentUser();
                            final Friend friend = new Friend(UUID.randomUUID().toString(), currentUser.getUuid(), currentUser.getName(), user.getUuid(), user.getName(), true);
                            RequestParams params = new RequestParams();
                            params.put("uuid", friend.getUuid());
                            params.put("request", true);
                            params.put("user_uuid", friend.getUserUuid());
                            params.put("user_name", friend.getUserUserName());
                            params.put("friend_uuid", friend.getFriendUuid());
                            params.put("friend_name", friend.getFriendUserName());
                            SpotShotRestClient.post(URLHelper.CREATE_FRIEND_URL, params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    // If the response is JSONObject
                                    try {
                                        if (response.getBoolean("success")) {
                                            DbHelper.commitOrUpdateFriend(friend);
                                            user.setFriendStatus("pending");
                                            userList.remove(position);
                                            userList.add(position, user);
                                            notifyDataSetChanged();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            holder.friendBtn.setText("Pending");
                        }
                    });
                    break;
                case "pending":
                    holder.friendBtn.setText("Pending");
                    holder.friendBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final User currentUser = SpotShotApplication.getCurrentUser();
                            RequestParams params = new RequestParams();
                            params.put("user_uuid", currentUser.getUuid());
                            params.put("friend_uuid", user.getUuid());
                            SpotShotRestClient.post(URLHelper.DELETE_FRIEND_REQUEST_URL, params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    // If the response is JSONObject
                                    try {
                                        if (response.getBoolean("success")) {
                                            Friend friend = new Friend();
                                            friend.setUserUuid(currentUser.getUuid());
                                            friend.setFriendUuid(user.getUuid());
                                            DbHelper.removeFriend(friend);
                                            user.setFriendStatus("not_friends");
                                            userList.remove(position);
                                            userList.add(position, user);
                                            notifyDataSetChanged();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            holder.friendBtn.setText("Add Friend");
                        }
                    });
                    break;
                case "requested":
                    holder.friendBtn.setText("Accept Friend");
                    holder.friendBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final User currentUser = SpotShotApplication.getCurrentUser();
                            RequestParams params = new RequestParams();
                            params.put("user_uuid", user.getUuid());
                            params.put("friend_uuid", currentUser.getUuid());
                            params.put("request", true);
                            params.put("new_friend_uuid", UUID.randomUUID().toString());
                            params.put("chat_uuid", UUID.randomUUID().toString());
                            SpotShotRestClient.post(URLHelper.ACCEPT_FRIEND_URL, params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    // If the response is JSONObject
                                    try {
                                        if (response.getBoolean("success")) {
                                            // Store in local db
                                            JSONObject jsonFriend = response.getJSONObject("friendBA");
                                            Friend friend = new Friend(jsonFriend.getString("uuid"), jsonFriend.getString("user_uuid"), jsonFriend.getString("user_name"), jsonFriend.getString("friend_uuid"), jsonFriend.getString("friend_name"), jsonFriend.getBoolean("request"));
                                            DbHelper.commitOrUpdateFriend(friend);
                                            JSONObject jsonFriendAB = response.getJSONObject("friendAB");
                                            Friend requestToBeRemoved = new Friend(jsonFriendAB.getString("uuid"), jsonFriendAB.getString("user_uuid"), jsonFriendAB.getString("user_name"), jsonFriendAB.getString("friend_uuid"), jsonFriendAB.getString("friend_name"), jsonFriendAB.getBoolean("request"));
                                            DbHelper.removeFriend(requestToBeRemoved);
                                            JSONObject jsonChat = response.getJSONObject("chat");
                                            Chat chat = new Chat(jsonChat.getString("uuid"), jsonChat.getString("user_a_uuid"), jsonChat.getString("user_a_name"), jsonChat.getString("user_b_uuid"), jsonChat.getString("user_b_name"));
                                            DbHelper.commitOrUpdateChat(chat);

                                            user.setFriendStatus("friends");
                                            userList.remove(position);
                                            userList.add(position, user);
                                            notifyDataSetChanged();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            holder.friendBtn.setText("Remove Friend");
                        }
                    });
                    break;
                case "friends":
                    holder.friendBtn.setText("Remove Friend");
                    holder.friendBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final User currentUser = SpotShotApplication.getCurrentUser();
                            RequestParams params = new RequestParams();
                            params.put("user_uuid", currentUser.getUuid());
                            params.put("friend_uuid", user.getUuid());
                            params.put("request", false);
                            SpotShotRestClient.post(URLHelper.DELETE_FRIEND_URL, params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    // If the response is JSONObject
                                    try {
                                        if (response.getBoolean("success")) {
                                            Friend friend = new Friend();
                                            friend.setUserUuid(currentUser.getUuid());
                                            friend.setFriendUuid(user.getUuid());
                                            DbHelper.removeFriend(friend);
                                            DbHelper.removeChat(new Chat(friend.getFriendUuid(), friend.getUserUuid()));

                                            user.setFriendStatus("not_friends");
                                            userList.remove(position);
                                            userList.add(position, user);
                                            notifyDataSetChanged();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            holder.friendBtn.setText("Add Friend");
                        }
                    });
                    break;
            }
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userNameTV;
        private Button friendBtn;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
