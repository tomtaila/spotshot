package tmtm.SpotShot.activities.friends;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.SingleFragmentActivity;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.gcm.GCMMessageHandler;
import tmtm.SpotShot.model.Friend;

/**
 * Created by ttaila on 8/3/15.
 */
public class AcceptFriendsActivity extends SingleFragmentActivity {

    @Override
    public Fragment createFragment() {
        return new AcceptFriendFragment();
    }

}
