package tmtm.SpotShot.activities.friends;

import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.gcm.GCMMessageHandler;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Friend;

/**
 * Created by ttaila on 8/3/15.
 */
public class AcceptFriendFragment extends ListFragment {

    public static boolean onScreen = false;

    private View rootView;
    private List<Friend> friendRequests;
    private ListView listView;
    private FriendRequestAdapter arrayAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        friendRequests = DbHelper.getFriendsRequestsForUser(SpotShotApplication.getCurrentUser());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_accept_friends, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews() {
        listView = (ListView) rootView.findViewById(android.R.id.list);
        listView.setDivider(null);
        arrayAdapter = new FriendRequestAdapter(getActivity(), R.layout.friend_item, friendRequests);
        setListAdapter(arrayAdapter);
    }

    private void getFriendRecordsAndUpdate() {
        RequestParams params = new RequestParams();
        params.put("user_uuid", SpotShotApplication.getCurrentUser().getUuid());
        SpotShotRestClient.post(URLHelper.GET_FRIENDS_AND_REQUESTS_URL, params, new JsonHttpResponseHandler() {
            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                // If the response is JSONObject
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        List<Friend> friends = new ArrayList<>();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Friend f = Friend.buildFromJson(jsonObject);
                                if (f != null) {
                                    friends.add(f);
                                }
                            }
                            DbHelper.commitOrUpdateFriends(friends);
                            DbHelper.removeInvalidFriends(friends, SpotShotApplication.getCurrentUser());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void param) {
                        friendRequests.clear();
                        friendRequests.addAll(DbHelper.getFriendsRequestsForUser(SpotShotApplication.getCurrentUser()));
                        arrayAdapter.notifyDataSetChanged();
                    }
                }.execute();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getFriendRecordsAndUpdate();
        onScreen = true;
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GCMMessageHandler.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        onScreen = false;
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(onScreen) {
                friendRequests.clear();
                friendRequests.addAll(DbHelper.getFriendsRequestsForUser(SpotShotApplication.getCurrentUser()));
                arrayAdapter.notifyDataSetChanged();
            }
        }
    };

}
