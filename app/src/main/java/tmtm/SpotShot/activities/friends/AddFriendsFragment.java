package tmtm.SpotShot.activities.friends;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import tmtm.SpotShot.R;
import tmtm.SpotShot.gcm.GCMMessageHandler;

/**
 * Created by ttaila on 8/2/15.
 */
public class AddFriendsFragment extends Fragment implements View.OnClickListener {

    public static boolean onScreen = false;

    private View rootView;
    private RelativeLayout addByUsernameItem;
    private RelativeLayout addFromAddressBookItem;
    private RelativeLayout acceptFriendsItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_friends, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews() {
        addByUsernameItem = (RelativeLayout) rootView.findViewById(R.id.add_by_username);
        addByUsernameItem.setOnClickListener(this);
        addFromAddressBookItem = (RelativeLayout) rootView.findViewById(R.id.add_from_address_book);
        addFromAddressBookItem.setOnClickListener(this);
        acceptFriendsItem = (RelativeLayout) rootView.findViewById(R.id.accept_friends);
        acceptFriendsItem.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == addByUsernameItem) {
            Intent i = new Intent(getActivity(), AddFriendByUsernameActivity.class);
            startActivity(i);
        } else if(v == addFromAddressBookItem) {
            Intent i = new Intent(getActivity(), AddressBookContactsActivity.class);
            startActivity(i);
        } else if(v == acceptFriendsItem) {
            Intent i = new Intent(getActivity(), AcceptFriendsActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onScreen = true;
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GCMMessageHandler.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        onScreen = false;
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(onScreen) {
                // TODO update db and update UI
            }
        }
    };

}
