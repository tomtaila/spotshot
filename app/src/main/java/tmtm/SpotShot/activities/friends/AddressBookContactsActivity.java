package tmtm.SpotShot.activities.friends;

import android.app.Fragment;

import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by mwszedybyl on 8/12/15.
 */
public class AddressBookContactsActivity extends SingleFragmentActivity
{

    @Override
    public Fragment createFragment() {
        return new AddressBookContactsFragment();
    }

}
