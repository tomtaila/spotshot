package tmtm.SpotShot.activities.friends;

import android.app.ListFragment;
import android.content.ContentResolver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import tmtm.SpotShot.AddressBookHelper;
import tmtm.SpotShot.R;
import tmtm.SpotShot.model.AddressBookContact;

/**
 * Created by mwszedybyl on 8/12/15.
 */
public class AddressBookContactsFragment extends ListFragment
{
    private View rootView;
    private List<AddressBookContact> contacts;
    private ListView listView;
    private ContactsAdapter arrayAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_address_book_contacts, parent, false);
        ContentResolver mContentResolver = getActivity().getContentResolver();

        contacts =  AddressBookHelper.getContactList(mContentResolver, getActivity().getBaseContext());
        setupViews();
        return rootView;
    }

    private void setupViews() {
        listView = (ListView) rootView.findViewById(android.R.id.list);
        listView.setDivider(null);
        arrayAdapter = new ContactsAdapter(getActivity(), R.layout.add_friend_from_address_book, contacts);
        setListAdapter(arrayAdapter);
        AddressBookTask addressBookTask = new AddressBookTask();
        addressBookTask.execute();
    }




    @Override
    public void onResume() {
        super.onResume();
    }

    private class AddressBookTask extends AsyncTask<Void, Void, Void>
    {
        ContentResolver mContentResolver = getActivity().getContentResolver();

        @Override
        protected Void doInBackground(Void... params) {
            contacts =  AddressBookHelper.getContactList(mContentResolver, getActivity().getBaseContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            arrayAdapter.notifyDataSetChanged();
        }

    }

}
