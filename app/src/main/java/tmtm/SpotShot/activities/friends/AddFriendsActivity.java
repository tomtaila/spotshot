package tmtm.SpotShot.activities.friends;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import tmtm.SpotShot.activities.SingleFragmentActivity;
import tmtm.SpotShot.gcm.GCMMessageHandler;

/**
 * Created by ttaila on 8/2/15.
 */
public class AddFriendsActivity extends SingleFragmentActivity {

    public static boolean onScreen = false;

    @Override
    public Fragment createFragment() {
        return new AddFriendsFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        onScreen = true;
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GCMMessageHandler.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        onScreen = false;
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(onScreen) {
                Bundle data = intent.getBundleExtra("data");
                // TODO Do stuff to update the UI
            }
        }
    };

}
