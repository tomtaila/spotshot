package tmtm.SpotShot.activities.friends;

import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.users.SearchUserTaskListener;
import tmtm.SpotShot.activities.users.ServerUserUtil;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.gcm.GCMMessageHandler;
import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 8/2/15.
 */
public class AddFriendByUsernameFragment extends ListFragment implements SearchUserTaskListener {

    private static final String TAG = "AddFriendByUsernameFragment";

    public static boolean onScreen = false;

    private View rootView;
    private List<User> userList;
    private ListView listView;
    private EditText searchBar;
    private AddFriendByUsernameAdapter arrayAdapter;
    private ServerUserUtil serverUserUtil;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serverUserUtil = new ServerUserUtil(getActivity(), this);
        userList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_user_by_name, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews() {
        listView = (ListView) rootView.findViewById(android.R.id.list);
        listView.setDivider(null);
        arrayAdapter = new AddFriendByUsernameAdapter(getActivity(), R.layout.add_friend_by_username_item, userList);
        setListAdapter(arrayAdapter);

        searchBar = (EditText) rootView.findViewById(R.id.user_search_bar);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                serverUserUtil.searchUser(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        onScreen = true;
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GCMMessageHandler.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        onScreen = false;
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
    }

    @Override
    public void doOnSearchUserTaskComplete(User user, String friendStatus) {
        userList.clear();
        if(user != null) {
            user.setFriendStatus(friendStatus);
            userList.add(user);
        }
        arrayAdapter.notifyDataSetChanged();
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(onScreen) {
                serverUserUtil.searchUser(searchBar.getText().toString());
            }
        }
    };
}
