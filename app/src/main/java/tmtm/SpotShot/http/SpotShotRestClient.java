package tmtm.SpotShot.http;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import tmtm.SpotShot.URLHelper;

/**
 * Created by ttaila on 7/29/15.
 */
public class SpotShotRestClient {

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String url) {
        if(!URLHelper.QA_ENABLED) {
            return URLHelper.BASE_URL + url;
        } else {
            return URLHelper.BASE_QA_URL + url;
        }
    }


}
