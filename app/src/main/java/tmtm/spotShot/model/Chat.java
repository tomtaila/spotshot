package tmtm.SpotShot.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ttaila on 2/28/15.
 */
public class Chat extends SugarRecord<Chat> implements Parcelable {

    // Unique id of chat on server
    private String uuid;
    // One of the users Id
    private String userAUuid;
    // One of the users Id
    private String userBUuid;
    // The other users names
    private String userAName;
    // The other users names
    private String userBName;

    public Chat() {}

    public Chat(String userAUuid, String userBUuid) {
        this.userAUuid = userAUuid;
        this.userBUuid = userBUuid;
    }

    public Chat(String uuid, String userAUuid, String userAName, String userBUuid, String userBName) {
        this.uuid = uuid;
        this.userAUuid = userAUuid;
        this.userAName = userAName;
        this.userBUuid = userBUuid;
        this.userBName = userBName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserAUuid() {
        return userAUuid;
    }

    public void setUserAUuid(String userAUuid) {
        this.userAUuid = userAUuid;
    }

    public String getUserBUuid() {
        return userBUuid;
    }

    public void setUserBUuid(String userBUuid) {
        this.userBUuid = userBUuid;
    }

    public String getUserAName() {
        return userAName;
    }

    public void setUserAName(String userAName) {
        this.userAName = userAName;
    }

    public String getUserBName() {
        return userBName;
    }

    public void setUserBName(String userBName) {
        this.userBName = userBName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(uuid);
        out.writeString(userAUuid);
        out.writeString(userAName);
        out.writeString(userBUuid);
        out.writeString(userBName);
    }

    public static final Parcelable.Creator<Chat> CREATOR = new Parcelable.Creator<Chat>() {
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    private Chat(Parcel in) {
        uuid = in.readString();
        userAUuid = in.readString();
        userAName = in.readString();
        userBUuid = in.readString();
        userBName = in.readString();
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if(obj instanceof Chat) {
            Chat other = (Chat) obj;
            result = this.getUuid().equals(other.getUuid());
        }
        return result;
    }

    public static Chat buildFromJson(JSONObject json) {
        Chat chat = null;
        try {
            chat = new Chat(json.getString("uuid"), json.getString("user_a_uuid"), json.getString("user_a_name"), json.getString("user_b_uuid"), json.getString("user_b_name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return chat;
    }
}
