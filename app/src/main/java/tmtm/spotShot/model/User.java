package tmtm.SpotShot.model;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

/**
 * Created by ttaila on 2/28/15.
 */
public class User extends SugarRecord<User>{

    // Name of user
    private String name;
    // Email of user
    private String email;
    // Phone number of user (helps find friends and friends find you)
    private long phoneNumber;
    // Total of all likes user has ever received for their snaps
    private int score;
    // Users UUID
    private String uuid;
    // represents the friend status for when we search users for friend purposes
    @Ignore
    private String friendStatus;

    public User() {
    }

    public User(String name, String password) {
        this.name = name;
        this.score = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void incrementScore() {
        score++;
    }

    public void decrementScore() {
        score--;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }


    public String getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(String friendStatus) {
        this.friendStatus = friendStatus;
    }
}
