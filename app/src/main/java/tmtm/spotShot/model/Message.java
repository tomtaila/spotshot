package tmtm.SpotShot.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ttaila on 2/28/15.
 */
public class Message extends SugarRecord<Message> implements Comparable<Message>, Parcelable {

    public static final int TEXT = 0;
    public static final int PHOTO = 1;
    public static final int VIDEO = 2;
    public static final int LOCATION = 3;

    // Unique id of the message
    private String uuid;
    // Id of the chat the message belongs to
    private String chatUuid;
    // Id of user sending the message
    private String senderUuid;
    // Name of user sending the message
    private String senderName;
    // Id of user receiving the message
    private String recipientUuid;
    // Name of user receiving the message
    private String recipientName;
    // Message content
    private String body;
    // Time sent
    private long timeSent;
    // Viewed by recipient
    private boolean viewed;
    private boolean senderSaved;
    private boolean recipientSaved;
    // Message type
    private int type;

    public Message() {}

    public Message(String uuid, String chatUuid, String senderUuid, String senderName, String recipientUuid, String recipientName, String body, int messageType, long timeSent, boolean viewed) {
        this.uuid = uuid;
        this.chatUuid = chatUuid;
        this.senderUuid = senderUuid;
        this.senderName = senderName;
        this.recipientUuid = recipientUuid;
        this.recipientName = recipientName;
        this.body = body;
        this.type = messageType;
        this.timeSent = timeSent;
        this.viewed = viewed;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getChatUuid() {
        return chatUuid;
    }

    public void setChatUuid(String chatUuid) {
        this.chatUuid = chatUuid;
    }

    public String getSenderUuid() {
        return senderUuid;
    }

    public void setSenderUuid(String senderUuid) {
        this.senderUuid = senderUuid;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getRecipientUuid() {
        return recipientUuid;
    }

    public void setRecipientUuid(String recipientUuid) {
        this.recipientUuid = recipientUuid;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }

    public boolean isSenderSaved() {
        return senderSaved;
    }

    public void setSenderSaved(boolean senderSaved) {
        this.senderSaved = senderSaved;
    }

    public boolean isRecipientSaved() {
        return recipientSaved;
    }

    public void setRecipientSaved(boolean recipientSaved) {
        this.recipientSaved = recipientSaved;
    }

    public long getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(long timeSent) {
        this.timeSent = timeSent;
    }

    @Override
    public int compareTo(Message another) {
        return (int) (timeSent - another.timeSent);
    }

    public static Message buildFromJson(JSONObject json) {
        Message m = null;
        try {
            m = new Message(json.getString("uuid"), json.getString("chat_uuid"), json.getString("sender_uuid"), json.getString("sender_name"),json.getString("recipient_uuid"), json.getString("recipient_name"), json.getString("body"), json.getInt("message_type"), json.getLong("time_sent"), json.getBoolean("viewed"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return m;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(uuid);
        out.writeString(chatUuid);
        out.writeString(senderUuid);
        out.writeString(senderName);
        out.writeString(recipientUuid);
        out.writeString(recipientName);
        out.writeString(body);
        out.writeInt(type);
        out.writeLong(timeSent);
        out.writeByte((byte) (viewed ? 1 : 0));     //if isVideo == true, byte == 1
//        out.writeByte((byte) (senderSaved ? 1 : 0)); // if anonymous == true, byte == 1
//        out.writeByte((byte) (recipientSaved ? 1 : 0)); // if successfullyUploaded == true, byte == 1
    }

    public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    private Message(Parcel in) {
        uuid = in.readString();
        chatUuid = in.readString();
        senderUuid = in.readString();
        senderName = in.readString();
        recipientUuid = in.readString();
        recipientName = in.readString();
        body = in.readString();
        type = in.readInt();
        timeSent = in.readLong();
        viewed = in.readByte() != 0;
//        senderSaved = in.readByte() != 0;
//        recipientSaved = in.readByte() != 0;
    }
}
