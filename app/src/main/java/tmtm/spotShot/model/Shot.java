package tmtm.SpotShot.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.Comparator;

import tmtm.SpotShot.Constants;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.LocationHelper;


/**
 * Created by ttaila on 2/27/15.
 */
public class Shot extends SugarRecord<Shot> implements Parcelable
{
    private String uuid;
    // Uuid of the User who created the shot
    private String userUuid;
    // lat for location
    private double lat;
    // long for location
    private double lng;
    // Location tag (mainly for longitude and latitude)
    @Ignore
    private Location location;
    // Distance (not to be stored server side)
    private int distance;
    // Url pointing to the file stored on AWS
    private String url;
    // Url pointing to thumbnail stored on AWS
    private String thumbnailUrl;
    // Representation of the time this was created
    private long timeCreated;
    // Text on shot
    private String caption;
    // Indicates where on the image the text bar should appear (offset from bottom of the screen)
    private int captionLocation;
    // Text size class for text on shot (1 = small, 2 = medium, 3 = large, 4 = xlarge)
    private int captionSizeClass;
    // Flag for NSFW shots
    private int nsfwTag;
    // Score based on how many likes recieved on a shot
    private int score;
    // Indicates whether this shot is a video
    private boolean isVideo;
    //Owner username
    private String username;
    // Anonymous (i.e. display user name with shot or not)
    private boolean anonymous;
    // Submitted or not
    private boolean successfullyUploaded;
    // Flagged for deletion
    private boolean deleted;
    //image finished downloaded
    private boolean mediaDownloaded;

    public Shot() {}

    public Shot(String uuid, String userUuid, double lat, double lng, String url, long timeCreated, boolean isVideo) {
        this(uuid, userUuid, lat, lng, url, timeCreated, null, 0, 0, isVideo);
    }

    public Shot(String uuid, String userUuid, double lat, double lng, String url, long timeCreated, String caption, boolean isVideo) {
        this(uuid, userUuid, lat, lng, url, timeCreated, caption, 1, 1, isVideo);
    }

    public Shot(String uuid, String userUuid, double lat, double lng, String url, long timeCreated, String caption, int captionLocation, int captionSizeClass, boolean isVideo) {
        this.successfullyUploaded = false;
        this.uuid = uuid;
        this.userUuid = userUuid;
        this.lat = lat;
        this.lng = lng;
        this.url = url;
        this.isVideo = isVideo;
        if(isVideo) {
            this.thumbnailUrl = url.replace(".mp4", "-thumb.jpg");
        } else {
            this.thumbnailUrl = url.replace(".jpg", "-thumb.jpg");
        }
        this.timeCreated = timeCreated;
        this.caption = caption;
        this.captionLocation = captionLocation;
        this.captionSizeClass = captionSizeClass;
        this.score = 0;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public Location getLocation() {
        if(location == null) {
            location = new Location("");
            location.setLatitude(lat);
            location.setLongitude(lng);
            this.distance = (int) (location.distanceTo(LocationHelper.getLastLocation()) / Constants.METRES_IN_A_MILE);
        }
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
        this.distance = (int) (location.distanceTo(LocationHelper.getLastLocation()) / Constants.METRES_IN_A_MILE);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getCaptionLocation() {
        return captionLocation;
    }

    public void setCaptionLocation(int captionLocation) {
        this.captionLocation = captionLocation;
    }

    public int getCaptionSizeClass() {
        return captionSizeClass;
    }

    public void setCaptionSizeClass(int captionSizeClass) {
        this.captionSizeClass = captionSizeClass;
    }

    public String getFullUrl() {
        return SpotShotApplication.APP_ROOT_MEDIA_PATH + url;
    }

    public int getNsfwTag()
    {
        return nsfwTag;
    }

    public void setNsfwTag(int nsfwTag)
    {
        this.nsfwTag = nsfwTag;
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int score)
    {
        this.score = score;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public boolean isSuccessfullyUploaded()
    {
        return successfullyUploaded;
    }

    public void setSuccessfullyUploaded(boolean successfullyUploaded)
    {
        this.successfullyUploaded = successfullyUploaded;
    }



    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if(obj instanceof Shot) {
            Shot other = (Shot) obj;
            result = this.getUuid().equals(other.getUuid());
        }
        return result;
    }

    public boolean isMediaDownloaded()
    {
        return mediaDownloaded;
    }

    public void setMediaDownloaded(boolean mediaDownloaded)
    {
        this.mediaDownloaded = mediaDownloaded;
    }
    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    public static class ScoreComparator implements Comparator<Shot> {
        @Override
        public int compare(Shot shotA, Shot shotB) {
            return shotB.score - shotA.score;
        }
    }

    public  static class TimeCreatedComparator implements Comparator<Shot> {
        @Override
        public int compare(Shot shotA, Shot shotB) {
            return (int) (shotB.timeCreated - shotA.timeCreated);
        }
    }

    @Override
    public String toString() {
        String str = "";
        str = str + "OwnerUuid = " + userUuid;
        str = str + ", TimeCreated = " + timeCreated;
        str = str + ", Score = " + score;

        return  str;
    }

    /** PARCELABLE IMPLEMENTATION */
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(uuid);
        out.writeString(userUuid);
        out.writeString(username);
        out.writeString(url);
        out.writeString(thumbnailUrl);
        out.writeLong(timeCreated);
        out.writeDouble(lat);
        out.writeDouble(lng);
        out.writeInt(score);
        out.writeInt(distance);
        out.writeByte((byte) (isVideo ? 1 : 0));     //if isVideo == true, byte == 1
        out.writeByte((byte) (anonymous ? 1 : 0)); // if anonymous == true, byte == 1
        out.writeByte((byte) (successfullyUploaded ? 1 : 0)); // if successfullyUploaded == true, byte == 1
        out.writeByte((byte) (mediaDownloaded ? 1 : 0)); // if successfullyDownloaded == true, byte == 1
    }

    public static final Parcelable.Creator<Shot> CREATOR = new Parcelable.Creator<Shot>() {
        public Shot createFromParcel(Parcel in) {
            return new Shot(in);
        }

        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };

    private Shot(Parcel in) {
        uuid = in.readString();
        userUuid = in.readString();
        username = in.readString();
        url = in.readString();
        thumbnailUrl = in.readString();
        timeCreated = in.readLong();
        lat = in.readDouble();
        lng = in.readDouble();
        score = in.readInt();
        distance = in.readInt();
        isVideo = in.readByte() != 0; // isVideo == true if byte != 0
        anonymous = in.readByte() != 0; // isVideo == true if byte != 0
        successfullyUploaded = in.readByte() != 0; // isVideo == true if byte != 0
        mediaDownloaded = in.readByte() != 0;
    }

}

