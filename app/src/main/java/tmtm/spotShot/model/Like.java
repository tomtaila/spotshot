package tmtm.SpotShot.model;

import com.orm.SugarRecord;

/**
 * Created by ttaila on 4/11/15.
 */
public class Like extends SugarRecord<Like> {

    private String likeUuid;
    private String userUuid;
    private String shotUuid;

    public Like() {}

    public Like(String likeUuid, String userUuid, String shotUuid) {
        this.likeUuid = likeUuid;
        this.userUuid = userUuid;
        this.shotUuid = shotUuid;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public String getShotUuid() {
        return shotUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public void setShotUuid(String shotUuid) {
        this.shotUuid = shotUuid;
    }

    @Override
    public boolean equals(Object o) {
        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Like or not "null instanceof [type]" also returns false */
        if (!(o instanceof Like)) {
            return false;
        }

        // typecast o to Like so that we can compare data members
        Like l = (Like) o;

        // Compare the data members and return accordingly
        return likeUuid.equals(l.likeUuid);

    }

    public String getLikeUuid()
    {
        return likeUuid;
    }

    public void setLikeUuid(String likeUuid)
    {
        this.likeUuid = likeUuid;
    }
}
