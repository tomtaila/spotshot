package tmtm.SpotShot;

/**
 * Created by ttaila on 3/9/15.
 */
public class Constants {

    public static final String PICTURE_BUCKET = "ttmw-localfeed-images";

    // DISTANCE CONSTANTS
    public static final double METRES_IN_A_MILE = 1609.34;
    public static final double METRES_IN_A_KM = 1000;

}
