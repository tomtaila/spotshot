package tmtm.SpotShot;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;

import com.amazonaws.event.ProgressEvent;
import com.amazonaws.mobileconnectors.s3.transfermanager.PersistableTransfer;
import com.amazonaws.mobileconnectors.s3.transfermanager.internal.S3ProgressListener;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import tmtm.SpotShot.activities.shot.ServerShotUtil;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 3/8/15.
 *
 */
public class AWSHelper {

    private static final String TAG = "AwsHelper";

    public static final String APP_ID = "112136867220";
    public static final String IDENTITY_POOL_ID = "us-east-1:8f25ddf6-ee44-4f42-b28c-ef9ac07c95cc";
    public static final String UNAUTH_ROLE = "arn:aws:iam::112136867220:role/ttmwlocalfeedrole_unauth";
    public static final String AUTH_ROLE = "arn:aws:iam::112136867220:role/ttmwlocal_role_auth";


    public static Bitmap getRemoteImageAsBitmap(String imageName) {
        try
        {
            S3Object object = SpotShotApplication.getS3Client().getObject(Constants.PICTURE_BUCKET, imageName);
            Bitmap bitmap = BitmapFactory.decodeStream(object.getObjectContent());
            return bitmap;
        } catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public static S3ObjectInputStream getRemoteVideo(String videoName) {
        S3Object object = null;
        try {
            object = SpotShotApplication.getS3Client().getObject(Constants.PICTURE_BUCKET, videoName);
        } catch (AmazonS3Exception e) {
            e.printStackTrace();
            // TODO: remove from server side AND remove from local db
        }

        if(object != null) {
            return object.getObjectContent();
        } else {
            return null;
        }
    }



    public static void storeMediaToS3(final File file, final Shot shot, final boolean isPrivateShot) {
        Log.d("awsHelper" , " storeMediaToS3");
        try
        {
            final PutObjectRequest por = new PutObjectRequest(Constants.PICTURE_BUCKET, shot.getUrl(), file);
            por.setGeneralProgressListener(new S3ProgressListener() {
                @Override
                public void onPersistableTransfer(PersistableTransfer persistableTransfer) {

                }

                @Override
                public void progressChanged(ProgressEvent progressEvent) {
                    if (progressEvent.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE) {
                        // Upload is completed!
                        Log.d(TAG, "Uploading file : " + file.getPath() + " COMPLETED!");
                        if(!isPrivateShot)
                        {
                            uploadThumbnail(file, shot);
                            ServerShotUtil serverShotUtil = new ServerShotUtil(SpotShotApplication.getInstance());
                            serverShotUtil.createShot(shot);
                        }
                    }
                }
            });
            PutObjectResult result = SpotShotApplication.getS3Client().putObject(por);
            Log.d(TAG, "Uploading file : " + file.getPath());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void uploadThumbnail(File file, final Shot shot) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        String thumbnailImageName = shot.getThumbnailUrl();
        String thumbnailPath = SpotShotApplication.APP_ROOT_MEDIA_PATH + "/" + shot.getThumbnailUrl();
        Bitmap thumbnailBitmap;

        if(shot.isVideo()) {
            thumbnailBitmap = ThumbnailUtils.createVideoThumbnail(file.getPath(), MediaStore.Images.Thumbnails.MICRO_KIND);
        } else {
            thumbnailBitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), 96, 96);
        }
        thumbnailBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

        byte[] data = byteArrayOutputStream.toByteArray();

        final File thumbnailFile = new File(thumbnailPath);
        try {
            FileOutputStream fos = new FileOutputStream(thumbnailFile);
            fos.write(data);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        PutObjectRequest por = new PutObjectRequest(Constants.PICTURE_BUCKET, thumbnailImageName, thumbnailFile);
        por.setGeneralProgressListener(new S3ProgressListener() {
            @Override
            public void onPersistableTransfer(PersistableTransfer persistableTransfer) {

            }

            @Override
            public void progressChanged(ProgressEvent progressEvent) {
                if(progressEvent.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE) {
                    // Upload is completed!
                    Log.d(TAG, "Uploading file : " + thumbnailFile.getPath() + " COMPLETED!");
                    DbHelper.commitOrUpdateShot(shot);
                    NotificationHelper.notify("Shot uploaded!", "", NotificationHelper.UPLOADING_SHOT_NOTIFICATION_ID, null, null);
                }
            }
        });
        Log.d(TAG, "Uploading file : " + thumbnailFile.getPath());
        SpotShotApplication.getS3Client().putObject(por);
    }

}
