package tmtm.SpotShot.ui;

import android.text.SpannableString;
import android.text.style.StyleSpan;

import tmtm.SpotShot.SpotShotApplication;

/**
 * Created by ttaila on 5/16/15.
 */
public class TextUtil {

    public static SpannableString spanIt(String text) {
        // search for query on text
        int startIndex = 0;
        int endIndex = text.indexOf(" ");
        // spannable to show the search query
        SpannableString spanText = new SpannableString(text);
        if (startIndex > -1) {
            spanText.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), startIndex, endIndex, 0);
        }
        return spanText;
    }

    public static String capitalize(String str) {
        StringBuilder sb;
        sb = new StringBuilder(str);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }

}
