package tmtm.SpotShot.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import tmtm.SpotShot.R;
import tmtm.SpotShot.TimeUtil;
import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 4/25/15.
 */
public class SnapThumbnailImageView extends View {

    private Shot shot;
    private Paint borderPaint;
    private int borderColor;
    private float diameter;

    public SnapThumbnailImageView(Context context, Shot shot) {
        super(context);
        this.shot = shot;
        init();
    }

    public SnapThumbnailImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SnapThumbnailImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        borderColor = getResources().getColor(R.color.accent);
        borderPaint.setColor(borderColor);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // Account for padding
        float xpad = (float)(getPaddingLeft() + getPaddingRight());
        float ypad = (float)(getPaddingTop() + getPaddingBottom());

        float ww = (float)w - xpad;
        float hh = (float)h - ypad;

        // Figure out how big we can make the diameter of the circle.
        diameter = Math.min(ww, hh);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the arc for the time remaining indication
        RectF boundingOval = new RectF(0, 0, getMeasuredWidth(), getMeasuredHeight());
        int startingAngle = -90;
        int endingAngle = (int) (360 * TimeUtil.getPercentageOfTwentyFourHoursGoneRemaining(shot.getTimeCreated()));
        canvas.drawArc(boundingOval, startingAngle, -360+endingAngle, true, borderPaint);
    }


}
