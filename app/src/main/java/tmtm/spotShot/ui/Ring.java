package tmtm.SpotShot.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import tmtm.SpotShot.R;

/**
 * Created by ttaila on 6/11/15.
 */
public class Ring extends View {

    private int color;
    private float strokeWidth;
    private Paint paint;

    public Ring(Context context) {
        super(context);
    }

    public Ring(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.Ring,
                0, 0);

        try {
            color = a.getColor(R.styleable.Ring_ring_color, Color.WHITE);
            strokeWidth = a.getDimension(R.styleable.Ring_ring_stroke_width, 1.0f);
            init();
        } finally {
            a.recycle();
        }
    }

    public Ring(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the ring
        canvas.drawCircle(Math.min(getMeasuredWidth()/2, getMeasuredHeight()/2), Math.min(getMeasuredWidth()/2, getMeasuredHeight()/2), Math.min(getMeasuredWidth()/2, getMeasuredHeight()/2)-strokeWidth, paint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = Math.min(heightSize, widthSize);
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(heightSize, widthSize);
        } else {
            //Be whatever you want
            width = Math.min(heightSize, widthSize);
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = Math.min(heightSize, widthSize);
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(heightSize, widthSize);
        } else {
            //Be whatever you want
            height = Math.min(heightSize, widthSize);
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }


}
