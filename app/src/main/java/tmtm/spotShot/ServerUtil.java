package tmtm.SpotShot;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ttaila on 7/7/15.
 */
public abstract class ServerUtil {

    protected String TAG;

    public HttpURLConnection setupHttpURLConnection(String urlPath) throws IOException {
        URL url = new URL(urlPath);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestMethod("POST");

        return conn;
    }

    public void sendJsonRequest(JSONObject json, HttpURLConnection conn) throws IOException {
        OutputStream os = conn.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
        osw.write(json.toString());
        osw.flush();
        osw.close();
    }

    public StringBuilder getJsonResponse(HttpURLConnection conn) throws IOException {
        // Get response code
        int responseCode = conn.getResponseCode();
        Log.d(TAG, "The response code is: " + responseCode);

        // Get response
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        Log.d(TAG, "Response = " + sb.toString());
        return sb;
    }

    protected String buildFullUrl(String relativeUrl) {
        if(URLHelper.QA_ENABLED) {
            return URLHelper.BASE_QA_URL + relativeUrl;
        } else {
            return URLHelper.BASE_URL + relativeUrl;
        }
    }

}
