package tmtm.SpotShot;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by ttaila on 3/8/15.
 */
public class MediaHelper {

    public static Bitmap makeBitmapFromUrl(String url) {
        return BitmapFactory.decodeFile(url);
    }

}
