package tmtm.SpotShot;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import tmtm.SpotShot.activities.shot.ShotFragment;
import tmtm.SpotShot.activities.shot.ShotListParentFragment;
import tmtm.SpotShot.activities.users.UserProfileFragment;

/**
 * Created by ttaila on 11/15/14.
 */
public class MyPagerAdapter extends FragmentPagerAdapter {

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 2:
                // SnapFragment
                return new ShotListParentFragment();
            case 1:
                // SnapFragment
                return new ShotFragment();
            case 0:
                // FollowerListFragment
                return new UserProfileFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public String getFragmentTag(int viewPagerId, int fragmentPosition)
    {
        return "android:switcher:" + viewPagerId + ":" + fragmentPosition;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
