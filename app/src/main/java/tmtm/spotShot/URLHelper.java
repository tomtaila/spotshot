package tmtm.SpotShot;

/**
 * Created by ttaila on 3/29/15.
 */
public class URLHelper {

    public static final boolean QA_ENABLED = false;

    public static final String BASE_URL = "https://shrouded-spire-3505.herokuapp.com";
    public static final String BASE_QA_URL = "https://pacific-shore-2065.herokuapp.com";
    // SNAPS
    public static final String CREATE_SNAP_URL = "/shots";
    public static final String GET_SNAPS_URL = "/shots/all";
    public static final String GET_SNAPS_FOR_USER_URL = "/shots/user";
    public static final String GET_NEWEST_SNAPS_URL = "/shots/newest";
    public static final String GET_TOP_SNAPS_URL = "/shots/top";
    public static final String LIKE_SNAP_URL = "/shots/like";
    public static final String FLAG_SHOT_DELETED = "/shots/flag_deleted";
    // USERS
    public static final String LOGIN_URL = "/login";
    public static final String CREATE_USER_URL = "/users";
    public static final String SEARCH_USER_URL = "/users/search";
    // LIKES
    public static final String GET_LIKES_URL = "/likes";
    public static final String DISLIKE_SNAP_URL = "/shots/dislike";
    // FRIENDS
    public static final String CREATE_FRIEND_URL = "/friends";
    public static final String GET_FRIENDS_URL = "/friends/all";
    public static final String GET_FRIENDS_AND_REQUESTS_URL = "/friends/friends_and_requests";
    public static final String GET_FRIEND_REQUESTS_URL = "/friends/requests";
    public static final String DELETE_FRIEND_REQUEST_URL = "/friends/delete_friend_request";
    public static final String DELETE_FRIEND_URL = "/friends/delete";
    public static final String ACCEPT_FRIEND_URL = "/friends/accept";
    public static final String DECLINE_FRIEND_URL = "/friends/decline";
    // CHATS
    public static final String GET_CHATS_URL = "/chats";
    // MESSAGES
    public static final String GET_MESSAGES_URL = "/messages";
    public static final String CREATE_MESSAGE_URL = "/messages/create";
    public static final String MARK_MESSAGES_FOR_CHAT_AS_VIEWED = "/messages/mark_messages_for_chat_as_viewed";
    // DEVICES
    public static final String REGISTER_DEVICE_URL = "/device/registerOrUpdate";
    public static final String DELETE_DEVICE_URL = "/device/delete";

}
