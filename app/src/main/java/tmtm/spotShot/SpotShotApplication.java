package tmtm.SpotShot;

import android.content.SharedPreferences;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bugsnag.android.Bugsnag;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.orm.SugarApp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.activities.likes.ServerLikeUtil;
import tmtm.SpotShot.activities.shot.ShotListFragment;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.model.Like;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 3/3/15.
 */
public class SpotShotApplication extends SugarApp {

    private static final String TAG = "SpotShotApplication";

    public static int mUnitsOfDistance = 0;
    public static int MILES = 0;
    public static int KM = 1;
    public static final double MILE_IN_METRES = 1609.34;
    public static final double KM_IN_METRES = 1000.00;

    public static String APP_ROOT_MEDIA_PATH;

    private static SharedPreferences sharedPreferences;
    private static SpotShotApplication instance;

    private static User currentUser;
    private static AmazonS3Client s3Client;

    private static Tracker tracker;

    // Cache of shot objects that are currently uploading
    private static List<Shot> shotsUploading;

    public SpotShotApplication()
    {
        super();
    }

    public static SpotShotApplication getInstance()
    {
        if (instance == null)
        {
            instance = new SpotShotApplication();
        }
        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        sharedPreferences = getSharedPreferences(Settings.FILE_NAME, MODE_PRIVATE);
        Log.d(TAG, "" + sharedPreferences.getString(Settings.REG_ID_KEY, "DEFAULT"));
        APP_ROOT_MEDIA_PATH = getFilesDir().getPath();
        File mediaStorageDir = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH);
        if (!mediaStorageDir.exists())
        {
            mediaStorageDir.mkdir();
        }

        // Initialise bugsnag
        Bugsnag.init(this);

        // Start location services
        LocationHelper.getInstance();
        // Create AmazonS3Client
        createS3Client();

        shotsUploading = new ArrayList<>();
    }

    public static void createS3Client() {
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                SpotShotApplication.getInstance(), // Context,
                AWSHelper.APP_ID,
                AWSHelper.IDENTITY_POOL_ID, // Identity Pool ID
                AWSHelper.UNAUTH_ROLE,
                AWSHelper.AUTH_ROLE,
                Regions.US_EAST_1 // Region
        );

        s3Client = new AmazonS3Client(credentialsProvider);
    }

    public static AmazonS3Client getS3Client()
    {
        return s3Client;
    }

    public static SharedPreferences getSharedPreferences()
    {
        if(sharedPreferences == null) {

        }
        return sharedPreferences;
    }

    public static void setUnitsOfDistance(int unitsOfDistance)
    {
        mUnitsOfDistance = unitsOfDistance;
    }

    /**
     * Gets the default {@link Tracker} for this {@link android.app.Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            tracker = analytics.newTracker(R.xml.analytics_tracker);
        }
        return tracker;
    }

    public static List<Shot> getAllShotsFromDatabase(int sortByFlag)
    {
        // Filter out ones outside of radius settings
        int distance = SpotShotApplication.getSharedPreferences().getInt("maxRadius", 50);
        int maxHoursAgo = SpotShotApplication.getSharedPreferences().getInt("maxHoursAgo", 24);
        List<Shot> shotList = DbHelper.getShotsWithnRadiusAndTime(distance, maxHoursAgo, sortByFlag);

        return shotList;
    }

    public static List<Shot> getAllShotsFromDatabase(int sortByFlag, int pageNumber)
    {
        return getAllShotsFromDatabase(sortByFlag, pageNumber, false);
    }

    public static List<Shot> getAllShotsFromDatabase(int sortByFlag, int pageNumber, boolean startAtZero)
    {
        int startIndex = pageNumber* ShotListFragment.SHOTS_PER_PAGE;
        List<Shot> shotList = getAllShotsFromDatabase(sortByFlag);
        if(startIndex >= shotList.size()) {
            int updatedPageNumber = (int) (Math.ceil(((double) shotList.size())/ShotListFragment.SHOTS_PER_PAGE) * ShotListFragment.SHOTS_PER_PAGE);
            SpotShotApplication.getSharedPreferences().edit().putInt("pageNum", updatedPageNumber);
            return shotList;
        }

        if(startIndex+ ShotListFragment.SHOTS_PER_PAGE > shotList.size()) {
            if(startAtZero) {
                shotList = shotList.subList(0, shotList.size());
            } else {
                shotList = shotList.subList(startIndex, shotList.size());
            }
        } else {
            if(startAtZero) {
                shotList = shotList.subList(0, startIndex+ ShotListFragment.SHOTS_PER_PAGE);
            } else {
                shotList = shotList.subList(startIndex, startIndex+ ShotListFragment.SHOTS_PER_PAGE);
            }
        }
        return shotList;
    }

    public static void storeUser(User user)
    {
        DbHelper.commitOrUpdateUser(user);
    }

    public static void storeLikes(ArrayList<Like> likes)
    {
        DbHelper.commitLikes(likes);
    }

    public static void storeLike(Like like) {
        DbHelper.commitLike(like);
    }

    public static void removeLike(Like like)
    {
        DbHelper.removeLike(like);
    }

    public static float convertDpToPx(int dimenResourceId) {
        return getInstance().getResources().getDimensionPixelSize(dimenResourceId);
    }

    public static User getCurrentUser()
    {
        if(currentUser == null) {
            currentUser = DbHelper.getUser(sharedPreferences.getString("name", ""));
        }
        return currentUser;
    }

    public static void setCurrentUser(User user)
    {
        currentUser = user;
    }

    public static boolean loggedIn()
    {
        return sharedPreferences.getBoolean(Settings.LOGGED_IN_KEY, false);
    }

    public static void clearLoginDetailsFromSharedPreferences()
    {
        sharedPreferences.edit().clear().commit();
    }

    public void pullCurrentUserLikes()
    {
        ServerLikeUtil serverLikeUtil = new ServerLikeUtil(this);
        serverLikeUtil.getLikes(currentUser);
    }

    public List<Like> getCurrentUserLikes()
    {
        return DbHelper.getLikesForUser(currentUser);
    }

    public static List<Shot> getShotsUploading() {
        return shotsUploading;
    }
}
