package tmtm.SpotShot.database;

import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.TimeUtil;
import tmtm.SpotShot.activities.shot.ShotListFragment;
import tmtm.SpotShot.model.Friend;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Like;
import tmtm.SpotShot.model.Message;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.model.User;

/**
 * Created by mwszedybyl on 4/26/15.
 */
public class DbHelper
{
    private static final String TAG = "DBHelper";
     /*****************************************************************************************\
                                  User CRUD
     \*****************************************************************************************/

    public static void commitOrUpdateUser(User user)
    {
        List<User> users = new ArrayList<>();
        users = User.find(User.class, "name = ?", user.getName());
        // if shot exists
        if(!users.isEmpty()) {
            User u = users.get(0);
            u.setScore(user.getScore());
            u.save();
        } else {
            user.save();
        }
    }

    public static User getUser(String name)
    {
        User u = null;
        List<User> result = User.find(User.class, "name = ?", name);
        if(!result.isEmpty()) {
            u = result.get(0);
        }
        return u;
    }

    public static void removeUser(User user)
    {
        List<User> result = new ArrayList<>();
        result = User.find(User.class, "uuid = ?", user.getUuid());
        if(!result.isEmpty()) {
            result.get(0).delete();
        }
    }

    /*****************************************************************************************\

                                     Shot CRUD

    \*****************************************************************************************/


    public static void commitOrUpdateShots(List<Shot> shotList)
    {
        for(Shot s : shotList) {
            commitOrUpdateShot(s);
        }
    }

    public static void commitOrUpdateShot(Shot shot)
    {
        List<Shot> shots = new ArrayList<>();
        shots = Shot.find(Shot.class, "uuid = ?", shot.getUuid());
        // if shot exists
        if(!shots.isEmpty()) {
            Shot s = shots.get(0);
            if(shot.isDeleted()) {
                s.delete();
            } else {
                s.setScore(shot.getScore());
                if(shot.isMediaDownloaded())
                {
                    s.setMediaDownloaded(true);
                }
                s.setTimeCreated(shot.getTimeCreated());
                s.setSuccessfullyUploaded(shot.isSuccessfullyUploaded());
                shot.getLocation();
                s.setDistance(shot.getDistance());
                s.save();
            }
        } else {
            if(!shot.isDeleted()) {
                shot.save();
            }
        }
    }

    public static List<Shot> getAllShots()
    {
        return Shot.listAll(Shot.class);
    }

    public static List<Shot> getShotsWithinRadius(int radius) {
        return Shot.find(Shot.class, "distance <= ?", String.valueOf(radius));
    }

    public static List<Shot> getShotsWithinHoursAgo(int hoursAgo) {
        return Shot.find(Shot.class, "time_created >= ?", String.valueOf(TimeUtil.getTimeStampHoursAgo(hoursAgo)));
    }

    public static List<Shot> getShotsWithnRadiusAndTime(int radius, int hoursAgo, int sortByFlag) {
        String[] args = { String.valueOf(TimeUtil.getTimeStampHoursAgo(hoursAgo)), String.valueOf(radius) };
        List<Shot> result = new ArrayList<>();
        if(sortByFlag == ShotListFragment.SORT_BY_NEWEST_FLAG) {
            result = Shot.find(Shot.class, "time_created >= ? and distance <= ?", args, "", "time_created desc", "");
        } else if(sortByFlag == ShotListFragment.SORT_BY_TOP_FLAG) {
            result = Shot.find(Shot.class, "time_created >= ? and distance <= ?", args, "", "score desc", "");
        }
        return result;
    }

    public static List<Shot> getUserShots(User user)
    {
        return Shot.find(Shot.class, "user_uuid = ?", user.getUuid());
    }

    public static void removeExpiredShots() {
        Shot.deleteAll(Shot.class, "time_created < ?", String.valueOf(TimeUtil.getTimeStampHoursAgo(24)));
    }

    public static void removeExpiredMedia(){
        loadRunnable.run();
    }

    private static Runnable loadRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            List<Shot> expiredShots = Shot.find(Shot.class, "time_created < ?", String.valueOf(TimeUtil.getTimeStampHoursAgo(24)));
            for(int i = 0 ; i< expiredShots.size(); i++)
            {
                File fdelete = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, expiredShots.get(i).getUrl());
                if (fdelete.exists())
                {
                    if (fdelete.delete())
                    {
                        System.out.println("file Deleted");
                    } else
                    {
                        System.out.println("file not Deleted");
                    }
                }
            }
        }
    };


    public static void removeShots(List<Shot> shotList)
    {
        for(Shot s : shotList) {
            removeShot(s);
        }
    }

    public static void removeShot(Shot shot)
    {
        List<Shot> result = new ArrayList<>();
        result = Shot.find(Shot.class, "uuid = ?", shot.getUuid());
        if(!result.isEmpty()) {
            result.get(0).delete();
        }
    }

    public static void removeAllShots() {
        Shot.deleteAll(Shot.class);
    }

    /*****************************************************************************************\

                                            Messages CRUD

     \*****************************************************************************************/

    public static void commitOrUpdateMessages(List<Message> messageList)
    {
        for(Message m : messageList) {
            commitOrUpdateMessage(m);
        }
    }

    public static void commitOrUpdateMessage(Message message)
    {
        List<Message> messages = new ArrayList<>();
        messages = Message.find(Message.class, "uuid = ?", message.getUuid());
        if(messages.isEmpty()) {
            message.save();
        } else {
            Message m = messages.get(0);
            m.setViewed(message.isViewed());
            m.save();
        }
    }

    public static List<Message> getMessagesForChat(Chat c)
    {
        return Message.find(Message.class, "chat_uuid = ?", c.getUuid());
    }

    public static void removeMessage(Message message)
    {
        List<Message> result = new ArrayList<>();
        result = Message.find(Message.class, "uuid = ?", message.getUuid());
        if(!result.isEmpty()) {
            result.get(0).delete();
        }
    }

    public static void markSentMessagesAsViewed(Chat ch) {
        List<Message> messages = markSentMessageAsViewed(ch);
        for(Message m : messages) {
            m.setViewed(true);
            m.save();
        }
    }

    public static List<Message> markSentMessageAsViewed(Chat ch) {
        List<Message> messages = Message.find(Message.class, "chat_uuid = ? and sender_uuid = ?", ch.getUuid(), SpotShotApplication.getCurrentUser().getUuid());
        return messages;
    }

    public static Message getMostRecentMessageForChat(Chat ch) {
        List<Message> messages = Message.find(Message.class, "chat_uuid = ?", ch.getUuid());
        if(!messages.isEmpty()) {
            return messages.get(messages.size()-1);
        } else {
            return null;
        }
    }

    /*****************************************************************************************\
                                         Likes CRUD

     \*****************************************************************************************/

    public static void commitLikes(List<Like> likeList)
    {
        for(Like l : likeList) {
            l.save();
        }
    }

    public static void commitLike(Like like)
    {
        like.save();
    }

    public static List<Like> getAllLikes()
    {
        return Like.listAll(Like.class);
    }

    public static List<Like> getLikesForUser(User user)
    {
        return Like.find(Like.class, "user_uuid = ?", user.getUuid());
    }

    public static void removeLike(Like like)
    {
        List<Like> result = new ArrayList<>();
        result = Like.find(Like.class, "like_uuid = ?", like.getLikeUuid());
        if(!result.isEmpty()) {
            result.get(0).delete();
        }
    }

    /*****************************************************************************************\
                                             Chats CRUD
     \*****************************************************************************************/

    public static void commitOrUpdateChats(List<Chat> chatList)
    {
        for(Chat ch : chatList) {
            commitOrUpdateChat(ch);
        }
    }

    public static void removeInvalidChats(List<Chat> serverSideChats, User user) {
        List<Chat> localDbChats = getChatsForUser(user);
        for(Chat ch : localDbChats) {
            if(!serverSideChats.contains(ch)) {
                removeChat(ch);
            }
        }
    }

    public static void commitOrUpdateChat(Chat chat)
    {
        List<Chat> chats = new ArrayList<>();
        chats = Chat.find(Chat.class, "uuid = ?", chat.getUuid());
        // if chat doesnt exist
        if(chats.isEmpty()) {
            chat.save();
        }
    }

    public static Chat getChat(String chatUuid) {
        List<Chat> chats = Chat.find(Chat.class, "uuid = ?", chatUuid);
        Chat result = null;
        if(!chats.isEmpty()) {
            result = chats.get(0);
        }
        return result;
    }

    public static List<Chat> getChatsForUser(User user)
    {
        return Chat.find(Chat.class, "user_a_uuid = ? or user_b_uuid = ?", user.getUuid(), user.getUuid());
    }

    public static void removeChat(Chat chat)
    {
        List<Chat> result = new ArrayList<>();
        result = Chat.find(Chat.class, "user_a_uuid = ?", chat.getUserAUuid());
        if(result.isEmpty()) {
            result = Chat.find(Chat.class, "user_b_uuid = ?", chat.getUserAUuid());
        }
        if(!result.isEmpty()) {
            Log.d(TAG, "Chat deleted");
            result.get(0).delete();
        }
    }

     /*****************************************************************************************\
                                        Friends CRUD

     \*****************************************************************************************/

     public static void commitOrUpdateFriends(List<Friend> friends)
     {
         for(Friend f : friends) {
             commitOrUpdateFriend(f);
         }
     }

    public static void commitOrUpdateFriend(Friend friend)
    {
        List<Friend> friends = new ArrayList<>();
        friends = Friend.find(Friend.class, "uuid = ?", friend.getUuid());
        // if shot exists
        if(!friends.isEmpty()) {
            Friend f = friends.get(0);
            f.setRequest(friend.isRequest());
            f.save();
        } else {
            friend.save();
        }
    }

    // Remove friend records in local db if they do not exist server side
    public static void removeInvalidFriends(List<Friend> serverSideFriends, User user) {
        List<Friend> localDbFriendsAndRequests = getFriendsAndRequestsForUser(user);
        for(Friend f : localDbFriendsAndRequests) {
            if(!serverSideFriends.contains(f)) {
                removeFriend(f);
            }
        }
    }

    public static List<Friend> getFriendsAndRequestsForUser(User user)
    {
        List<Friend> friends = getFriendsForUser(user);
        List<Friend> requests = getFriendsRequestsForUser(user);
        friends.addAll(requests);
        return friends;
    }

    public static List<Friend> getFriendsForUser(User user)
    {
        return Friend.find(Friend.class, "user_uuid = ? and request = ?", user.getUuid(), "0");
    }

    public static List<Friend> getFriendsRequestsForUser(User user)
    {
        return Friend.find(Friend.class, "friend_uuid = ? and request = ?", user.getUuid(), "1");
    }

    public static List<Friend> getSentFriendsRequestsForUser(User user)
    {
        return Friend.find(Friend.class, "user_uuid = ? and request = ?", user.getUuid(), "1");
    }

    public static void removeFriend(Friend friend)
    {
        List<Friend> result = new ArrayList<>();
        String[] args = { friend.getUserUuid(), friend.getFriendUuid() };
        result = Friend.find(Friend.class, "user_uuid = ? and friend_uuid = ?", args);
        if(!result.isEmpty()) {
            Log.d(TAG, "Friend deleted");
            result.get(0).delete();
        }
    }

    public void findContacts(){
//        ContentResolver mContentResolver = getContentResolver();
//        Cursor cursor = mContentResolver.query(
//                ContactsContract.RawContacts.CONTENT_URI,
//                new String[]{ContactsContract.RawContacts._ID, ContactsContract.RawContacts.ACCOUNT_TYPE},
//                ContactsContract.RawContacts.ACCOUNT_TYPE + " <> 'com.anddroid.contacts.sim' "
//                        + " AND " + ContactsContract.RawContacts.ACCOUNT_TYPE + " <> 'com.google' " //if you don't want to google contacts also
//                ,
//                null,
//                null);
    }

}