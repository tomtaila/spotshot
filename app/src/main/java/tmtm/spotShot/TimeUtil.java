package tmtm.SpotShot;

/**
 * Created by ttaila on 4/4/15.
 */
public class TimeUtil {

    public static long TWENTY_FOUR_HOURS_IN_MILLIS = 86400000;
    public static long HOUR_IN_MILLIS = 3600000;

    public static String generateTimeAgoString(long timeAgo) {
        String str;

        long minutesAgo = (System.currentTimeMillis() - timeAgo) / (1000 * 60);
        if(minutesAgo > 60) {
            int hoursAgo = (int) (minutesAgo / 60);
            if(hoursAgo > 24) {
                int daysAgo = hoursAgo/24;
                str = daysAgo + "d " + (hoursAgo%24) + "h ago";
            } else {
                str = hoursAgo + "h ago";
            }
        } else {
            str = minutesAgo + "m ago";
        }

        return str;
    }

    public static float getPercentageOfTwentyFourHoursGoneRemaining(long timeAgo) {
        long minutesAgo = (System.currentTimeMillis() - timeAgo);

        return (float) ((float) minutesAgo / (float) TWENTY_FOUR_HOURS_IN_MILLIS);
    }

    public static long getTimeStampHoursAgo(int numHours){
        return System.currentTimeMillis() - (numHours * HOUR_IN_MILLIS);
    }

    public static long getTimeAgoInHours(long timeAgo) {
        return (int) (System.currentTimeMillis() - timeAgo) / HOUR_IN_MILLIS;
    }

}
