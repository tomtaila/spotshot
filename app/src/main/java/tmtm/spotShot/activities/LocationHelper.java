package tmtm.SpotShot.activities;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.Places;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.shot.GetShotsTaskListener;
import tmtm.SpotShot.activities.shot.ServerShotUtil;
import tmtm.SpotShot.activities.shot.ShotListFragment;
import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 3/7/15.
 */
public class LocationHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, GetShotsTaskListener
{

    public static final String TAG = "LocationHelper";

    private static LocationHelper instance;
    private static GoogleApiClient googleApiClient;
    private static Location lastLocation;
    private static String lastUpdateTime;
    private static LocationRequest locationRequest;
    private static boolean requestingLocationUpdates = true;
    private static List<PlaceLikelihood> placeLikelihoods;

    private LocationHelper() {
        buildGoogleApiClient();
    }

    public static LocationHelper getInstance() {
        if(instance == null) {
            instance = new LocationHelper();
        }
        return instance;
    }

    public static List<PlaceLikelihood> getPlaceLikelihoods() {
        return placeLikelihoods;
    }

    public static void setPlaceLikelihoods(List<PlaceLikelihood> placeLikelihoods) {
        LocationHelper.placeLikelihoods = placeLikelihoods;
    }

    private synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Attempting to build GoogleApiClient object");
        if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(SpotShotApplication.getInstance()) == ConnectionResult.SUCCESS) {
            Log.i(TAG, "Google play services are installed and available on this device");
            googleApiClient = new GoogleApiClient.Builder(SpotShotApplication.getInstance())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            googleApiClient.connect();
            Log.i(TAG, "Building GoogleApiClient object successful, GoogleApiClient connected");
        } else {
//       TODO: Implement: GooglePlayServicesUtil.getErrorDialog(connectionResult, Activity, requestCode);
        }
    }

    public static GoogleApiClient getGoogleApiClient(){
        return googleApiClient;
    }

    public static boolean isRequestingLocationUpdates() {
        return requestingLocationUpdates;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if(lastLocation == null) {
            lastLocation = new Location("");
            lastLocation.setLatitude(Double.valueOf(SpotShotApplication.getSharedPreferences().getString("lat", "42.331427")));
            lastLocation.setLongitude(Double.valueOf(SpotShotApplication.getSharedPreferences().getString("long", "-83.045754")));
        }

        if (requestingLocationUpdates) {
            createLocationRequest();
            startLocationUpdates();
            ServerShotUtil serverShotUtil = new ServerShotUtil(SpotShotApplication.getInstance(), this);
            serverShotUtil.getShots(lastLocation, SpotShotApplication.getSharedPreferences().getInt("maxRadius", 50), ShotListFragment.SORT_BY_TOP_FLAG);
        }
    }

    protected void startLocationUpdates() {
        Log.i(TAG, "Starting location updates");
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(120000);
        locationRequest.setFastestInterval(119000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        SpotShotApplication.getSharedPreferences().edit().putString("lat", String.valueOf(lastLocation.getLatitude())).commit();
        SpotShotApplication.getSharedPreferences().edit().putString("long", String.valueOf(lastLocation.getLongitude())).commit();
        lastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        Log.i(TAG, "lastLocation.latitude = " + lastLocation.getLatitude() + ", lastLocation.longitude = " + lastLocation.getLongitude());
    }


    protected void stopLocationUpdates() {
        Log.i(TAG, "Stopping location updates");
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    // Get the last recorded location - May return null
    public static Location getLastLocation() {
        if(lastLocation == null) {
            lastLocation = new Location("");
            lastLocation.setLatitude(Double.valueOf(SpotShotApplication.getSharedPreferences().getString("lat", "0.00")));
            lastLocation.setLongitude(Double.valueOf(SpotShotApplication.getSharedPreferences().getString("lng", "0.00")));
        }
        return lastLocation;
    }

    public static Location generateLocationFromString(String locationStr) {
        double lat = Double.valueOf(locationStr.substring(0, locationStr.indexOf(" ")));
        double lng = Double.valueOf(locationStr.substring(locationStr.indexOf(" "), locationStr.length()));
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(lng);
        return location;
    }

    @Override
    public void doOnComplete(List<Shot> shotList) {
    }
}
