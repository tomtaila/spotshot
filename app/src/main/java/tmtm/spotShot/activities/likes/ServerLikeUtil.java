package tmtm.SpotShot.activities.likes;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.ServerUtil;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.model.Like;
import tmtm.SpotShot.model.User;

/**CreateLikeTask
 * Created by ttaila on 3/29/15.
 */
public class ServerLikeUtil extends ServerUtil {

    private static final String TAG = "ServerLikeUtil";

    private Context context;

    public ServerLikeUtil(Context context) {
        this.context = context;
    }

    // Get all snaps from server side db within radius of location
    public void getLikes(User user) {
        new GetLikesTask().execute(user);
    }
    // AsyncTask for creating snap db record for server side db
    private class GetLikesTask extends AsyncTask<User, Void, List<Like>> {

        @Override
        protected List<Like> doInBackground(User... params) {
            User user = params[0];
            List<Like> likeList = new ArrayList<>();

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "getLikesTask.doInBackground() called");

                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.GET_LIKES_URL));

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("user_uuid", user.getUuid());

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONArray jsonArray = new JSONArray(sb.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Like like = new Like();
                        String likeUuid = jsonObject.getString("like_uuid");
                        String userUuid = jsonObject.getString("user_uuid");
                        String shotUuid = jsonObject.getString("shot_uuid");

                        like.setLikeUuid(likeUuid);
                        like.setUserUuid(userUuid);
                        like.setShotUuid(shotUuid);
                        likeList.add(like);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            return likeList;
        }

        @Override
        protected void onPostExecute(List<Like> likeList) {
            ArrayList<Like> arrayLikes = new ArrayList<>(likeList);
            // TODO Commit or update likes instead of using application class
            SpotShotApplication.getInstance().storeLikes(arrayLikes);
        }
    }

}
