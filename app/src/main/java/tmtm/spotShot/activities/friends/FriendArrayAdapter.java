package tmtm.SpotShot.activities.friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Friend;
import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 6/9/15.
 */
public class FriendArrayAdapter extends ArrayAdapter<Friend>
{
    private static final String TAG = "FriendArrayAdapter";

    private Context context;
    private List<Friend> friendsList;

    public FriendArrayAdapter(Context context, int resource, List<Friend> friendList)
    {
        super(context, resource);
        this.context = context;
        this.friendsList = friendList;
    }

    @Override
    public Friend getItem(int position)
    {
        return friendsList.get(position);
    }

    @Override
    public int getCount()
    {
        return friendsList.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final Friend friend = getItem(position);
        FriendRequestItemViewHolder holder = null;

        if(convertView == null || ((FriendRequestItemViewHolder)convertView.getTag()).needsInflate) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.friend_item, null);
            holder = new FriendRequestItemViewHolder(convertView);
            holder.friendNameTV = (TextView) convertView.findViewById(R.id.friend_name);
            holder.deleteBtn = (ImageButton) convertView.findViewById(R.id.decline_btn);
            convertView.setTag(holder);
            final View convertViewDup = convertView;
            holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bugsnag.leaveBreadcrumb("Delete friend button clicked");
//                    TODO deleteCell(convertViewDup, position, false);
                    Toast.makeText(getContext(), "Friend Deleted!", Toast.LENGTH_SHORT).show();

                    final User currentUser = SpotShotApplication.getCurrentUser();
                    RequestParams params = new RequestParams();
                    params.put("user_uuid", currentUser.getUuid());
                    params.put("friend_uuid", friend.getFriendUuid());
                    params.put("request", false);
                    SpotShotRestClient.post(URLHelper.DELETE_FRIEND_URL, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            // If the response is JSONObject
                            try {
                                if (response.getBoolean("success")) {
                                    DbHelper.removeFriend(friend);
                                    DbHelper.removeChat(new Chat(friend.getFriendUuid(), friend.getUserUuid()));

                                    friendsList.clear();
                                    friendsList.addAll(DbHelper.getFriendsForUser(SpotShotApplication.getCurrentUser()));
                                    notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            });
        } else {
            holder = (FriendRequestItemViewHolder) convertView.getTag();
        }

        populateFriendItemView(holder, friend);

        return convertView;
    }

    private void populateFriendItemView(FriendRequestItemViewHolder viewHolder, Friend friend) {
        viewHolder.friendNameTV.setText(friend.getFriendUserName());
    }

    private class FriendRequestItemViewHolder extends RecyclerView.ViewHolder {

        private TextView friendNameTV;
        private boolean needsInflate;
        private ImageButton deleteBtn;

        public FriendRequestItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    private void deleteCell(final View v, final int index, boolean accepted) {
        final Animation.AnimationListener al = new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                friendsList.remove(index);

                FriendRequestItemViewHolder vh = (FriendRequestItemViewHolder)v.getTag();
                vh.needsInflate = true;

                notifyDataSetChanged();
            }
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationStart(Animation animation) {}
        };

        collapse(v, al);
    }

    private void collapse(final View v, Animation.AnimationListener al) {
        final int initialHeight = v.getMeasuredHeight();

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                }
                else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        if (al!=null) {
            anim.setAnimationListener(al);
        }
        anim.setDuration(300);
        v.startAnimation(anim);
    }

}
