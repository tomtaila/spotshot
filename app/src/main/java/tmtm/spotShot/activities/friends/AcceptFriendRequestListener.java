package tmtm.SpotShot.activities.friends;

import tmtm.SpotShot.model.Friend;

/**
 * Created by ttaila on 5/3/15.
 */
public interface AcceptFriendRequestListener {

    void acceptFollowRequestOnComplete(boolean success, Friend friend);

}
