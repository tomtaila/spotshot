package tmtm.SpotShot.activities.friends;

/**
 * Created by ttaila on 4/11/15.
 */
public interface CreateFriendTaskListener {
    public void createFriendOnComplete(Boolean success);
}
