package tmtm.SpotShot.activities.friends;

import android.app.Fragment;

import tmtm.SpotShot.activities.ComingSoonFragment;
import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by ttaila on 6/9/15.
 */
public class FriendsListActivity extends SingleFragmentActivity {
    @Override
    public Fragment createFragment() {
//        return new FriendListFragment(); TODO
        return new ComingSoonFragment();
    }
}
