package tmtm.SpotShot.activities.friends;

import java.util.List;

import tmtm.SpotShot.model.Friend;

/**
 * Created by ttaila on 5/1/15.
 */
public interface GetFriendListener {

    void assignFriendsList(List<Friend> friendList);

}
