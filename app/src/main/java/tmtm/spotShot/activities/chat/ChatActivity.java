package tmtm.SpotShot.activities.chat;

import android.app.Fragment;
import android.content.Intent;

import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.SingleFragmentActivity;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.ui.TextUtil;

/**
 * Created by ttaila on 6/12/15.
 */
public class ChatActivity extends SingleFragmentActivity {

    @Override
    public Fragment createFragment() {
        return new ChatFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        Chat chat = getIntent().getParcelableExtra("chat");
        if(chat.getUserAName().equals(SpotShotApplication.getCurrentUser().getName())) {
            getActionBar().setTitle(TextUtil.capitalize(chat.getUserBName()));
        } else {
            getActionBar().setTitle(TextUtil.capitalize(chat.getUserAName()));
        }
    }
}
