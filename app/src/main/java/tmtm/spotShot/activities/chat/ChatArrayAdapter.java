package tmtm.SpotShot.activities.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.TimeUtil;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Message;
import tmtm.SpotShot.ui.TextUtil;

/**
 * Created by mwszedybyl on 3/5/15.
 */
public class ChatArrayAdapter extends ArrayAdapter<Chat>
{
    private static final String TAG = "ChatArrayAdapter";

    private Context context;
    private List<Chat> chatList;

    public ChatArrayAdapter(Context context, int resource, List<Chat> chatList)
    {
        super(context, resource);
        this.context = context;
        this.chatList = chatList;
    }

    @Override
    public Chat getItem(int position)
    {
        return chatList.get(position);
    }

    @Override
    public int getCount()
    {
        return chatList.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Chat chat = getItem(position);
        ChatItemViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.chat_message_item, null);
            holder = new ChatItemViewHolder(convertView);
            holder.friendNameTV = (TextView) convertView.findViewById(R.id.friend_name);
            holder.chatIcon = (ImageView) convertView.findViewById(R.id.chat_icon);
            holder.timeTV = (TextView) convertView.findViewById(R.id.time);
            holder.newTV = (TextView) convertView.findViewById(R.id.new_flag);
            holder.viewedTV = (TextView) convertView.findViewById(R.id.viewed_flag);
            convertView.setTag(holder);
        } else {
            holder = (ChatItemViewHolder) convertView.getTag();
        }

        populateChatItemView(holder, chat);

        return convertView;
    }

    private void populateChatItemView(ChatItemViewHolder viewHolder, Chat chat) {
        if(SpotShotApplication.getCurrentUser().getName().equals(chat.getUserAName())) {
            viewHolder.friendNameTV.setText(TextUtil.capitalize(chat.getUserBName()));
        } else {
            viewHolder.friendNameTV.setText(TextUtil.capitalize(chat.getUserAName()));
        }

        // get most recent message from chat and change chatIcon based on that messages properties
        Message mostRecentMessage = DbHelper.getMostRecentMessageForChat(chat);

        if(mostRecentMessage != null) {
            viewHolder.timeTV.setText(TimeUtil.generateTimeAgoString(mostRecentMessage.getTimeSent()));

            // if most recent message was sent by the current user
            if(mostRecentMessage.getSenderUuid().equals(SpotShotApplication.getCurrentUser().getUuid())) {
                // if the most recent message has been viewed
                switch (mostRecentMessage.getType()) {
                    case Message.TEXT:
                        viewHolder.chatIcon.setImageResource(R.drawable.chat_list_icon_sent);
                        break;
                    case Message.LOCATION:
                        viewHolder.chatIcon.setImageResource(R.drawable.logo_32_sent);
                        break;
                    case Message.PHOTO:
                        viewHolder.chatIcon.setImageResource(R.drawable.chat_icon_camera_sent);
                        break;
                    case Message.VIDEO:
                        viewHolder.chatIcon.setImageResource(R.drawable.chat_icon_camera_sent);
                        break;
                }

                viewHolder.newTV.setVisibility(View.GONE);

                // if the most recent message has been viewed
                if(mostRecentMessage.isViewed()) {
                    viewHolder.viewedTV.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.viewedTV.setVisibility(View.GONE);
                }
            } else {
                switch (mostRecentMessage.getType()) {
                    case Message.TEXT:
                        viewHolder.chatIcon.setImageResource(R.drawable.chat_icon_received);
                        break;
                    case Message.LOCATION:
                        viewHolder.chatIcon.setImageResource(R.drawable.pink_logo_32_received);
                        break;
                    case Message.PHOTO:
                        viewHolder.chatIcon.setImageResource(R.drawable.pink_camera_32_received);
                        break;
                    case Message.VIDEO:
                        viewHolder.chatIcon.setImageResource(R.drawable.pink_camera_32_received);
                        break;
                }

                viewHolder.viewedTV.setVisibility(View.GONE);

                // if the most recent message has been viewed
                if(mostRecentMessage.isViewed()) {
                    viewHolder.newTV.setVisibility(View.GONE);
                } else {
                    viewHolder.newTV.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private class ChatItemViewHolder extends RecyclerView.ViewHolder {

        private TextView friendNameTV;
        private ImageView chatIcon;
        private TextView timeTV;
        private TextView newTV;
        private TextView viewedTV;

        public ChatItemViewHolder(View itemView) {
            super(itemView);
        }
    }

}

