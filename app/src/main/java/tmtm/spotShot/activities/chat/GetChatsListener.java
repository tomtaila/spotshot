package tmtm.SpotShot.activities.chat;

import java.util.List;

import tmtm.SpotShot.model.Chat;

/**
 * Created by ttaila on 6/9/15.
 */
public interface GetChatsListener {

    void doOnGetChatsComplete(List<Chat> chats);

}
