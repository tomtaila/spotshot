package tmtm.SpotShot.activities.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.model.Message;

/**
 * Created by ttaila on 6/12/15.
 */
public class MessageArrayAdapter extends ArrayAdapter<Message> {

    private static final String TAG = "MessageArrayAdapter";

    private Context context;
    private List<Message> messages;

    public MessageArrayAdapter(Context context, int resource, List<Message> messages)
    {
        super(context, resource);
        this.context = context;
        this.messages = messages;
    }

    // Return an integer representing the type by fetching the enum type ordinal
    @Override
    public int getItemViewType(int position) {
        if(SpotShotApplication.getCurrentUser().getName().equals(getItem(position).getSenderName())) {
            return 0;
        } else {
            return 1;
        }
    }

    // Total number of types is the number of enum values
    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public Message getItem(int position)
    {
        return messages.get(position);
    }

    @Override
    public int getCount()
    {
        return messages.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int viewType = this.getItemViewType(position);

        View view = convertView;
        switch (viewType) {
            case 0:
                MessageSentHolder sentHolder;

                if (view == null) {
                    LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view = vi.inflate(R.layout.message_item_sent, parent, false);

                    sentHolder = new MessageSentHolder(view);
                    sentHolder.itemWrapper = (RelativeLayout) view.findViewById(R.id.item_wrapper);
                    sentHolder.iconTypeIV = (ImageView) view.findViewById(R.id.message_icon);
                    sentHolder.messageBodyTV = (TextView) view.findViewById(R.id.message_body);
                    view.setTag(sentHolder);
                } else {
                    sentHolder = (MessageSentHolder) view.getTag();
                    sentHolder.iconTypeIV.setVisibility(View.VISIBLE);
                    sentHolder.messageBodyTV.setText(context.getString(R.string.press_to_view));
                }

                // set view data
                populateMessageSentItemView(sentHolder, position);

                return view;
            case 1:
                MessageReceivedHolder receivedHolder;

                if (view == null) {
                    LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view = vi.inflate(R.layout.message_item_received, parent, false);

                    receivedHolder = new MessageReceivedHolder(view);
                    receivedHolder.itemWrapper = (RelativeLayout) view.findViewById(R.id.item_wrapper);
                    receivedHolder.iconTypeIV = (ImageView) view.findViewById(R.id.message_icon);
                    receivedHolder.messageBodyTV = (TextView) view.findViewById(R.id.message_body);
                    view.setTag(receivedHolder);
                } else {
                    receivedHolder = (MessageReceivedHolder) view.getTag();
                    receivedHolder.iconTypeIV.setVisibility(View.VISIBLE);
                    receivedHolder.messageBodyTV.setText(context.getString(R.string.press_to_view));
                }

                // set view data
                populateMessageReceivedItemView(receivedHolder, position);

                // return the created view
                return view;
        }
        return view;
    }

    private void populateMessageReceivedItemView(MessageReceivedHolder holder, int position) {
        // If its the first message in the chat or the sender from the last message is different than the sender of the current message
        if(position == 0 || (position - 1 >= 0 && getItemViewType(position) != getItemViewType(position-1))) {
            holder.itemWrapper.setBackground(getContext().getResources().getDrawable(R.drawable.friend_chat_first));
        } else {
            holder.itemWrapper.setBackground(getContext().getResources().getDrawable(R.drawable.friend_chat));
        }
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.itemWrapper.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_START);
        holder.itemWrapper.setLayoutParams(params);

        if(getItem(position).getType() == Message.TEXT) {
            holder.iconTypeIV.setVisibility(View.GONE);
            holder.messageBodyTV.setText(getItem(position).getBody());
            holder.messageBodyTV.setVisibility(View.VISIBLE);
        } else if(getItem(position).getType() == Message.PHOTO || getItem(position).getType() == Message.VIDEO) {
            holder.iconTypeIV.setImageResource(R.drawable.pink_camera_32_received);
        } else if(getItem(position).getType() == Message.LOCATION) {
            holder.iconTypeIV.setImageResource(R.drawable.pink_logo_32_received);
            holder.messageBodyTV.setVisibility(View.GONE);
        }
    }

    private void populateMessageSentItemView(MessageSentHolder holder, int position) {
        // If its the first message in the chat or the sender from the last message is different than the sender of the current message
        if(position == 0 || (position - 1 >= 0 && getItemViewType(position) != getItemViewType(position-1))) {
            holder.itemWrapper.setBackground(getContext().getResources().getDrawable(R.drawable.you_chat_first));
        } else {
            holder.itemWrapper.setBackground(getContext().getResources().getDrawable(R.drawable.you_chat));
        }
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.itemWrapper.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
        holder.itemWrapper.setLayoutParams(params);

        if(getItem(position).getType() == Message.TEXT) {
            holder.iconTypeIV.setVisibility(View.GONE);
            holder.messageBodyTV.setText(getItem(position).getBody());
            holder.messageBodyTV.setVisibility(View.VISIBLE);
        } else if(getItem(position).getType() == Message.PHOTO || getItem(position).getType() == Message.VIDEO) {
            holder.iconTypeIV.setImageResource(R.drawable.chat_icon_camera_sent);
        } else if(getItem(position).getType() == Message.LOCATION) {
            holder.iconTypeIV.setImageResource(R.drawable.logo_32_sent);
            holder.messageBodyTV.setVisibility(View.GONE);
        }
    }

    private class MessageReceivedHolder extends RecyclerView.ViewHolder {
        private RelativeLayout itemWrapper;
        private TextView messageBodyTV;
        private ImageView iconTypeIV;

        public MessageReceivedHolder(View itemView) {
            super(itemView);
        }
    }

    private class MessageSentHolder extends RecyclerView.ViewHolder {

        private RelativeLayout itemWrapper;
        private TextView messageBodyTV;
        private ImageView iconTypeIV;

        public MessageSentHolder(View itemView) {
            super(itemView);
        }
    }


}
