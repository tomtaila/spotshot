package tmtm.SpotShot.activities.chat;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

import tmtm.SpotShot.R;
import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by mwszedybyl on 3/5/15.
 */
public class ChatListActivity extends SingleFragmentActivity {
    @Override
    public Fragment createFragment() {
        return new ChatListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = "ChatListActivity";
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_fragment);

        fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }
}
