package tmtm.SpotShot.activities.chat;

import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.gcm.GCMMessageHandler;
import tmtm.SpotShot.model.Chat;

/**
 * Created by ttaila on 6/5/15.
 */
public class ChatListFragment extends ListFragment implements GetChatsListener {

    public static boolean onScreen = false;

    public static final String TAG = "ChatListFragment";

    private View rootView;
    private List<Chat> chatList;
    private ListView listView;
    private ChatArrayAdapter arrayAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chatList = DbHelper.getChatsForUser(SpotShotApplication.getCurrentUser());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat_list, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews() {
        listView = (ListView) rootView.findViewById(android.R.id.list);
        listView.setDivider(null);
        arrayAdapter = new ChatArrayAdapter(getActivity(), R.layout.friend_item, chatList);
        setListAdapter(arrayAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ServerChatUtil.getChats(SpotShotApplication.getCurrentUser(), this);
        onScreen = true;
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GCMMessageHandler.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onResume();
        onScreen = false;
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Chat chat = arrayAdapter.getItem(position);
        Intent i = new Intent(getActivity(), ChatActivity.class);

        i.putExtra("chat", chat);
        startActivity(i);
    }

    @Override
    public void doOnGetChatsComplete(List<Chat> chats) {
        chatList.clear();
        chatList.addAll(DbHelper.getChatsForUser(SpotShotApplication.getCurrentUser()));
        arrayAdapter.notifyDataSetChanged();
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(onScreen) {
                chatList.clear();
                chatList.addAll(DbHelper.getChatsForUser(SpotShotApplication.getCurrentUser()));
                arrayAdapter.notifyDataSetChanged();
            }
        }
    };

}
