package tmtm.SpotShot.activities.chat;

import java.util.List;

import tmtm.SpotShot.model.Message;

/**
 * Created by ttaila on 6/14/15.
 */
public interface GetMessagesListener {

    void getMessagesOnComplete(List<Message> messageList);

}
