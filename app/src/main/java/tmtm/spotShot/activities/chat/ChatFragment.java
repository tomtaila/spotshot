package tmtm.SpotShot.activities.chat;

import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.bugsnag.android.Bugsnag;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;

import java.util.List;
import java.util.UUID;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.TimeUtil;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.activities.map.StaticMapActivity;
import tmtm.SpotShot.activities.shot.DisplayMediaActivity;
import tmtm.SpotShot.activities.shot.ShotActivity;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.gcm.GCMMessageHandler;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Message;

/**
 * Created by ttaila on 6/12/15.
 */
public class ChatFragment extends ListFragment implements GetMessagesListener {

    public static boolean onScreen = false;

    private static final String TAG = "ChatFragment";
    private static final int SHOT_RESULT_REQUEST_CODE = 0;

    private List<Message> messages;
    private Chat chat;
    private MessageArrayAdapter arrayAdapter;

    private View rootView;
    private ListView listView;
    private EditText messageField;
    private ImageButton sendBtn;
    private ImageButton geoBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getActivity().getIntent();
        chat = i.getParcelableExtra("chat");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat, parent, false);
        messages = DbHelper.getMessagesForChat(chat);
        setupViews();

        return rootView;
    }

    private void setupViews() {
        listView = (ListView) rootView.findViewById(android.R.id.list);
        geoBtn = (ImageButton) rootView.findViewById(R.id.geo_btn);
        arrayAdapter = new MessageArrayAdapter(getActivity(), R.layout.message_item_received, messages);
        setListAdapter(arrayAdapter);
        messageField = (EditText) rootView.findViewById(R.id.message_field);
        messageField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    sendBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera_white_36dp));
                    geoBtn.setVisibility(View.VISIBLE);
                } else {
                    sendBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_white_36dp));
                    geoBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        geoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String body = LocationHelper.getLastLocation().getLatitude() + " " + LocationHelper.getLastLocation().getLongitude();
                Message m = makeMessage(body, Message.LOCATION);
                sendMessage(m);
            }
        });
        sendBtn = (ImageButton) rootView.findViewById(R.id.send_btn);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Send message button clicked");

                if (messageField.getText().toString().trim().length() > 0) {
                    String body = messageField.getText().toString().trim();
                    Message m = makeMessage(body, Message.TEXT);
                    sendMessage(m);
                    messageField.setText("");
                } else {
                    Intent intent = new Intent(getActivity(), ShotActivity.class);
                    Message m = makeMessage("", Message.PHOTO);
                    intent.putExtra("message", m);
                    getActivity().startActivityForResult(intent, SHOT_RESULT_REQUEST_CODE);
                }
            }
        });
    }

    private void markMessagesAsViewed() {
        RequestParams params = new RequestParams();
        params.put("chat_uuid", chat.getUuid());
        params.put("recipient_uuid", SpotShotApplication.getCurrentUser().getUuid());
        String senderUuid;
        if(SpotShotApplication.getCurrentUser().getUuid().equals(chat.getUserAUuid())) {
            senderUuid = chat.getUserBUuid();
        } else {
            senderUuid = chat.getUserAUuid();
        }
        params.put("sender_uuid", senderUuid);
        SpotShotRestClient.post(URLHelper.MARK_MESSAGES_FOR_CHAT_AS_VIEWED, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        DbHelper.markSentMessagesAsViewed(chat);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void param) {
                    }
                }.execute();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        markMessagesAsViewed();
        ServerChatUtil.getMessages(chat, this);
        onScreen = true;
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GCMMessageHandler.ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        onScreen = false;
        markMessagesAsViewed();
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(onScreen) {
                messages.clear();
                messages.addAll(DbHelper.getMessagesForChat(chat));
                arrayAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    public void getMessagesOnComplete(List<Message> messageList) {
        messages.clear();
        messages.addAll(messageList);
        arrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Message message = arrayAdapter.getItem(position);

        if(message.getType() == Message.LOCATION) {
            Intent i = new Intent(getActivity(), StaticMapActivity.class);

            Location location = LocationHelper.generateLocationFromString(message.getBody());
            i.putExtra("lat", location.getLatitude());
            i.putExtra("long", location.getLongitude());
            startActivity(i);
        } else if(message.getType() == Message.PHOTO || message.getType() == Message.VIDEO) {
            Intent i = new Intent(getActivity(), DisplayMediaActivity.class);

            i.putExtra("mediaUrl", message.getBody());
            i.putExtra("anonymous", false);
            i.putExtra("username", message.getSenderName());
            i.putExtra("timeElapsed", TimeUtil.generateTimeAgoString(message.getTimeSent()));
            i.putExtra("distance", "");
            startActivity(i);
        }
    }

    private Message makeMessage(String body, int type) {
        Message m = new Message();
        m.setBody(body);
        m.setType(type);
        m.setUuid(UUID.randomUUID().toString());
        m.setSenderUuid(SpotShotApplication.getCurrentUser().getUuid());
        m.setSenderName(SpotShotApplication.getCurrentUser().getName());
        if (SpotShotApplication.getCurrentUser().getName().equals(chat.getUserAName())) {
            m.setRecipientUuid(chat.getUserBUuid());
            m.setRecipientName(chat.getUserBName());
        } else {
            m.setRecipientUuid(chat.getUserAUuid());
            m.setRecipientName(chat.getUserAName());
        }
        m.setChatUuid(chat.getUuid());
        m.setTimeSent(System.currentTimeMillis());
        return m;
    }

    private void sendMessage(Message m) {
        ServerChatUtil.createMessage(m, null);
        messages.add(m);
        arrayAdapter.notifyDataSetChanged();

        // Scroll to bottom
        listView.setSelection(arrayAdapter.getCount() - 1);
    }
}
