package tmtm.SpotShot.activities.chat;

import android.content.Context;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.ServerUtil;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Chat;
import tmtm.SpotShot.model.Message;
import tmtm.SpotShot.model.User;


/**
 * Created by ttaila on 3/29/15.
 */
public class ServerChatUtil extends ServerUtil {

    private static final String TAG = "ServerChatUtil";

    private Context context;
    private tmtm.SpotShot.activities.chat.GetMessagesListener getMessagesListener;

    public ServerChatUtil(Context context, tmtm.SpotShot.activities.chat.GetMessagesListener getMessagesListener) {
        this.context = context;
        this.getMessagesListener = getMessagesListener;
    }

    public static void getChats(User user, final GetChatsListener getChatsListener) {
        RequestParams params = new RequestParams();
        params.put("user_uuid", user.getUuid());
        SpotShotRestClient.post(URLHelper.GET_CHATS_URL, params, new JsonHttpResponseHandler() {
            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                // If the response is JSONObject
                new Runnable() {
                    @Override
                    public void run() {
                        List<Chat> chats = new ArrayList<>();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Chat ch = generateChatFromJson(jsonObject);
                                if (ch != null) {
                                    chats.add(ch);
                                }
                            }
                            DbHelper.commitOrUpdateChats(chats);
                            DbHelper.removeInvalidChats(chats, SpotShotApplication.getCurrentUser());
                            if(getChatsListener != null) {
                                getChatsListener.doOnGetChatsComplete(DbHelper.getChatsForUser(SpotShotApplication.getCurrentUser()));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }.run();
            }
        });
    }

    private static Chat generateChatFromJson(JSONObject json) {
        Chat chat = null;
        try {
            chat = new Chat(json.getString("uuid"), json.getString("user_a_uuid"), json.getString("user_a_name"), json.getString("user_b_uuid"), json.getString("user_b_name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return chat;
    }

    public static void getMessages(final Chat chat, final GetMessagesListener getMessagesListener) {
        RequestParams params = new RequestParams();
        params.put("chat_uuid", chat.getUuid());
        SpotShotRestClient.post(URLHelper.GET_MESSAGES_URL, params, new JsonHttpResponseHandler() {
            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                // If the response is JSONObject
                new Runnable() {
                    @Override
                    public void run() {
                        List<Message> messages = new ArrayList<>();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Message m = Message.buildFromJson(jsonObject);
                                if (m != null) {
                                    messages.add(m);
                                }
                            }
                            DbHelper.commitOrUpdateMessages(messages);
                            if(getMessagesListener != null) {
                                getMessagesListener.getMessagesOnComplete(DbHelper.getMessagesForChat(chat));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }.run();
            }
        });
    }

    public static void createMessage(final Message message, final CreateMessageListener createMessageListener) {
        RequestParams params = new RequestParams();
        params.put("uuid", message.getUuid());
        params.put("chat_uuid", message.getChatUuid());
        params.put("body", message.getBody());
        params.put("sender_uuid", message.getSenderUuid());
        params.put("sender_name", message.getSenderName());
        params.put("recipient_uuid", message.getRecipientUuid());
        params.put("recipient_name", message.getRecipientName());
        params.put("message_type", message.getType());
        params.put("time_sent", message.getTimeSent());
        SpotShotRestClient.post(URLHelper.CREATE_MESSAGE_URL, params, new JsonHttpResponseHandler() {
            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                // If the response is JSONObject
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if(response.getBoolean("success")) {
                                DbHelper.commitOrUpdateMessage(message);
                                if(createMessageListener != null) {
                                    createMessageListener.onMessageCreated(message);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }.run();
            }
        });
    }

}
