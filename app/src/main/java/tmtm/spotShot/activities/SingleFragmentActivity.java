package tmtm.SpotShot.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import tmtm.SpotShot.R;

/**
 * Created by ttaila on 2/28/15.
 */
public abstract class SingleFragmentActivity extends FragmentActivity {

    protected String TAG;

    protected FragmentManager fm;

    public abstract Fragment createFragment();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_fragment);
        fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (LocationHelper.getInstance() != null && LocationHelper.getGoogleApiClient() != null && LocationHelper.getGoogleApiClient().isConnected() && LocationHelper.isRequestingLocationUpdates()) {
            LocationHelper.getInstance().startLocationUpdates();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (LocationHelper.getGoogleApiClient() != null && LocationHelper.getGoogleApiClient().isConnected() && LocationHelper.isRequestingLocationUpdates()) {
            LocationHelper.getInstance().stopLocationUpdates();
        }
    }

}
