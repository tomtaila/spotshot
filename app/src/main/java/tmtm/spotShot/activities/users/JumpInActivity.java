package tmtm.SpotShot.activities.users;

import android.app.Fragment;
import android.content.Intent;

import tmtm.SpotShot.activities.MainActivity;
import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by mwszedybyl on 4/9/15.
 */
public class JumpInActivity extends SingleFragmentActivity
{
    @Override
    public Fragment createFragment() {
        return new JumpInFragment();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }

}
