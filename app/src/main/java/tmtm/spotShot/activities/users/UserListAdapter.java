package tmtm.SpotShot.activities.users;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.model.User;

/**
 * Created by mwszedybyl on 5/19/15.
 */
public class UserListAdapter extends ArrayAdapter<User>
{

    private Context context;
    private List<User> userList;

    public UserListAdapter(Context context, int resource, List<User> userList) {
        super(context, resource);
        this.userList = userList;
        this.context = context;
    }

    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User user = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.user_list_item, null);

        populateView(convertView, user);

        return convertView;
    }

    private void populateView(View convertView, final User user) {
        TextView username = (TextView) convertView.findViewById(R.id.username_tv);
        username.setText(user.getName());
    }

}