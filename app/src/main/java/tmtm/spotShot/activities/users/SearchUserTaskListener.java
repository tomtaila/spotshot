package tmtm.SpotShot.activities.users;

import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 4/27/15.
 */
public interface SearchUserTaskListener {

    // FriendStatus may be null so handle
    void doOnSearchUserTaskComplete(User user, String friendStatus);

}
