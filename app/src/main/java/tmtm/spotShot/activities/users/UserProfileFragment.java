package tmtm.SpotShot.activities.users;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bugsnag.android.Bugsnag;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.chat.ChatListFragment;
import tmtm.SpotShot.activities.friends.AddFriendsActivity;
import tmtm.SpotShot.activities.friends.FriendsActivity;
import tmtm.SpotShot.activities.settings.SettingsActivity;
import tmtm.SpotShot.activities.shot.ShotListFragment;

/**
 * Created by ttaila on 4/28/15.
 */
public class UserProfileFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "UserFragment";

    public static final int CURRENT_FRAG_CHATS = 0;
    public static final int CURRENT_FRAG_MY_SHOTS = 1;

    public static int currentFrag = 0;

    private View rootView;
    private FrameLayout fragmentContainer;
    private FragmentManager fm;

    private Button shotsBtn;
    private Button chatsBtn;
    private Button myFriendsBtn;
    private Button addFriendsBtn;
    private ImageButton optionsPopupBtn;
    private TextView optionsTooltip;
    private TextView fabTooltip;
    private RelativeLayout optionsPopup;
    private ArrayList itemsList;
    private boolean loadMedia;
    private Button settingsBtn;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        itemsList = new ArrayList<>();
        loadMedia = true;
        fm = getChildFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.fragment_user_profile, container, false);
        return rootView;
    }

    private void setupViews(){
        fragmentContainer = (FrameLayout) rootView.findViewById(R.id.fragment_container);
        optionsPopup = (RelativeLayout) rootView.findViewById(R.id.options_popup);
        optionsTooltip = (TextView) rootView.findViewById(R.id.options_tooltip);
        if(SpotShotApplication.getSharedPreferences().getBoolean("userProfileOptionsTooltip", false)) {
            optionsTooltip.setVisibility(View.GONE);
        }

        optionsPopupBtn = (ImageButton) rootView.findViewById(R.id.options_popup_btn);
        optionsPopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Options popup button clicked");
                SpotShotApplication.getSharedPreferences().edit().putBoolean("userProfileOptionsTooltip", true).commit();
                optionsTooltip.setVisibility(View.GONE);
                if(optionsPopup.getVisibility() == View.VISIBLE) {
                    Bugsnag.leaveBreadcrumb("Settings close button clicked");
                    Animation animation = generatePopdownAnimation();
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            optionsPopup.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    optionsPopup.startAnimation(animation);
                } else {
                    Animation animation = generatePopupAnimation();
                    optionsPopup.startAnimation(animation);
                    optionsPopup.setVisibility(View.VISIBLE);
                }
            }
        });

        settingsBtn = (Button) rootView.findViewById(R.id.settings_option_btn);
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Settings button clicked");
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
            }
        });
        myFriendsBtn = (Button) rootView.findViewById(R.id.friends_option_btn);
        myFriendsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Friends button clicked");
                Intent i = new Intent(getActivity(), FriendsActivity.class);
                startActivity(i);
            }
        });
        addFriendsBtn = (Button) rootView.findViewById(R.id.add_friends_option_btn);
        addFriendsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Add Friends button clicked");
                Intent intent = new Intent(getActivity(), AddFriendsActivity.class);
                startActivity(intent);
            }
        });

        shotsBtn = (Button) rootView.findViewById(R.id.shots_btn);
        chatsBtn = (Button) rootView.findViewById(R.id.chats_btn);
        shotsBtn.setOnClickListener(this);
        chatsBtn.setOnClickListener(this);

        if(currentFrag == CURRENT_FRAG_MY_SHOTS) {
            Tracker tracker = SpotShotApplication.getInstance().getDefaultTracker();
            tracker.setScreenName("User Shots Screen");
            tracker.send(new HitBuilders.ScreenViewBuilder().build());

            ShotListFragment shotListFragment = new ShotListFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("sortByFlag", ShotListFragment.USERS_OWN_FLAG);
            shotListFragment.setArguments(bundle);

            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment_container, shotListFragment);
            ft.commit();

            shotsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_selected));
            shotsBtn.setTextColor(getResources().getColor(R.color.white));
            chatsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_unselected));
            chatsBtn.setTextColor(getResources().getColor(R.color.white_65alpha));
        } else {
            Tracker tracker = SpotShotApplication.getInstance().getDefaultTracker();
            tracker.setScreenName("Chat Screen");
            tracker.send(new HitBuilders.ScreenViewBuilder().build());

            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            ft.replace(R.id.fragment_container, new ChatListFragment());
            ft.commit();

            chatsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_selected));
            chatsBtn.setTextColor(getResources().getColor(R.color.white));
            shotsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_unselected));
            shotsBtn.setTextColor(getResources().getColor(R.color.white_65alpha));
        }

    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewStateRestored (Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart () {
        super.onStart();
    }

    @Override
    public void onResume () {
        super.onResume();
        setupViews();
    }

    @Override
    public void onPause () {
        super.onPause();
    }

    @Override
    public void onStop () {
        super.onStop();
    }

    @Override
    public void onDestroyView () {
        super.onDestroyView();
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
    }

    @Override
    public void onDetach () {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        if(v == shotsBtn) {
            Bugsnag.leaveBreadcrumb("Users own shots button clicked");

            Tracker tracker = SpotShotApplication.getInstance().getDefaultTracker();
            tracker.setScreenName("User Shots Screen");
            tracker.send(new HitBuilders.ScreenViewBuilder().build());

            // keep track of the last tab selected
            currentFrag = CURRENT_FRAG_MY_SHOTS;

            ShotListFragment shotListFragment = new ShotListFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("sortByFlag", ShotListFragment.USERS_OWN_FLAG);
            shotListFragment.setArguments(bundle);

            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            ft.replace(R.id.fragment_container, shotListFragment);
            ft.commit();

            shotsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_selected));
            shotsBtn.setTextColor(getResources().getColor(R.color.white));
            chatsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_unselected));
            chatsBtn.setTextColor(getResources().getColor(R.color.white_65alpha));
        } else if(v == chatsBtn) {
            Bugsnag.leaveBreadcrumb("Chats button clicked");

            Tracker tracker = SpotShotApplication.getInstance().getDefaultTracker();
            tracker.setScreenName("Chat Screen");
            tracker.send(new HitBuilders.ScreenViewBuilder().build());

            // keep track of the last tab selected
            currentFrag = CURRENT_FRAG_CHATS;

            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            ft.replace(R.id.fragment_container, new ChatListFragment());
            ft.commit();

            chatsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_selected));
            chatsBtn.setTextColor(getResources().getColor(R.color.white));
            shotsBtn.setBackground(getResources().getDrawable(R.drawable.user_profile_tab_bg_unselected));
            shotsBtn.setTextColor(getResources().getColor(R.color.white_65alpha));
        }
    }

    private Animation generatePopupAnimation() {
        Animation scaleAnimation = new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setInterpolator(new OvershootInterpolator());
        scaleAnimation.setDuration(300);
        scaleAnimation.setFillEnabled(true);
        scaleAnimation.setFillAfter(true);

        return scaleAnimation;
    }

    private Animation generatePopdownAnimation() {
        Animation scaleAnimation = new ScaleAnimation(1f, 0f, 1f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setInterpolator(new LinearInterpolator());
        scaleAnimation.setDuration(100);

        return scaleAnimation;
    }

}
