package tmtm.SpotShot.activities.users;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import net.steamcrafted.loadtoast.LoadToast;

import org.json.JSONObject;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.UUID;

import tmtm.SpotShot.MathUtil;
import tmtm.SpotShot.R;
import tmtm.SpotShot.ServerUtil;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.common.AppInfoUtil;
import tmtm.SpotShot.model.User;


/**CreateUserTask
 * Created by ttaila on 3/29/15.
 */
public class ServerUserUtil extends ServerUtil {

    private static final String TAG = "ServerSnapUtil";

    private Context context;
    private tmtm.SpotShot.activities.users.LoginTaskListener loginTaskListener;
    private tmtm.SpotShot.activities.users.CreateUserTaskListener createUserTaskListener;
    private tmtm.SpotShot.activities.users.SearchUserTaskListener searchUserTaskListener;

    public ServerUserUtil(Context context) {
        this.context = context;
    }

    public ServerUserUtil(Context context, tmtm.SpotShot.activities.users.LoginTaskListener loginTaskListener) {
        this.context = context;
        this.loginTaskListener = loginTaskListener;
    }

    public ServerUserUtil(Context context, tmtm.SpotShot.activities.users.CreateUserTaskListener createUserTaskListener) {
        this.context = context;
        this.createUserTaskListener = createUserTaskListener;
    }

    public ServerUserUtil(Context context, tmtm.SpotShot.activities.users.SearchUserTaskListener searchUserTaskListener) {
        this.context = context;
        this.searchUserTaskListener = searchUserTaskListener;
    }

    // Create snap db record for server side db
    public void createUser(User user, String password) {
        new CreateUserTask().execute(user, password);
    }
    // AsyncTask for creating snap db record for server side db
    private class CreateUserTask extends AsyncTask<Object, Void, Boolean> {
        private User user;
        private String password;

        @Override
        protected Boolean doInBackground(Object... params) {
            user = (User) params[0];
            password = (String) params[1];
            boolean success = false;

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "CreateUserOperation.doInBackground() called");

                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.CREATE_USER_URL));

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    JSONObject jsonUser = new JSONObject();
                    jsonParam.put("user", jsonUser);
                    jsonUser.put("name", user.getName());
                    jsonUser.put("email", user.getEmail());
                    jsonUser.put("password", password);
                    String uuid = UUID.randomUUID().toString();
                    jsonUser.put("uuid", uuid);
                    user.setUuid(uuid);
                    System.out.println("JSON = " + jsonParam.toString());

//                    sendJsonRequest(jsonParam, conn);
                    OutputStream os = conn.getOutputStream();
                    OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                    osw.write(jsonParam.toString());
                    osw.flush();
                    osw.close();

                    StringBuilder sb = getJsonResponse(conn);

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    success = jsonObject.getBoolean("success");
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            Log.i(TAG, "Creating user record onto server db was successful = " + success);
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(createUserTaskListener != null) {
                createUserTaskListener.doOnComplete(success, user, password);
            }
        }
    }

    // Get all snaps from server side db within radius of location
    public void login(String email, String password) {
        new LoginTask().execute(email, password);
    }
    // AsyncTask for creating snap db record for server side db
    private class LoginTask extends AsyncTask<String, Void, User> {

        String regUuid;
        boolean success = false;
        LoadToast lt;

        public LoginTask() {
            lt = new LoadToast(context);
            lt.setText("Logging in...");
            lt.setProgressColor(context.getResources().getColor(R.color.primary));
            lt.show();
        }


        @Override
        protected User doInBackground(String... params) {
            String name = params[0];
            String password = params[1];
            User resultUser = null;

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "LoginTask.doInBackground() called");

                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.LOGIN_URL));

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("name", name);
                    jsonParam.put("password", password);
                    jsonParam.put("app_version", AppInfoUtil.getAppVersion(SpotShotApplication.getInstance()));
                    jsonParam.put("platform", AppInfoUtil.PLATFORM);

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    success = jsonObject.getBoolean("success");
                    if(success) {
                        JSONObject resultUserJson = jsonObject.getJSONObject("user");
                        resultUser = new User();
                        resultUser.setName(resultUserJson.getString("name"));
                        resultUser.setEmail(resultUserJson.getString("email"));
                        resultUser.setScore(resultUserJson.getInt("score"));
                        resultUser.setUuid(resultUserJson.getString("uuid"));
//                        if(resultUserJson.has("phone_number")) {
//                            resultUser.setPhoneNumber(resultUserJson.getInt("phone_number"));
//                        }
                        if(jsonObject.has("device")) {
                            JSONObject deviceJson = jsonObject.getJSONObject("device");


                            regUuid = deviceJson.getString("reg_uuid");
                        }
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            return resultUser;
        }

        @Override
        protected void onPostExecute(User user) {
            if(success) {
                lt.success();
            } else {
                lt.error();
            }
            if(loginTaskListener != null) {
                loginTaskListener.doOnComplete(user, regUuid);
            }
        }

    }

    // Create snap db record for server side db
    public void searchUser(String userName) {
        new SearchUserTask().execute(userName);
    }
    // AsyncTask for creating snap db record for server side db
    private class SearchUserTask extends AsyncTask<String, Void, User> {

        String friendStatus;

        @Override
        protected User doInBackground(String... params) {
            String userName = params[0];
            User user = null;
            boolean success = false;

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "SearchUserTask.doInBackground() called");

                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.SEARCH_USER_URL));

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("user_name", userName);
                    jsonParam.put("searching_user_uuid", SpotShotApplication.getCurrentUser().getUuid());

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    success = jsonObject.getBoolean("success");
                    if(success) {
                        user = new User();
                        JSONObject userJson = jsonObject.getJSONObject("user");
                        user.setEmail(userJson.getString("email"));
                        user.setUuid(userJson.getString("uuid"));
                        user.setName(userJson.getString("name"));
                        user.setScore(userJson.getInt("score"));
                        if(jsonObject.has("friend_status")) {
                            friendStatus = jsonObject.getString("friend_status");
                        }
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            Log.i(TAG, "Finding user record from server db was successful = " + success);
            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            if(searchUserTaskListener != null) {
                searchUserTaskListener.doOnSearchUserTaskComplete(user, friendStatus);
            }
        }
    }

}
