package tmtm.SpotShot.activities.users;

import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 4/9/15.
 */
public interface LoginTaskListener {

    // device may be null
    void doOnComplete(User user, String regUuid);

}
