package tmtm.SpotShot.activities.users;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;

/**
 * Created by ttaila on 3/3/15.
 */
public class CreateUserFragment extends Fragment {

    public static final String TAG = "CreateUserFragment";

    private SpotShotApplication app;
    private View rootView;
    private EditText nameEditText;
    private EditText passwordEditText;
    private Button createUserButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = SpotShotApplication.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.fragment_create_user, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews() {
        createUserButton = (Button) rootView.findViewById(R.id.create_user_btn);
        nameEditText = (EditText) rootView.findViewById(R.id.user_name_edit_text);
        passwordEditText = (EditText) rootView.findViewById(R.id.password_edit_text);

    }

}
