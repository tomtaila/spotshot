package tmtm.SpotShot.activities.users;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;

import tmtm.SpotShot.R;
import tmtm.SpotShot.Settings;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.ViewPagerActivity;
import tmtm.SpotShot.gcm.GCMUtil;
import tmtm.SpotShot.model.User;

/**
 * Created by mwszedybyl on 4/9/15.
 */
public class JumpInFragment extends Fragment implements View.OnClickListener, LoginTaskListener, CreateUserTaskListener
{
    private static final String TAG = "JumpInFragment";

    private View rootView;
    private RelativeLayout jumpInDetailsWrapper;
    private RelativeLayout newHereDetailsWrapper;
    private Button jumpInBtn;
    private Button newHereBtn;
    private Button createUserBtn;
    private Button loginBtn;
    private EditText loginUsernameET;
    private EditText loginPasswordET;
    private EditText newHereUsernameET;
    private EditText newHerePasswordET;
    private String userPhoneNumber;
    private long phoneNumberInt;
    private SharedPreferences.Editor editor;

    private boolean jumpInExpanded;
    private boolean newHereExpanded;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        editor = SpotShotApplication.getInstance().getSharedPreferences().edit();
        reloginIfUserWasLoggedIn();
        TelephonyManager tMgr = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNumberWithPlus = tMgr.getLine1Number();
        if(phoneNumberWithPlus!=null)
        {
            userPhoneNumber = phoneNumberWithPlus.replace("+", "");
            try {
                phoneNumberInt = Long.parseLong(userPhoneNumber.toString());
            } catch (Exception e) {
                e.printStackTrace();
                Bugsnag.leaveBreadcrumb(phoneNumberWithPlus);
                Bugsnag.notify(e);
                phoneNumberInt = 0;
            }
        } else {
            phoneNumberInt = 0;
        }
    }

    private void reloginIfUserWasLoggedIn() {
        String username = SpotShotApplication.getInstance().getSharedPreferences().getString(Settings.USER_NAME_KEY, "default");
        String password = SpotShotApplication.getInstance().getSharedPreferences().getString(Settings.USER_PASSWORD_KEY, "default");
        if(SpotShotApplication.getInstance().loggedIn() && !username.equals("default") && !password.equals("default") && !username.equals("") && !password.equals("")) {
            ServerUserUtil serverUserUtil = new ServerUserUtil(getActivity(), (LoginTaskListener) this);
            serverUserUtil.login(username, password);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.jump_in_fragment, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews(){
        jumpInExpanded = false;
        newHereExpanded = false;
        jumpInBtn = (Button) rootView.findViewById(R.id.log_in_btn);
        newHereBtn = (Button) rootView.findViewById(R.id.new_here_btn);
        createUserBtn = (Button) rootView.findViewById(R.id.create_user_btn);
        loginBtn = (Button) rootView.findViewById(R.id.login_btn);
        loginUsernameET = (EditText) rootView.findViewById(R.id.username_edit_text);
        loginPasswordET = (EditText) rootView.findViewById(R.id.password_edit_text);
        newHereUsernameET = (EditText) rootView.findViewById(R.id.new_here_name_edit_text);
        newHerePasswordET = (EditText) rootView.findViewById(R.id.new_here_password_edit_text);
        jumpInDetailsWrapper = (RelativeLayout) rootView.findViewById(R.id.login_details_wrapper);
        newHereDetailsWrapper = (RelativeLayout) rootView.findViewById(R.id.new_here_details_wrapper);

        jumpInBtn.setOnClickListener(this);
        newHereBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        createUserBtn.setOnClickListener(this);
    }
    //login user
    @Override
    public void doOnComplete(final User user, String regUuid) {
        if(user != null) {
            SpotShotApplication.getInstance().setCurrentUser(user);
            SpotShotApplication.getInstance().pullCurrentUserLikes();
            editor.putBoolean(Settings.LOGGED_IN_KEY, true);
            editor.putString(Settings.USER_NAME_KEY, user.getName());
            if(regUuid != null) {
                editor.putString(Settings.REG_ID_KEY, regUuid);
            } else {
                // register user/device on gcm
                // GCM
                GCMUtil.registerOrUpdateGCM(getActivity(), user);
            }
            editor.commit();
            SpotShotApplication.storeUser(user);

            Intent i = new Intent(getActivity(), ViewPagerActivity.class);
            startActivity(i);
        } else {
            Toast toast = Toast.makeText(getActivity(), "Invalid user name and password combination, please try again", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void expandJumpIn() {
        // expand login form view
        Animation scaleJumpInDetailsWrapperVerticallyAnimation = generateScaleVerticallyAnimation(0.0f, 1.0f, 200);
        jumpInDetailsWrapper.setVisibility(View.VISIBLE);
        jumpInDetailsWrapper.startAnimation(scaleJumpInDetailsWrapperVerticallyAnimation);

        // move login header view
        // calculate new top y coordinate
        final int yOffset = (int) -SpotShotApplication.convertDpToPx(R.dimen.login_details_wrapper_height);
        final int newTop = jumpInBtn.getTop()+yOffset;
        Animation moveJumpInBtnUpAnimation = generateMoveVerticallyAnimation(yOffset, 200);
        moveJumpInBtnUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // change position in relative layout ot be above the login_details_wrapper
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.login_btn_height));
                p.addRule(RelativeLayout.ABOVE, R.id.login_details_wrapper);
                p.setMargins(0, 0, 0, 0);
                jumpInBtn.setLayoutParams(p);
                // change the coordinates of the view object itself so that on click listener reacts to new position
                jumpInBtn.layout(0, newTop, 0 + jumpInBtn.getMeasuredWidth(), newTop + jumpInBtn.getMeasuredHeight());
                jumpInBtn.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        jumpInBtn.startAnimation(moveJumpInBtnUpAnimation);
        // set jump in expanded to be true
        jumpInExpanded = true;
    }

    private void collapseJumpIn() {
        // expand login form view
        Animation scaleJumpInDetailsWrapperVerticallyAnimation = generateScaleVerticallyAnimation(1.0f, 0.0f, 200);
        scaleJumpInDetailsWrapperVerticallyAnimation.setFillAfter(false);
        scaleJumpInDetailsWrapperVerticallyAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                jumpInDetailsWrapper.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        jumpInDetailsWrapper.startAnimation(scaleJumpInDetailsWrapperVerticallyAnimation);

        // move login header view
        // calculate new top y coordinate
        final int yOffset = (int) SpotShotApplication.convertDpToPx(R.dimen.login_details_wrapper_height);
        final int newTop = jumpInBtn.getTop()+yOffset;
        Animation moveJumpInBtnUpAnimation = generateMoveVerticallyAnimation(yOffset, 200);
        moveJumpInBtnUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // change position in relative layout ot be aligned with the bottom of the view
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.login_btn_height));
                p.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                p.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.login_btn_height));
                jumpInBtn.setLayoutParams(p);
                // change the coordinates of the view object itself so that on click listener reacts to new position
                jumpInBtn.layout(0, newTop, 0 + jumpInBtn.getMeasuredWidth(), newTop + jumpInBtn.getMeasuredHeight());
                jumpInBtn.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        jumpInBtn.startAnimation(moveJumpInBtnUpAnimation);
        // set jump in expanded to be false
        jumpInExpanded = false;
    }

    private void expandNewHere() {
        // expand login form view
        Animation scaleJumpInDetailsWrapperVerticallyAnimation = generateScaleVerticallyAnimation(0.0f, 1.0f, 200);
        newHereDetailsWrapper.setVisibility(View.VISIBLE);
        newHereDetailsWrapper.startAnimation(scaleJumpInDetailsWrapperVerticallyAnimation);

        // move login header view
        // calculate new top y coordinate
        final int yOffset = (int) -SpotShotApplication.convertDpToPx(R.dimen.login_details_wrapper_height);
        final int newTop = newHereBtn.getTop()+yOffset;
        Animation moveJumpInBtnUpAnimation = generateMoveVerticallyAnimation(yOffset, 200);
        moveJumpInBtnUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // change position in relative layout ot be above the login_details_wrapper
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.login_btn_height));
                p.addRule(RelativeLayout.ABOVE, R.id.new_here_details_wrapper);
                p.setMargins(0, 0, 0, 0);
                newHereBtn.setLayoutParams(p);
                // change the coordinates of the view object itself so that on click listener reacts to new position
                newHereBtn.layout(0, newTop, 0 + newHereBtn.getMeasuredWidth(), newTop + newHereBtn.getMeasuredHeight());
                newHereBtn.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        newHereBtn.startAnimation(moveJumpInBtnUpAnimation);
        // set jump in expanded to be true
        newHereExpanded = true;
    }

    private void collapseNewHere() {
        // expand login form view
        Animation scaleJumpInDetailsWrapperVerticallyAnimation = generateScaleVerticallyAnimation(1.0f, 0.0f, 200);
        scaleJumpInDetailsWrapperVerticallyAnimation.setFillAfter(false);
        scaleJumpInDetailsWrapperVerticallyAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                newHereDetailsWrapper.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        newHereDetailsWrapper.startAnimation(scaleJumpInDetailsWrapperVerticallyAnimation);

        // move login header view
        // calculate new top y coordinate
        final int yOffset = (int) SpotShotApplication.convertDpToPx(R.dimen.login_details_wrapper_height);
        final int newTop = newHereBtn.getTop()+yOffset;
        Animation moveJumpInBtnUpAnimation = generateMoveVerticallyAnimation(yOffset, 200);
        moveJumpInBtnUpAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // change position in relative layout ot be aligned with the bottom of the view
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.login_btn_height));
                p.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                newHereBtn.setLayoutParams(p);
                // change the coordinates of the view object itself so that on click listener reacts to new position
                newHereBtn.layout(0, newTop, 0 + newHereBtn.getMeasuredWidth(), newTop + newHereBtn.getMeasuredHeight());
                newHereBtn.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        newHereBtn.startAnimation(moveJumpInBtnUpAnimation);
        // set jump in expanded to be false
        newHereExpanded = false;
    }

    @Override
    public void onClick(View v) {
        if(v == jumpInBtn) {
            Bugsnag.leaveBreadcrumb("Jump in button clicked");
            Log.i(TAG, "jumpInBtn.onClick()");
            if(!jumpInExpanded && !newHereExpanded) {
                expandJumpIn();
            } else if(jumpInExpanded && !newHereExpanded) {
                collapseJumpIn();
            } else if(jumpInExpanded && newHereExpanded) {
                collapseNewHere();
            }
        } else if (v == newHereBtn) {
            Bugsnag.leaveBreadcrumb("New here button clicked");
            Log.i(TAG, "newHereBtn.onClick()");
            if(!jumpInExpanded && !newHereExpanded) {
                expandJumpIn();
                expandNewHere();
            } else if(jumpInExpanded && !newHereExpanded) {
                expandNewHere();
            } else if(jumpInExpanded && newHereExpanded) {
                collapseNewHere();
            }
        } else if(v == loginBtn) {
            Bugsnag.leaveBreadcrumb("Log in button clicked");
            Log.i(TAG, "loginBtn.onClick()");
            String username = loginUsernameET.getText().toString().toLowerCase();
            String password = loginPasswordET.getText().toString();
            login(username, password);
        } else if(v == createUserBtn) {
            Bugsnag.leaveBreadcrumb("Create user button clicked");
            Log.i(TAG, "createUserBtn.onClick()");
            String userName = newHereUsernameET.getText().toString().toLowerCase();
            String password = newHerePasswordET.getText().toString();
            createUser(userName, password);
        } else if(v == loginUsernameET) {

        }
    }

    private void createUser(String name, String password) {
        ServerUserUtil serverUserUtil = new ServerUserUtil(getActivity(), (CreateUserTaskListener) this);
        User user = new User();
        user.setName(name);
        user.setPhoneNumber(phoneNumberInt);
        serverUserUtil.createUser(user, password);
    }

    private void login(String name, String password) {
        editor.putString(Settings.USER_NAME_KEY, loginUsernameET.getText().toString());
        editor.putString(Settings.USER_PASSWORD_KEY, loginPasswordET.getText().toString());
        editor.commit();
        ServerUserUtil serverUserUtil = new ServerUserUtil(getActivity(), (LoginTaskListener) this);
        serverUserUtil.login(name, password);
    }

    @Override
    public void doOnComplete(Boolean success, final User user, String password) {
        if(success) {
            Toast toast = Toast.makeText(getActivity(), "You successfully created a user! Welcome SpotShotter!", Toast.LENGTH_SHORT);
            toast.show();
            login(user.getName(), password);
        } else {
        Toast toast = Toast.makeText(getActivity(), "Creating a user failed!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public Animation generateMoveVerticallyAnimation(float deltaY, long duration) {
        Animation animation = new TranslateAnimation(0f, 0f, 0f, deltaY);
        animation.setDuration(duration);
        animation.setFillAfter(true);
        return animation;
    }

    public Animation generateScaleVerticallyAnimation(float fromYScale, float toYScale, long duration) {
        Animation animation = new ScaleAnimation(1.0f, 1.0f, fromYScale, toYScale,  Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF, 1f);
        animation.setDuration(duration);
        animation.setFillAfter(true);
        return animation;
    }

}
