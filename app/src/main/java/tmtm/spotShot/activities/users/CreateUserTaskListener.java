package tmtm.SpotShot.activities.users;

import tmtm.SpotShot.model.User;

/**
 * Created by ttaila on 4/11/15.
 */
public interface CreateUserTaskListener {
    public void doOnComplete(Boolean success, User user, String password);
}
