package tmtm.SpotShot.activities.users;

import android.app.Fragment;

import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by ttaila on 3/3/15.
 */
public class CreateUserActivity extends SingleFragmentActivity {
    @Override
    public Fragment createFragment() {
        return new CreateUserFragment();
    }
}
