package tmtm.SpotShot.activities.shot;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import tmtm.SpotShot.AWSHelper;
import tmtm.SpotShot.SpotShotApplication;

/**
 * Created by mwszedybyl on 4/13/15.
 */
public class ThumbnailDownloader<Token> extends HandlerThread
{
    private static final String TAG = "ThumbnailDownloader";
    private static final int MESSAGE_DOWNLOAD = 0;

    Handler handler;
    Handler responseHandler;
    Listener<Token> listener;
    Map<Token,String> requestMap = Collections.synchronizedMap(new HashMap<Token, String>());

    public interface Listener<Token> {
        void onThumbnailDownloaded(Token token, Bitmap thumbnail);
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

    public ThumbnailDownloader(Handler responseHandler)
    {
        super(TAG);
        this.responseHandler = responseHandler;
    }

    public void queueThumbnail(Token token, String url) {
        Log.i(TAG, "Got url = " + url);
        requestMap.put(token, url);
        handler.obtainMessage(MESSAGE_DOWNLOAD, token).sendToTarget();
    }

    @SuppressLint("HandlerLeak")
    @Override
    protected void onLooperPrepared() {
        handler = new Handler() {
            @Override
        public void handleMessage(Message msg) {
                if(msg.what == MESSAGE_DOWNLOAD){
                    @SuppressWarnings("unchecked")
                    Token token = (Token) msg.obj;
//                    Log.i(TAG, "Got a request for url: " + requestMap.get(token));
                    handleRequest(token);
                }
            }
        };
    }

    private void handleRequest(final Token token){
        try{
            final String url = requestMap.get(token);
            if(url == null)
            {
                return;
            }
            File file = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, url);
            if(file.exists()) {
                InputStream is = new FileInputStream(file);
                final Bitmap bitmap = BitmapFactory.decodeStream(is);

                responseHandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(requestMap.get(token) != url){
                            return;
                        }
                        requestMap.remove(token);

                        listener.onThumbnailDownloaded(token, bitmap);
                    }
                });
            } else {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                final Bitmap bitmap = AWSHelper.getRemoteImageAsBitmap(url);
                if(bitmap!=null)
                {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                }
                    byte[] data = byteArrayOutputStream.toByteArray();


                File thumbnailFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, url);
                try {
                    FileOutputStream fos = new FileOutputStream(thumbnailFile);
                    fos.write(data);
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                responseHandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if (requestMap.get(token) != url)
                        {
                            return;
                        }
                        requestMap.remove(token);

                        listener.onThumbnailDownloaded(token, bitmap);
                    }
                });
            }
        } catch (Exception e){
            Log.e(TAG, "Error downloading image", e);
        }
    }

    public void clearQueue() {
        handler.removeMessages(MESSAGE_DOWNLOAD);
        requestMap.clear();
    }


}
