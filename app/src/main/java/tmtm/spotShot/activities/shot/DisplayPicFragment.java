package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import tmtm.SpotShot.AWSHelper;
import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;

/**
 * Created by ttaila on 3/8/15.
 */
public class DisplayPicFragment extends Fragment {

    public static final String TAG = "DisplayPicFragment";

    private View rootView;
    private ImageView imageView;
    private TextView usernameView;
    private TextView distanceView;
    private TextView timeElapsedView;
    private String mediaUrl;
    private String thumbnailUrl;
    private String username;
    private String distance;
    private String timeElapsed;
    private boolean anonymous;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        Bundle extras = getActivity().getIntent().getExtras();
        mediaUrl = (String) extras.get("mediaUrl");
        thumbnailUrl = (String) extras.get("thumbnailUrl");
        anonymous = (Boolean) extras.get("anonymous");
        username = (String) extras.get("username");
        distance = (String) extras.get("distance");
        timeElapsed = (String) extras.get("timeElapsed");

        rootView = inflater.inflate(R.layout.fragment_display_pic, parent, false);
        setupPicViews();
        return rootView;
    }

    private void setupPicViews() {
        imageView = (ImageView) rootView.findViewById(R.id.image);
        usernameView = (TextView) rootView.findViewById(R.id.username_textview);
        distanceView = (TextView) rootView.findViewById(R.id.distance);
        timeElapsedView = (TextView) rootView.findViewById(R.id.elapsed_time);

        if(anonymous) {
            usernameView.setText("Username: Ninja");
        } else {
            usernameView.setText("Username: " + username);
        }
        distanceView.setText("Distance: " + distance +" miles away");
        timeElapsedView.setText("Taken: " + timeElapsed);

            AWSImageLoader awsImageLoader = new AWSImageLoader();
            awsImageLoader.setImageUrl(mediaUrl);
            awsImageLoader.execute();

    }


    private class AWSImageLoader extends AsyncTask<Void, Void, Void> {

        String imageUrl;
        Bitmap bitmap;

        protected void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        @Override
        protected Void doInBackground(Void... params) {
            File f = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, imageUrl);
            if(f.exists()) {
                bitmap = BitmapFactory.decodeFile(f.getPath());
            } else {
                bitmap = AWSHelper.getRemoteImageAsBitmap(imageUrl);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            Log.i(TAG, "onPostExectute() called");
            imageView.setImageBitmap(bitmap);
        }

    }

}
