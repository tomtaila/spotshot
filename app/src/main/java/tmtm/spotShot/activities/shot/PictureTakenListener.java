package tmtm.SpotShot.activities.shot;

/**
 * Created by ttaila on 5/9/15.
 */
public interface PictureTakenListener {

    void onPictureTaken();

}
