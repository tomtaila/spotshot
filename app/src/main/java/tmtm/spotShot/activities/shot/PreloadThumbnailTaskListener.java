package tmtm.SpotShot.activities.shot;

/**
 * Created by ttaila on 4/14/15.
 */
public interface PreloadThumbnailTaskListener {

    public void doOnComplete();

    public void updateShots();

}
