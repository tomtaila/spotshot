package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bugsnag.android.Bugsnag;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;

/**
 * Created by ttaila on 4/20/15.
 */
public class HotNewHeaderFragment extends Fragment {

    private final static String TAG = "HotNewHeaderFragment";
    private View rootView;
    private Button topBtn;
    private Button newBtn;
    private HotNewHeaderButtonListener hotNewHeaderButtonListener;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.hot_new_header_fragment, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews(){
        topBtn = (Button) rootView.findViewById(R.id.top_btn);
        newBtn = (Button) rootView.findViewById(R.id.new_btn);
        topBtn.setBackground(getResources().getDrawable(R.drawable.shot_list_tab_bg_selected));
        newBtn.setTextColor(getResources().getColor(R.color.white_65alpha));
    }

    @Override
    public void onResume(){
        super.onResume();
        setUpOnClickListeners();
    }

    public void setHotNewHeaderButtonListener(HotNewHeaderButtonListener hotNewHeaderButtonListener) {
        this.hotNewHeaderButtonListener = hotNewHeaderButtonListener;
    }

    public void setUpOnClickListeners() {
        topBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Top shots button clicked");
                Tracker tracker = SpotShotApplication.getInstance().getDefaultTracker();
                tracker.setScreenName("Top Shots Screen");
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                hotNewHeaderButtonListener.onTopBtnClick();
                topBtn.setBackground(getResources().getDrawable(R.drawable.shot_list_tab_bg_selected));
                topBtn.setTextColor(getResources().getColor(R.color.white));
                newBtn.setBackground(getResources().getDrawable(R.drawable.shot_list_tab_bg_unselected));
                newBtn.setTextColor(getResources().getColor(R.color.white_65alpha));
            }
        });

        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("New shots button clicked");
                hotNewHeaderButtonListener.onNewBtnClick();
                Tracker tracker = SpotShotApplication.getInstance().getDefaultTracker();
                tracker.setScreenName("New Shots Screen");
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                newBtn.setBackground(getResources().getDrawable(R.drawable.shot_list_tab_bg_selected));
                newBtn.setTextColor(getResources().getColor(R.color.white));
                topBtn.setBackground(getResources().getDrawable(R.drawable.shot_list_tab_bg_unselected));
                topBtn.setTextColor(getResources().getColor(R.color.white_65alpha));
            }
        });
    }

}
