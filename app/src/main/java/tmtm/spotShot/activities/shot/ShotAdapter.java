package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

/**
 * Created by mwszedybyl on 5/17/15.
 */
public class ShotAdapter extends FragmentPagerAdapter
{

    private boolean isVideo;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public ShotAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i)
        {
            case 0:
                if(isVideo)
                {
                    return new DisplayVideoFragment();
                }
                else {
                    return new DisplayPicFragment();
                }

            case 1:
                return new StaticMapShotFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void setIsVideo(boolean isVideo)
    {
        this.isVideo = isVideo;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}

