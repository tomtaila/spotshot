package tmtm.SpotShot.activities.shot;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bugsnag.android.Bugsnag;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;

/**
 * Created by ttaila on 4/20/15.
 */
public class ShotListParentFragment extends Fragment implements HotNewHeaderButtonListener {

    private String TAG = "ShotListParentFragment";

    private View rootView;
    private HotNewHeaderFragment headerFragment;
    private ShotListFragment shotListFragmentHot;
    private ShotListFragment shotListFragmentNew;
    private ImageButton optionsPopupBtn;
    private TextView optionsTooltip;
    private RelativeLayout optionsPopup;
    private SeekBar radiusSB;
    private SeekBar timeSB;
    private TextView radiusTV;
    private TextView timeTV;

    private FragmentManager fm;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        fm = getChildFragmentManager();
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.fragment_shot_parent_list, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews(){
        headerFragment = (HotNewHeaderFragment) fm.findFragmentById(R.id.hot_new_header_fragment_container);
        if(headerFragment == null) {
            fm.beginTransaction()
                    .add(R.id.hot_new_header_fragment_container, getHotNewHeaderFragmentInstance())
                    .commit();
        }

        // PUT HOT LIST BY DEFAULT
        ShotListFragment currentFrag = (ShotListFragment) fm.findFragmentById(R.id.shot_list_fragment_container);
        if(currentFrag == null) {
            fm.beginTransaction()
                    .add(R.id.shot_list_fragment_container, getShotListHotFragmentInstance())
                    .commit();
        }

        optionsPopup = (RelativeLayout) rootView.findViewById(R.id.options_popup);
        optionsTooltip = (TextView) rootView.findViewById(R.id.options_tooltip);
        if(SpotShotApplication.getSharedPreferences().getBoolean("hotNewOptionsTooltip", false)) {
            optionsTooltip.setVisibility(View.GONE);
        }

        optionsPopupBtn = (ImageButton) rootView.findViewById(R.id.options_popup_btn);
        optionsPopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Options popup button clicked");
                SpotShotApplication.getSharedPreferences().edit().putBoolean("hotNewOptionsTooltip", true).commit();
                optionsTooltip.setVisibility(View.GONE);
                if(optionsPopup.getVisibility() == View.VISIBLE) {
                    Bugsnag.leaveBreadcrumb("Settings close button clicked");
                    Animation animation = generatePopdownAnimation();
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            optionsPopup.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    optionsPopup.startAnimation(animation);
                } else {
                    Animation animation = generatePopupAnimation();
                    optionsPopup.startAnimation(animation);
                    optionsPopup.setVisibility(View.VISIBLE);
                }
            }
        });

        radiusTV = (TextView) rootView.findViewById(R.id.radius_tv);
        radiusTV.setText(getString(R.string.radius, SpotShotApplication.getSharedPreferences().getInt("maxRadius", 25)));
        timeTV = (TextView) rootView.findViewById(R.id.time_tv);
        timeTV.setText(getString(R.string.time, SpotShotApplication.getSharedPreferences().getInt("maxHoursAgo", 12)));

        radiusSB = (SeekBar) rootView.findViewById(R.id.radius_sb);
        radiusSB.setProgress(SpotShotApplication.getSharedPreferences().getInt("maxRadius", 25));
        radiusSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress == 0) {
                    progress = progress + 1;
                }
                radiusTV.setText(getString(R.string.radius, progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(seekBar.getProgress() == 0) {
                    seekBar.setProgress(1);
                }
                SpotShotApplication.getSharedPreferences().edit().putInt("maxRadius", seekBar.getProgress()).commit();
                refreshShots();
            }
        });
        timeSB = (SeekBar) rootView.findViewById(R.id.time_sb);
        timeSB.setProgress(SpotShotApplication.getSharedPreferences().getInt("maxHoursAgo", 12));
        timeSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress == 0) {
                    progress = progress + 1;
                }
                timeTV.setText(getString(R.string.time, progress));
                Log.d(TAG, "TimeSB progess = " + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(seekBar.getProgress() == 0) {
                    seekBar.setProgress(1);
                }
                SpotShotApplication.getSharedPreferences().edit().putInt("maxHoursAgo", seekBar.getProgress()).commit();
                refreshShots();
            }
        });
    }

    private HotNewHeaderFragment getHotNewHeaderFragmentInstance() {
        if(headerFragment == null) {
            headerFragment = new HotNewHeaderFragment();
            headerFragment.setHotNewHeaderButtonListener(this);
        }
        return headerFragment;
    }

    private ShotListFragment getShotListHotFragmentInstance() {
        if(shotListFragmentHot == null) {
            Bundle bundle = new Bundle();
            bundle.putInt("sortByFlag", ShotListFragment.SORT_BY_TOP_FLAG);
            shotListFragmentHot = new ShotListFragment();
            shotListFragmentHot.setArguments(bundle);
        }
        return shotListFragmentHot;
    }

    private ShotListFragment getShotListNewFragmentInstance() {
        if(shotListFragmentNew == null) {
            Bundle bundle = new Bundle();
            bundle.putInt("sortByFlag", ShotListFragment.SORT_BY_NEWEST_FLAG);
            shotListFragmentNew = new ShotListFragment();
            shotListFragmentNew.setArguments(bundle);
        }
        return shotListFragmentNew;
    }


    @Override
    public void onTopBtnClick() {
        Log.i(TAG, "onTopBtnClick()");
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        ft.replace(R.id.shot_list_fragment_container, getShotListHotFragmentInstance());
        ft.commit();
    }

    @Override
    public void onNewBtnClick() {
        Log.i(TAG, "onNewBtnClick()");
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        ft.replace(R.id.shot_list_fragment_container, getShotListNewFragmentInstance());
        ft.commit();
    }

    public void refreshShots() {
        ShotListFragment currentFrag = (ShotListFragment) fm.findFragmentById(R.id.shot_list_fragment_container);
        if(currentFrag != null) {
            currentFrag.refreshShotsFromServer();
        }
    }

    private Animation generatePopupAnimation() {
        Animation scaleAnimation = new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setInterpolator(new OvershootInterpolator());
        scaleAnimation.setDuration(300);
        scaleAnimation.setFillAfter(true);

        return scaleAnimation;
    }

    private Animation generatePopdownAnimation() {
        Animation scaleAnimation = new ScaleAnimation(1f, 0f, 1f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setInterpolator(new LinearInterpolator());
        scaleAnimation.setDuration(100);

        return scaleAnimation;
    }

}
