package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.amazonaws.services.s3.model.S3ObjectInputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import tmtm.SpotShot.AWSHelper;
import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;

/**
 * Created by mwszedybyl on 3/31/15.
 */
public class DisplayVideoFragment extends Fragment
{
    public static final String TAG = "DisplayVideoFragment";

    private View rootView;
    private VideoView videoView;
    private ImageView previewIV;
    private TextView usernameView;
    private TextView distanceView;
    private TextView timeElapsedView;
    private String mediaUrl;
    private String thumbnailUrl;
    private String username;
    private String distance;
    private String timeElapsed;
    private int position = 0;
    private Bitmap bitmap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.fragment_display_video, parent, false);
        Bundle extras = getActivity().getIntent().getExtras();

        mediaUrl = (String) extras.get("mediaUrl");
        thumbnailUrl = (String) extras.get("thumbnailUrl");
        username = (String) extras.get("username");
        distance = (String) extras.get("distance");
        timeElapsed = (String) extras.get("timeElapsed");
        setupViews();

        return rootView;
    }

    private void setupViews() {
        videoView = (VideoView) rootView.findViewById(R.id.video_view);
        previewIV = (ImageView) rootView.findViewById(R.id.preview);
        usernameView = (TextView) rootView.findViewById(R.id.username_textview);
        distanceView = (TextView) rootView.findViewById(R.id.distance);
        timeElapsedView = (TextView) rootView.findViewById(R.id.elapsed_time);

        usernameView.setText("Username: " + username);
        distanceView.setText("Distance: " + distance +" miles away");
        timeElapsedView.setText("taken: " + timeElapsed);

        if(thumbnailUrl!=null)
        {
            File f = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, thumbnailUrl);
            if (f.exists())
            {
                bitmap = BitmapFactory.decodeFile(f.getPath());
                previewIV.setImageBitmap(bitmap);
            }
        }

        videoView.setVisibility(View.INVISIBLE);
        File mediaFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, mediaUrl);
        if(!mediaFile.exists())
        {
            AWSImageLoader awsImageLoader = new AWSImageLoader();
            awsImageLoader.setMediaUrl(mediaUrl);
            awsImageLoader.execute();
        } else {
            videoView.setVisibility(View.VISIBLE);
            Uri uri = Uri.parse(SpotShotApplication.APP_ROOT_MEDIA_PATH + "/" +mediaUrl);
            videoView.setVideoURI(uri);

            videoView.requestFocus();
            //we also set an setOnPreparedListener in order to know when the video file is ready for playback
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                public void onPrepared(MediaPlayer mediaPlayer) {
                    //if we have a position on savedInstanceState, the video playback should start from here
                    videoView.seekTo(position);
                    if (position == 0) {
                        videoView.start();
                    } else {
                        //if we come from a resumed activity, video playback will be paused
                        videoView.pause();
                    }
                }
            });

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mp)
                {
                    videoView.start();
                }
            });
        }
    }

    private class AWSImageLoader extends AsyncTask<Void, Void, Void>
    {
        String videoUrl;
        S3ObjectInputStream inputStream;
        OutputStream outputStream = null;
        File videoFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH + "/" + videoUrl);

        protected void setMediaUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        @Override
        protected Void doInBackground(Void... params) {
            inputStream = AWSHelper.getRemoteVideo(videoUrl);
            Log.i(TAG, "doInBackground called");
            try {
                // write the inputStream to a FileOutputStream
                outputStream =  new FileOutputStream(videoFile);

                int read = 0;
                byte[] bytes = new byte[1024];

                while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            Log.i(TAG, "onPostExecute() called");
            videoView.setVisibility(View.VISIBLE);
            Uri uri = Uri.parse(videoFile.getAbsolutePath());
            videoView.setVideoURI(uri);

            videoView.requestFocus();
            //we also set an setOnPreparedListener in order to know when the video file is ready for playback
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                public void onPrepared(MediaPlayer mediaPlayer) {
                    //if we have a position on savedInstanceState, the video playback should start from here
                    videoView.seekTo(position);
                    if (position == 0) {
                        videoView.start();
                    } else {
                        //if we come from a resumed activity, video playback will be paused
                        videoView.pause();
                    }
                }
            });

            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                @Override
                public void onCompletion(MediaPlayer mp)
                {
                    videoView.start();
                }
            });
        }

    }

}
