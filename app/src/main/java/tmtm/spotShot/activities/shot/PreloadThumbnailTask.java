package tmtm.SpotShot.activities.shot;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.AWSHelper;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 4/14/15.
 */
public class PreloadThumbnailTask extends AsyncTask<List<Shot>, Void, Void> {

    public static final String TAG = "PreloadThumbnailTask";

    public static final int preloadThumbnailLimit = 40;
    public static final int preloadMediaLimit = 5;
    private Context context;
    private List<Shot> shotList;
    private PreloadThumbnailTaskListener preloadThumbnailTaskListener;
    private boolean isThumbnails;
    private boolean newMediaCached;
    private int pageNum;

    public PreloadThumbnailTask(Context context, boolean isThumbnails, int pageNum) {
        this.context = context;
        this.isThumbnails = isThumbnails;
        this.pageNum = pageNum;
        newMediaCached = false;
    }

    public PreloadThumbnailTask(Context context, boolean isThumbnails, int pageNum, PreloadThumbnailTaskListener preloadThumbnailTaskListener) {
        this.context = context;
        this.isThumbnails = isThumbnails;
        this.preloadThumbnailTaskListener = preloadThumbnailTaskListener;
        this.pageNum = pageNum;
        newMediaCached = false;
    }

    @Override
    protected Void doInBackground(List<Shot>... params) {
        shotList = params[0];

        if(isThumbnails) {
            int startIndex = pageNum* ShotListFragment.SHOTS_PER_PAGE;
            int endIndex = startIndex+(ShotListFragment.SHOTS_PER_PAGE*2);
            if(endIndex >= shotList.size()) {
                if(startIndex < shotList.size()) {
                    if(endIndex < shotList.size()) {
                        downloadMediaForSnaps(shotList.subList(startIndex, endIndex));
                    } else {
                        downloadMediaForSnaps(shotList.subList(startIndex, shotList.size()));
                    }
                }
            }
        } else {
            if(shotList.size() > preloadMediaLimit) {
                downloadMediaForSnaps(shotList.subList(0, preloadMediaLimit));
            } else {
                downloadMediaForSnaps(shotList);
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        if(preloadThumbnailTaskListener != null) {
            preloadThumbnailTaskListener.doOnComplete();
        }
        if(newMediaCached && preloadThumbnailTaskListener != null){

            preloadThumbnailTaskListener.updateShots();

        }
    }

    private void downloadMediaForSnaps(List<Shot> shotList) {
        for(Shot s : shotList) {
            String url;
            if(isThumbnails) {
                url = s.getThumbnailUrl();
            } else {
                url = s.getUrl();
            }

            File file = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, url);
            if(!file.exists()) {
                File mediaFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, url);

                if(!s.isVideo() || isThumbnails) {
                    makeImageFile(s, mediaFile);
                } else {
                    makeVideoFile(s, mediaFile);
                }
            }
        }
    }

    private void makeImageFile(Shot s, File f) {
        Bitmap bitmap;
        if(isThumbnails) {
            bitmap = AWSHelper.getRemoteImageAsBitmap(s.getThumbnailUrl());
        } else {

            bitmap = AWSHelper.getRemoteImageAsBitmap(s.getUrl());
            List<Shot> shotForUpgrade = new ArrayList<Shot>();
            s.setMediaDownloaded(true);
            shotForUpgrade.add(s);
            DbHelper.commitOrUpdateShots(shotForUpgrade);
            newMediaCached = true;
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            if(bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            }

            byte[] data = byteArrayOutputStream.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(data);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    protected void makeVideoFile(Shot s, File f) {
        InputStream inputStream = AWSHelper.getRemoteVideo(s.getUrl());
        OutputStream outputStream = null;
        Log.i(TAG, "doInBackground called");
        try {
            // write the inputStream to a FileOutputStream
            outputStream =  new FileOutputStream(f);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
