package tmtm.SpotShot.activities.shot;

import java.util.List;

import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 4/14/15.
 */
public interface GetShotsTaskListener
{

    void doOnComplete(List<Shot> shotList);

}
