package tmtm.SpotShot.activities.shot;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.Constants;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 3/10/15.
 */
public class ShotFilter
{

    private static List<Shot> filterSnapsByDistanceInMiles(List<Shot> shotList, int distance) {
        List<Shot> result = new ArrayList<>();
        Location l;
        for(Shot s : shotList) {
            l = s.getLocation();
            if(l.distanceTo(LocationHelper.getLastLocation()) <= distance* Constants.METRES_IN_A_MILE) {
                result.add(s);
            }
        }
        return result;
    }

    private static List<Shot> filterSnapsByDistanceInKms(List<Shot> shotList, int distance) {
        List<Shot> result = new ArrayList<>();
        Location l;
        for(Shot s : shotList) {
            l = s.getLocation();
            if(l.distanceTo(LocationHelper.getLastLocation()) <= distance* Constants.METRES_IN_A_KM) {
                result.add(s);
            }
        }
        return result;
    }

    public static List<Shot> filterSnapsByDistance(List<Shot> shotList, int distance) {
        if(SpotShotApplication.mUnitsOfDistance == SpotShotApplication.MILES) {
            return filterSnapsByDistanceInMiles(shotList, distance);
        } else {
            return filterSnapsByDistanceInKms(shotList, distance);
        }
    }

}
