package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.os.Bundle;

import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by ttaila on 2/28/15.
 */
public class ShotActivity extends SingleFragmentActivity {

    private final String TAG = "ShotActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment createFragment() {
        return new ShotFragment();
    }
}
