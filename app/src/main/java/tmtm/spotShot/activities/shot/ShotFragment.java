package tmtm.SpotShot.activities.shot;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.ViewPagerActivity;
import tmtm.SpotShot.activities.settings.SettingsActivity;
import tmtm.SpotShot.model.Message;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.ui.FillingRing;

/**
 * Created by ttaila on 2/28/15.
 */
public class ShotFragment extends Fragment implements PictureTakenListener, OnShotCreatedListener
{

    private final static String TAG = "ShotFragment";
    private static final int VIDEO_CALLBACK_REQUEST_CODE = 1;
    private static final int VIDEO_SENT_CODE = 1;
    private static final int VIDEO_CANCELED_CODE = 2;
    private int chosenCamera;

    private ViewGroup rootView;
    private FrameLayout cameraPreviewContainer;
    private CameraPreview cameraPreview;

    private RelativeLayout optionsPanel;
    private FrameLayout darkTransparentOverlay;

    private ImageButton createShotBtn;
    private ImageButton flashBtn;
    private ImageButton cancelBtn;
    private ImageButton allShotsBtn;
    private ImageButton switchCameraBtn;
    private ImageButton sendMedia;
    private ImageButton anonymousBtn;
    private TextView anonymousTooltip;
    private boolean isFlashOn;
    private GestureDetector gestureDetector;

    private boolean isRecording;
    private Timer timer;
    private TimerTask timerTask;
    final Handler handler = new Handler();
    private long downTime;
    private int flyingAngle;
    private boolean isVideoOver12Secs;
    private FillingRing fillingRing;

    private boolean takingPicture;

    private boolean optionsPanelVisible;
    private boolean isPrivateShot;
    private Message message;

    @Override
    public void onShotCreatedComplete(Shot shot)
    {

    }

    enum MediaType {PICTURE, VIDEO}

    private MediaType mediaType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cameraPreview = new CameraPreview(getActivity(), getCameraInstance());
        cameraPreview.setPictureTakenListener(this);
        if(getActivity() instanceof  ShotActivity){
            isPrivateShot = true;
            message = getActivity().getIntent().getParcelableExtra("message");
        } else {
            isPrivateShot = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isPrivateShot){

        }
        initializeCamera();
        setPicReadyMode();
        fillingRing.setProgressPercentage(0);
    }

    public void initializeCamera(){

        gestureDetector = new GestureDetector(getActivity(), new MyGestureListener());
        if(cameraPreview == null) {
            cameraPreview = new CameraPreview(getActivity(), getCameraInstance());
            cameraPreviewContainer.addView(cameraPreview);
            cameraPreview.setPictureTakenListener(this);
        }

        if(cameraPreview.getCamera() != null) {
            setCameraDisplayOrientation(getActivity(), 0, cameraPreview.getCamera());
        } else {
            // Create a camera instance
            // TODO: Have ability to get a specific camera available to the device if more than one is available
            try{
                if(getCameraInstance()!=null)
                {
                    cameraPreview.setCamera(getCameraInstance());
                }
                setCameraDisplayOrientation(getActivity(), 0, cameraPreview.getCamera());
            }catch(Exception e){
                e.printStackTrace();
                Bugsnag.notify(e);

                Toast toast = Toast.makeText(SpotShotApplication.getInstance(), "Camera was unable to open, please try restarting the application", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        removeCamera();
    }

    public void removeCamera(){
        if(cameraPreview.getCamera() != null) {
            cameraPreview.getCamera().release();
            cameraPreview.setCamera(null);
        }
        cameraPreviewContainer.removeView(cameraPreview);
        cameraPreview = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_shot, parent, false);
        setupViews();
        return rootView;
    }

    public void setupViews() {
//        optionsPanel = (RelativeLayout) rootView.findViewById(R.id.options_panel);
        darkTransparentOverlay = (FrameLayout) rootView.findViewById(R.id.dark_transparent_overlay);
        fillingRing = (FillingRing) rootView.findViewById(R.id.record_progress);

        cameraPreviewContainer = (FrameLayout) rootView.findViewById(R.id.camera_preview_container);
        cameraPreviewContainer.addView(cameraPreview);
        cameraPreviewContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

        createShotBtn = (ImageButton) rootView.findViewById(R.id.create_shot_btn);
        createShotBtn.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                int eventAction = event.getAction();

                if (eventAction == MotionEvent.ACTION_DOWN)
                {
                    Log.i(TAG, "Action down");
                    downTime = Calendar.getInstance().getTimeInMillis();
                    initializeTimerTask();
                    startTimer();
                    if(!isPrivateShot)
                    {
                        ((ViewPagerActivity) getActivity()).setPagingEnabled(false);
                    }
                } else if (eventAction == MotionEvent.ACTION_UP)
                {
                    Log.i(TAG, "Action up");
                    if(!isVideoOver12Secs)
                    {
                        onCameraButtonUp();
                    }
                }
                return true;
            }
        });

        cancelBtn = (ImageButton) rootView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bugsnag.leaveBreadcrumb("Cancel button pressed");
                setPicReadyMode();
                if (chosenCamera == 0)
                {
                    cameraPreview.toggleFlash(isFlashOn);
                } else {
                    cameraPreview.getCamera().startPreview();
                }
                cameraPreview.setState(CameraPreview.PREVIEW_STATE);
            }
        });

        allShotsBtn = (ImageButton) rootView.findViewById(R.id.all_shots_btn);
        allShotsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("All shots button pressed");
                ((ViewPagerActivity) getActivity()).changeFragment(ViewPagerActivity.SNAP_LIST_FRAGMENT_POS);
            }
        });
        if(isPrivateShot){
            allShotsBtn.setVisibility(View.GONE);
        }
        //TODO store flash preference from last time used? or maybe just default to false always
        isFlashOn = false;
        flashBtn = (ImageButton) rootView.findViewById(R.id.flash_btn);
        if(!hasFlash()){
            flashBtn.setVisibility(View.GONE);
        }
        flashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (chosenCamera == 0)
                {
                    if (isFlashOn)
                    {
                        Bugsnag.leaveBreadcrumb("Flash button pressed to put flash ON");
                        isFlashOn = false;
                        cameraPreview.toggleFlash(isFlashOn);
                        flashBtn.setImageDrawable(getResources().getDrawable(R.drawable.spotshot_propersize64_part4));
                    } else
                    {
                        Bugsnag.leaveBreadcrumb("Flash button pressed to put flash OFF");
                        isFlashOn = true;
                        cameraPreview.toggleFlash(isFlashOn);
                        flashBtn.setImageDrawable(getResources().getDrawable(R.drawable.spotshot_propersize64_part1));
                    }
                }
            }
        });


        sendMedia = (ImageButton) rootView.findViewById(R.id.send_media_btn);
        sendMedia.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bugsnag.leaveBreadcrumb("Upload media button pressed");
                Log.i(TAG, "send video button pressed");
                sendMedia();
            }
        });

        switchCameraBtn = (ImageButton) rootView.findViewById(R.id.switch_camera_btn);
        switchCameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bugsnag.leaveBreadcrumb("Switch camera button pressed");
                switchCamera();
            }
        });

        anonymousTooltip = (TextView) rootView.findViewById(R.id.anonymous_tooltip);
        if(SpotShotApplication.getSharedPreferences().getBoolean("anonymousTooltipUsed", false)) {
            anonymousTooltip.setVisibility(View.GONE);
        }
        anonymousBtn = (ImageButton) rootView.findViewById(R.id.anonymous);
        updateAnonymousBtn();
        anonymousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpotShotApplication.getSharedPreferences().edit().putBoolean("anonymousTooltipUsed", true).commit();
                anonymousTooltip.setVisibility(View.GONE);
                Toast toast;
                if(SpotShotApplication.getSharedPreferences().getBoolean("anonymousOn", false)) {
                    toast = Toast.makeText(getActivity(), "Ninja Mode Off!", Toast.LENGTH_SHORT);
                    SpotShotApplication.getSharedPreferences().edit().putBoolean("anonymousOn", false).commit();
                } else {
                    toast = Toast.makeText(getActivity(), "Ninja Mode On!", Toast.LENGTH_SHORT);
                    SpotShotApplication.getSharedPreferences().edit().putBoolean("anonymousOn", true).commit();
                }
                toast.show();
                updateAnonymousBtn();
            }
        });

        setPicReadyMode();
        initializeCamera();

    }

    private void onCameraButtonUp()
    {
        if(anonymousTooltip.getVisibility() != View.GONE) {
            SpotShotApplication.getSharedPreferences().edit().putBoolean("anonymousTooltipUsed", true).commit();
            anonymousTooltip.setVisibility(View.GONE);
        }
        flyingAngle = 0;
        resetAnimation();
        stoptimerTask();
        if (isRecording)
        {
            // TODO - BUG --> This should not take place when coming back into fragment after taking a video and then hitting take a pic button
            mediaType = MediaType.VIDEO;
            Log.i(TAG, "Stopped recording video");
            cameraPreview.videoFlashOff();
            cameraPreview.mediaRecorder.stop();
            cameraPreview.releaseMediaRecorder();
            isRecording = false;
            Bugsnag.leaveBreadcrumb("Video taken");
            Intent i = new Intent(getActivity(), PreviewVideoActivity.class);
            i.putExtra("filePath", cameraPreview.getFilePathExtension());
            i.putExtra("isPrivate", isPrivateShot);
            i.putExtra("message", message);
            startActivityForResult(i, VIDEO_CALLBACK_REQUEST_CODE);
            if(!isPrivateShot)
            {
                ((ViewPagerActivity) getActivity()).setPagingEnabled(true);
            }
        } else
        {
            mediaType = MediaType.PICTURE;
            Log.i(TAG, "take picture");
            Bugsnag.leaveBreadcrumb("Picture taken");
            fillingRing.setProgressPercentage(0);
            takePicture();
        }
    }

    private void setPicReadyMode(){
        fillingRing.setProgressPercentage(0);
        createShotBtn.setVisibility(View.VISIBLE);
        switchCameraBtn.setVisibility(View.VISIBLE);
        flashBtn.setVisibility(View.VISIBLE);
        anonymousBtn.setVisibility(View.VISIBLE);
        sendMedia.setVisibility(View.GONE);
        cancelBtn.setVisibility(View.GONE);
        isVideoOver12Secs = false;

        // Disable sliding panel
        rootView.setEnabled(false);

        // Enable viewpager swiping
        if(!isPrivateShot)
        {
            ((ViewPagerActivity) getActivity()).setPagingEnabled(true);
        }
    }

    private void setPicTakenMode() {
        allShotsBtn.setVisibility(View.GONE);
        fillingRing.setProgressPercentage(0);
        takingPicture = true;
        createShotBtn.setVisibility(View.GONE);
        switchCameraBtn.setVisibility(View.GONE);
        flashBtn.setVisibility(View.GONE);
        cancelBtn.setVisibility(View.VISIBLE);
        anonymousBtn.setVisibility(View.GONE);

        // Enable sliding panel
        rootView.setEnabled(true);
    }

    private void takePicture() {
        setPicTakenMode();
        if(chosenCamera == 0){
            cameraPreview.setCameraSide(true);
        } else {
            cameraPreview.setCameraSide(false);
        }
        cameraPreview.setState(CameraPreview.BUSY_STATE);
        try
        {
            Log.i(TAG, "Attempting to take picture");
            cameraPreview.getCamera().takePicture(null, null, cameraPreview.getPictureCallback());
        } catch(Exception e){
            Toast toast = Toast.makeText(SpotShotApplication.getInstance(), "Shot failed to create", Toast.LENGTH_SHORT);
            toast.show();
            e.printStackTrace();
        }
    }

    private void sendMedia(){
        setPicReadyMode();
        cameraPreview.setAwsListener(this);
        Log.i(TAG, "send video method called ");
        if(mediaType.equals(MediaType.PICTURE)){
            sendPicture();
        }
    }

    private void sendPicture(){

        cameraPreview.sendPicture(SpotShotApplication.getSharedPreferences().getBoolean("anonymousOn", false), isPrivateShot, message);
        if(isPrivateShot){
            getActivity().finish();
        } else {
            cameraPreview.getCamera().startPreview();
            cameraPreview.setState(CameraPreview.PREVIEW_STATE);
        }
    }

    /** Check how many cameras this device has */
    private int checkCameraHardware() {
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return Camera.getNumberOfCameras();
        } else {
            return 0;
        }
    }

    private boolean hasFlash(){
        return getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

    }

    /** Convenience method for checking has at least one camera */
    private boolean hasCamera() {
        return checkCameraHardware() != 0;
    }

    private Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(chosenCamera);
        } catch (Exception e) {
            Log.e(TAG, "Camera is not available");
            Bugsnag.notify(e);
            e.printStackTrace();
        }
        return c;
    }


    private void switchCamera() {
        switchChosenCamera();
        if(cameraPreview.getCamera() != null) {
            cameraPreview.getCamera().release();
            cameraPreview.setCamera(null);
        }
        cameraPreviewContainer.removeView(cameraPreview);
        cameraPreview = null;
        cameraPreview = new CameraPreview(getActivity(), getCameraInstance());
        cameraPreview.setPictureTakenListener(this);
        if(chosenCamera == 0) {
            cameraPreview.setCameraSide(true);
        } else {
            cameraPreview.setCameraSide(false);
        }
        setCameraDisplayOrientation(getActivity(), chosenCamera, cameraPreview.getCamera());
        cameraPreviewContainer.addView(cameraPreview);
    }


    private void switchChosenCamera() {
        if(checkCameraHardware() > 1) {
            if(chosenCamera == 0) {
                chosenCamera = 1;
            } else {
                chosenCamera = 0;
            }
        }
    }

    private void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 10, 50);
    }

    public void stoptimerTask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            int i =0;
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        //get the current timeStamp
                        long currentTime = Calendar.getInstance().getTimeInMillis();
                        long timeInMilliseconds = (currentTime - downTime);
                        if(timeInMilliseconds>200 && !takingPicture){
                            if(!isRecording)
                            {
                                if (cameraPreview.prepareVideoRecorder(chosenCamera==0))
                                {
                                    // Camera is available and unlocked, MediaRecorder is prepared,
                                    // now you can start recording
                                    Log.i(TAG, "Started recording video");
                                    //TODO app crashed onthe line below
                                    cameraPreview.mediaRecorder.start();
                                    if(isFlashOn && chosenCamera == 0)
                                    {
                                        cameraPreview.videoFlashOn();
                                    }
                                } else
                                {
                                    Log.i(TAG, "release media recorder");
                                    cameraPreview.releaseMediaRecorder();
                                    cameraPreview.setState(CameraPreview.FROZEN_STATE);
                                }
                                isRecording = true;
                            }
                        }
                        float percentageDone = (float) (timeInMilliseconds/12000.0 * 100);
                        fillingRing.setProgressPercentage(percentageDone);
                        i++;
                        if(i>20 ){
                           doVideoAnimation();
                            i=0;
                        } else if (i == 8){
                            resetAnimation();
                        }
                    }
                });
            }
        };
    }

    private void doVideoAnimation(){
        flyingAngle++;
        if(flyingAngle == 11){
            isVideoOver12Secs = true;
            onCameraButtonUp();
        }
    }

    private void resetAnimation(){
        ImageView heartIconAnimationImage = (ImageView) rootView.findViewById(R.id.heart_icon_animation);
        heartIconAnimationImage.setVisibility(View.GONE);
        heartIconAnimationImage.setAnimation(null);
    }

    @Override
    public void onPictureTaken() {
        sendMedia.setVisibility(View.VISIBLE);
        takingPicture = false;
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            Log.d(TAG, "onFling: " + event1.toString() + event2.toString());

            float deltaY = event2.getY() - event1.getY();

            if(!rootView.isEnabled()) {
                if (deltaY > 200 && event1.getY() < 100) {
                    // Open settings activity
                    Intent i = new Intent(getActivity(), SettingsActivity.class);
                    startActivity(i);
                }
            }

            return super.onFling(event1, event2, velocityX, velocityY);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(isPrivateShot)
        {
            if (requestCode == VIDEO_CALLBACK_REQUEST_CODE && resultCode == VIDEO_SENT_CODE)
            {
                getActivity().finish();
            }
        }
    }

    public void updateAnonymousBtn() {
        if(SpotShotApplication.getSharedPreferences().getBoolean("anonymousOn", false)) {
            anonymousBtn.setImageDrawable(getResources().getDrawable(R.drawable.spotshot_propersize64_part5));
        } else {
            anonymousBtn.setImageDrawable(getResources().getDrawable(R.drawable.spotshot_propersize64_part2));
        }
    }
}
