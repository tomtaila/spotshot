package tmtm.SpotShot.activities.shot;

import tmtm.SpotShot.model.Shot;

/**
 * Created by ttaila on 6/25/15.
 */
public interface OnShotCreatedListener
{

    void onShotCreatedComplete(Shot shot);

}
