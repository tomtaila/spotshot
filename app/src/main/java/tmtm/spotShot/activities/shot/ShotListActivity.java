package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

import tmtm.SpotShot.R;
import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by ttaila on 3/7/15.
 */
public class ShotListActivity extends SingleFragmentActivity{

    @Override
    public Fragment createFragment() {
        return new ShotListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = "SnapListActivity";
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_shot_list);

        fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }
}
