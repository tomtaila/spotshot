package tmtm.SpotShot.activities.shot;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bugsnag.android.Bugsnag;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import tmtm.SpotShot.MathUtil;
import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.TimeUtil;
import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Like;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.ui.SnapThumbnailImageView;
import tmtm.SpotShot.ui.TextUtil;

/**
 * Created by ttaila on 3/7/15.
 */
public class ShotArrayAdapter extends ArrayAdapter<Shot>{

    private Context context;
    private List<Shot> shotList;
    private ThumbnailDownloader<ImageView> thumbnailDownloaderThread;
    private int sortByFlag;

    public ShotArrayAdapter(Context context, int resource, List<Shot> shotList, ThumbnailDownloader thumbnailDownloaderThread, int sortByFlag) {
        super(context, resource);
        this.context = context;
        this.shotList = shotList;
        this.thumbnailDownloaderThread = thumbnailDownloaderThread;
        this.sortByFlag = sortByFlag;
    }

    public void setList(List<Shot> shotList){
        this.shotList = shotList;
    }


    @Override
    public Shot getItem(int position) {
        return shotList.get(position);
    }

    @Override
    public int getCount() {
        return shotList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            if(sortByFlag == ShotListFragment.USERS_OWN_FLAG) {
                convertView = inflater.inflate(R.layout.users_own_shot_list_item, null);
            } else {
                convertView = inflater.inflate(R.layout.shot_list_item, null);
            }
            holder = new ViewHolder(convertView);
            holder.distanceTV = (TextView) convertView.findViewById(R.id.distance_tv);
            holder.timeAgoTV = (TextView) convertView.findViewById(R.id.time_ago_tv);
            holder.scoreTV = (TextView) convertView.findViewById(R.id.score_tv);
            holder.heartBtn = (Button) convertView.findViewById(R.id.heart_icon);
            holder.deleteBtn = (Button) convertView.findViewById(R.id.flag_icon);
            holder.heartIconAnimationImage = (ImageView) convertView.findViewById(R.id.heart_icon_animation);
            holder.userNameTV = (TextView) convertView.findViewById(R.id.user_name);
            holder.shotPreviewWrapper = (ViewGroup) convertView.findViewById(R.id.shot_preview_wrapper);
            holder.thumbnailView = (ImageView) convertView.findViewById(R.id.shot_preview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Shot shot = getItem(position);
        populateView(holder, shot, position);

        if(!shot.isMediaDownloaded()){
            convertView.setAlpha((float) 0.25);
        } else {
            convertView.setAlpha((float) 1);

        }

        return convertView;
    }

    private void populateView(final ViewHolder holder, final Shot shot, final int position) {
        if(shot.getNsfwTag() == 0) {
//            v.setBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));
            // TODO
        } else {
//            v.setBackgroundColor(context.getResources().getColor(android.R.color.holo_orange_light));
            // TODO
        }

        ViewGroup shotPreviewWrapper = holder.shotPreviewWrapper;
        SnapThumbnailImageView shotThumbnailImageView = new SnapThumbnailImageView(context, shot);
        shotPreviewWrapper.addView(shotThumbnailImageView);

        ImageView thumbnailView = holder.thumbnailView;
        thumbnailView.bringToFront();

        if(thumbnailDownloaderThread != null) {
            thumbnailDownloaderThread.queueThumbnail(thumbnailView, shot.getThumbnailUrl());
        }

        float distanceInMetres = LocationHelper.getLastLocation().distanceTo(shot.getLocation());
        String distanceStr;
        SpannableString spannedDistanceStr;
        int distance = 0;
        if(SpotShotApplication.mUnitsOfDistance == SpotShotApplication.MILES) {
            distance = MathUtil.metresToMiles(distanceInMetres);
            distanceStr = distance + "mi away";
            spannedDistanceStr = TextUtil.spanIt(distanceStr);
        } else {
            distance = MathUtil.metresToKm(distanceInMetres);
            distanceStr = distance + "km away";
            spannedDistanceStr = TextUtil.spanIt(distanceStr);
        }
        TextView distanceTV = holder.distanceTV;

        distanceTV.setText(spannedDistanceStr);

        String dateStr = TimeUtil.generateTimeAgoString(shot.getTimeCreated());
        SpannableString dateStrSpanned = TextUtil.spanIt(dateStr);
        TextView timeAgoTV = holder.timeAgoTV;
        if(shot.isSuccessfullyUploaded())
        {
            timeAgoTV.setText(dateStrSpanned);
        } else if (!SpotShotApplication.getShotsUploading().contains(shot)){
            timeAgoTV.setText(context.getString(R.string.tap_to_retry));
        } else {
            timeAgoTV.setText(context.getString(R.string.posting));

        }
        TextView scoreTV = holder.scoreTV;
        scoreTV.setText(String.valueOf(shot.getScore()));

        TextView userNameTV = holder.userNameTV;
        if(!shot.isAnonymous()) {
            userNameTV.setText(TextUtil.capitalize(shot.getUsername()));
        } else {
            if(shot.getUsername().equals(context.getText(R.string.loading))){
                userNameTV.setText(shot.getUsername());
            } else
            {
                userNameTV.setText(context.getText(R.string.ninja));
            }
        }

        final Button heartBtn = holder.heartBtn;

        boolean likeExists = false;
        String likeUuid = null;
        for(Like l : SpotShotApplication.getInstance().getCurrentUserLikes()) {
            if(l.getShotUuid().equals(shot.getUuid())) {
                likeExists = true;
                likeUuid = l.getLikeUuid();
            }
        }
        final String finalLikeUuid = likeUuid;
        if(likeExists) {
            heartBtn.setBackgroundResource(R.drawable.heart_on);
            heartBtn.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Bugsnag.leaveBreadcrumb("User pressed dislike heart button");
                    heartBtn.setBackgroundResource(R.drawable.heart_off);
                    ImageView heartIconAnimationImage = holder.heartIconAnimationImage;
                    heartIconAnimationImage.setVisibility(View.INVISIBLE);
                    shot.setScore(shot.getScore() - 1);
                    Like like = new Like(finalLikeUuid, SpotShotApplication.getCurrentUser().getUuid(), shot.getUuid());
                    ServerShotUtil serverShotUtil = new ServerShotUtil(context);
                    serverShotUtil.dislikeShot(shot, like);
                    SpotShotApplication.removeLike(like);
                    notifyDataSetChanged();
                }
            });
        } else {
            heartBtn.setBackgroundResource(R.drawable.heart_off);
            heartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bugsnag.leaveBreadcrumb("User pressed like heart button");
                    Animation fadeOut = new AlphaAnimation(1, 0);
                    fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
                    fadeOut.setDuration(400);

                    float yTranslation = -SpotShotApplication.convertDpToPx(R.dimen.heart_y_translation);
                    TranslateAnimation moveUp = new TranslateAnimation(0.0f, 0.0f, 0.0f, yTranslation);
                    moveUp.setDuration(400);

                    AnimationSet animation = new AnimationSet(false); //change to false
                    animation.addAnimation(fadeOut);
                    animation.addAnimation(moveUp);
                    final ImageView heartIconAnimationImage = holder.heartIconAnimationImage;
                    heartIconAnimationImage.setVisibility(View.VISIBLE);
                    heartIconAnimationImage.setAnimation(animation);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            heartIconAnimationImage.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    animation.start();

                    heartBtn.setBackgroundResource(R.drawable.heart_on);
                    shot.setScore(shot.getScore() + 1);
                    Like like = new Like(UUID.randomUUID().toString(), SpotShotApplication.getCurrentUser().getUuid(), shot.getUuid());
                    SpotShotApplication.storeLike(like);
                    ServerShotUtil serverShotUtil = new ServerShotUtil(context);
                    serverShotUtil.likeShot(shot, like);
                    notifyDataSetChanged();
                }
            });

            if(sortByFlag == ShotListFragment.USERS_OWN_FLAG) {
                holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new ServerShotUtil(getContext()).flagShotDeleted(shot);
                        shotList.remove(position);
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }

    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private Button heartBtn;
        private Button deleteBtn;
        private ImageView heartIconAnimationImage;
        private TextView userNameTV;
        private TextView scoreTV;
        private TextView timeAgoTV;
        private TextView distanceTV;
        private ViewGroup shotPreviewWrapper;
        private ImageView thumbnailView;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
