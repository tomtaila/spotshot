package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.LocationHelper;

/**
 * Created by mwszedybyl on 5/17/15.
 */
public class StaticMapShotFragment extends Fragment
{
    private ImageView staticMapIV;
    private View rootView;
    private ImageButton directionsBtn;
    private TextView fabTooltip;
    private double lat;
    private double lng;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.status_map_shot_fragment, parent, false);
        setupViews();

        return rootView;
    }

    private void setupViews(){
        staticMapIV = (ImageView) rootView.findViewById(R.id.static_map);

        final Intent intent = getActivity().getIntent();
        lat = intent.getDoubleExtra("lat", 0.0);
        lng = intent.getDoubleExtra("long", 0.0);
        final Location loc = new Location("");
        loc.setLatitude(lat);
        loc.setLongitude(lng);

        directionsBtn = (ImageButton) rootView.findViewById(R.id.directions_btn);
        fabTooltip = (TextView) rootView.findViewById(R.id.fab_tooltip);
        if(SpotShotApplication.getSharedPreferences().getBoolean("directionsFabTooltip", false)) {
            fabTooltip.setVisibility(View.GONE);
        }

        directionsBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SpotShotApplication.getSharedPreferences().edit().putBoolean("directionsFabTooltip", true).commit();
                fabTooltip.setVisibility(View.GONE);
                if (LocationHelper.getLastLocation() != null)
                {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + LocationHelper.getLastLocation().getLatitude() + "," + LocationHelper.getLastLocation().getLongitude() + "&daddr=" + loc.getLatitude() + "," + loc.getLongitude()));
                    startActivity(intent);
                } else
                {
                    Toast toast = Toast.makeText(getActivity(), "Current location undefined", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        new GetStaticMapImageTask().execute();
    }

    private class GetStaticMapImageTask extends AsyncTask<Void, String, Bitmap>
    {

        @Override
        protected void onPostExecute(Bitmap bmp){
            staticMapIV.setImageBitmap(bmp);
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            // TODO Auto-generated method stub
            Bitmap bm = getGoogleMapThumbnail(lat, lng);
            return bm;

        }
    }

    public static Bitmap getGoogleMapThumbnail(double lat, double lng){

        String URL = "http://maps.google.com/maps/api/staticmap?center=" +lat + "," + lng+ "&zoom=15&size=800x600&sensor=false?&markers=label:H%7C" + lat + "," + lng;

        Bitmap bmp = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet(URL);

        InputStream in = null;
        try {
            in = httpclient.execute(request).getEntity().getContent();
            bmp = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return bmp;
    }

}
