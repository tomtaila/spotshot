package tmtm.SpotShot.activities.shot;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.ui.CustomViewPager;

/**
 * Created by ttaila on 3/8/15.
 */
public class DisplayMediaActivity extends FragmentActivity
{

    private SpotShotApplication app;
    private ShotAdapter pagerAdapter;
    private CustomViewPager viewPager;
    private boolean isVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = SpotShotApplication.getInstance();
        setContentView(R.layout.activity_main);
        Bundle extras = getIntent().getExtras();
        String mediaUrl = (String) extras.get("mediaUrl");
        if(mediaUrl.contains("jpg"))
        {
            isVideo = false;
        }
        else {
            isVideo = true;
        }

        pagerAdapter = new ShotAdapter(getFragmentManager());
        pagerAdapter.setIsVideo(isVideo);
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2);

        viewPager.setCurrentItem(0);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}

