package tmtm.SpotShot.activities.shot;

/**
 * Created by ttaila on 4/21/15.
 */
public interface HotNewHeaderButtonListener {

    void onTopBtnClick();
    void onNewBtnClick();

}
