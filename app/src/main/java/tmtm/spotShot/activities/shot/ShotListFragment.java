package tmtm.SpotShot.activities.shot;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.bugsnag.android.Bugsnag;
import com.melnykov.fab.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tmtm.SpotShot.AWSHelper;
import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.TimeUtil;
import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.activities.map.MapActivity;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.services.UploadMediaService;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Created by ttaila on 4/2/15.
 */
public class ShotListFragment extends ListFragment implements GetShotsTaskListener, PreloadThumbnailTaskListener, OnShotCreatedListener, OnRefreshListener
{
    public static final String TAG = "ShotListFragment";

    public static final int SHOTS_PER_PAGE = 20;

    public static final int SORT_BY_TOP_FLAG = 0;
    public static final int SORT_BY_NEWEST_FLAG = 1;
    public static final int USERS_OWN_FLAG = 2;
    private View rootView;
    private List<Shot> shotList;
    private PullToRefreshLayout mPullToRefreshLayout;
    private ListView listView;
    private ShotArrayAdapter arrayAdapter;
    private FloatingActionButton mapViewFab;
    private TextView fabTooltip;
    private ServerShotUtil serverShotUtil;
    private ThumbnailDownloader<ImageView> thumbnailDownloaderThread;
    private boolean loadMedia;
    private int sortByFlag;
    private int pageNum;
    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle args = this.getArguments();
        this.sortByFlag = args.getInt("sortByFlag");

        loadMedia = true;
        serverShotUtil = new ServerShotUtil(getActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        Log.d(TAG, "onCreateView()");

        thumbnailDownloaderThread = new ThumbnailDownloader<>(new Handler());
        thumbnailDownloaderThread.setListener(new ThumbnailDownloader.Listener<ImageView>()
        {
            public void onThumbnailDownloaded(ImageView imageView, Bitmap thumbnail)
            {
                if (isVisible())
                {
                    imageView.setImageBitmap(thumbnail);
                }
            }
        });
        thumbnailDownloaderThread.start();
        thumbnailDownloaderThread.getLooper();
        Log.i(TAG, "backgroundThread started");

        rootView = inflater.inflate(R.layout.fragment_shot_list, parent, false);
        setupViews();

        return rootView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.d(TAG, "onResume() called.");
        arrayAdapter.notifyDataSetChanged();
    }

    private void setupViews()
    {
        mPullToRefreshLayout = (PullToRefreshLayout) rootView.findViewById(R.id.ptr_layout);
        // Now setup the PullToRefreshLayout
        ActionBarPullToRefresh.from(getActivity())
                // Mark All Children as pullable
                .allChildrenArePullable()
                // Set a OnRefreshListener
                .listener(this)
                // Finally commit the setup to our PullToRefreshLayout
                .setup(mPullToRefreshLayout);


        listView = (ListView) rootView.findViewById(android.R.id.list);
        listView.setDivider(null);
        shotList = new ArrayList<>();
        if (sortByFlag == USERS_OWN_FLAG)
        {
            shotList.addAll(DbHelper.getUserShots(SpotShotApplication.getCurrentUser()));
            Collections.sort(shotList, new Shot.TimeCreatedComparator());

        } else
        {
            shotList.addAll(SpotShotApplication.getAllShotsFromDatabase(sortByFlag, SpotShotApplication.getSharedPreferences().getInt("pageNum", 0), true));
            Button footerView = (Button) ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.load_more_layout, null, false);
            listView.addFooterView(footerView);
            footerView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
//                    pageNum++;
                    SpotShotApplication.getSharedPreferences().edit().putInt("pageNum", SpotShotApplication.getSharedPreferences().getInt("pageNum", 0) + 1).commit();
                    refreshShotsFromServer();
                }
            });
        }

        arrayAdapter = new ShotArrayAdapter(getActivity(), R.layout.shot_list_item, shotList, thumbnailDownloaderThread, sortByFlag);
        setListAdapter(arrayAdapter);

        mapViewFab = (FloatingActionButton) rootView.findViewById(R.id.map_fab);
        mapViewFab.attachToListView(listView);
        fabTooltip = (TextView) rootView.findViewById(R.id.fab_tooltip);
        if(SpotShotApplication.getSharedPreferences().getBoolean("mapFabTooltip", false))
        {
            fabTooltip.setVisibility(View.GONE);
        }

        mapViewFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SpotShotApplication.getSharedPreferences().edit().putBoolean("mapFabTooltip", true).commit();
                fabTooltip.setVisibility(View.GONE);
                Intent i = new Intent(getActivity(), MapActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        Shot shot = arrayAdapter.getItem(position);
        Bugsnag.leaveBreadcrumb("User clicked on a one of the shots, shot uuid = " + shot.getUuid());
        if (shot.isSuccessfullyUploaded())
        {
            if(!shot.isMediaDownloaded()){
                if(shot.getUsername()!=getActivity().getBaseContext().getText(R.string.loading).toString())
                {
                    AWSImageLoader awsImageLoader = new AWSImageLoader();
                    awsImageLoader.setShot(shot, position);
                    awsImageLoader.execute();
                }
            } else
            {
                Intent i = new Intent(getActivity(), DisplayMediaActivity.class);
                String timeAgo = TimeUtil.generateTimeAgoString(shot.getTimeCreated());
                float distanceInMeters = LocationHelper.getLastLocation().distanceTo(shot.getLocation());
                double distanceInMiles = distanceInMeters / 1609.34;
                i.putExtra("mediaUrl", shot.getUrl());
                i.putExtra("thumbnailUrl", shot.getThumbnailUrl());
                i.putExtra("username", "" + shot.getUsername());
                i.putExtra("anonymous", shot.isAnonymous());
                i.putExtra("lat", shot.getLocation().getLatitude());
                i.putExtra("long", shot.getLocation().getLongitude());
                i.putExtra("distance", String.format("%.2f", distanceInMiles));
                i.putExtra("timeElapsed", timeAgo);
                startActivity(i);
            }
        } else if (!SpotShotApplication.getShotsUploading().contains(shot))
        {
            // Upload image to AWS
            Intent intent = new Intent(getActivity().getBaseContext(), UploadMediaService.class);
            intent.putExtra("filePath", shot.getUrl());
            intent.putExtra("shot", shot);

            getActivity().getBaseContext().startService(intent);
        }
    }

    private class AWSImageLoader extends AsyncTask<Void, Void, Void>
    {
        Shot shot;
        int position;
        String username;
        S3ObjectInputStream inputStream;
        OutputStream outputStream = null;
        String result = null;
        File videoFile = null;

        protected void setShot(Shot shot, int position) {
            this.shot = shot;
            this.position = position;
            if(shot.isVideo())
            {
                videoFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, shot.getUrl());
            }
        }

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute() called");

            username = shotList.get(position).getUsername();
            shotList.get(position).setUsername(getActivity().getBaseContext().getText(R.string.loading).toString());
            arrayAdapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ConnectivityManager connMgr = (ConnectivityManager) getActivity().getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
            {
                try
                {
                    if (!shot.isVideo())
                    {
                        Bitmap bitmap = AWSHelper.getRemoteImageAsBitmap(shot.getUrl());
                        storeMediaToAppFolder(shot.getUrl(), bitmap);
                    } else
                    {
                        inputStream = AWSHelper.getRemoteVideo(shot.getUrl());
                        try
                        {
                            // write the inputStream to a FileOutputStream
                            outputStream = new FileOutputStream(videoFile);

                            int read = 0;
                            byte[] bytes = new byte[1024];

                            while ((read = inputStream.read(bytes)) != -1)
                            {
                                outputStream.write(bytes, 0, read);
                            }
                        } catch (IOException e)
                        {
                            e.printStackTrace();
                            shotList.get(position).setUsername(username);
                        } finally
                        {
                            if (inputStream != null)
                            {
                                try
                                {
                                    inputStream.close();
                                } catch (IOException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                            if (outputStream != null)
                            {
                                try
                                {
                                    outputStream.close();
                                } catch (IOException e)
                                {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                    result = "error";
                } finally
                {
                    result = "success";
                }
            } else {
                result = "no internet";
            }
            return null;
        }

        private  void storeMediaToAppFolder(String mediaName, Bitmap bitmap)
        {
            try
            {
                File mediaFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, mediaName);
                FileOutputStream outputStream = new FileOutputStream(mediaFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                outputStream.flush();
                outputStream.close(); // do not forget to close the stream

            } catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Void v) {
            Log.i(TAG, "onPostExecute() called");
            Toast toast;
            switch(result){
                case "success":
                    toast = Toast.makeText(SpotShotApplication.getInstance(), "Download finished", Toast.LENGTH_SHORT);
                    toast.show();
                    shotList.get(position).setMediaDownloaded(true);
                    loadRunnable.run();
                    break;
                case "no internet":
                    toast = Toast.makeText(SpotShotApplication.getInstance(), "No internet connection", Toast.LENGTH_SHORT);
                    toast.show();
                    break;
                case "error":
                    toast = Toast.makeText(SpotShotApplication.getInstance(), "error", Toast.LENGTH_SHORT);
                    toast.show();
                    break;

            }
            shotList.get(position).setUsername(username);
            pullNewShotListFromDatabase();
        }


        private Runnable loadRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                DbHelper.commitOrUpdateShot(shot);
            }
        };

    }

    private void pullNewShotListFromDatabase()
    {
        arrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void doOnComplete(List<Shot> shots)
    {
        shotList.clear();
        if (sortByFlag == USERS_OWN_FLAG)
        {
            DbHelper.commitOrUpdateShots(shots);
            shotList.addAll(DbHelper.getUserShots(SpotShotApplication.getCurrentUser()));
            Collections.sort(shotList, new Shot.TimeCreatedComparator());
        } else
        {
            DbHelper.commitOrUpdateShots(shots);
            shotList.addAll(SpotShotApplication.getAllShotsFromDatabase(sortByFlag, SpotShotApplication.getSharedPreferences().getInt("pageNum", 0), true));
        }

        arrayAdapter.notifyDataSetChanged();
        new PreloadThumbnailTask(SpotShotApplication.getInstance(), true, SpotShotApplication.getSharedPreferences().getInt("pageNum", 0), this).execute(shots);
        mPullToRefreshLayout.setRefreshComplete();
    }

    public void refreshShotsFromServer()
    {
        if (sortByFlag == USERS_OWN_FLAG)
        {
            serverShotUtil.getShotsFromUser(SpotShotApplication.getCurrentUser());
        } else
        {
            serverShotUtil.getShots(LocationHelper.getLastLocation(), SpotShotApplication.getSharedPreferences().getInt("maxRadius", 50), sortByFlag);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        thumbnailDownloaderThread.quit();
        Log.i(TAG, "background thread destroyed");
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        thumbnailDownloaderThread.clearQueue();
    }

    @Override
    public void doOnComplete()
    {

        arrayAdapter.notifyDataSetChanged();
        Log.d(TAG, "PreloadThumbnailTaskListener.doOnComplete() is called");
        if (loadMedia)
        {
            PreloadThumbnailTask preloadThumbnailTask = new PreloadThumbnailTask(SpotShotApplication.getInstance(), false, SpotShotApplication.getSharedPreferences().getInt("pageNum", 0));
            if (sortByFlag == USERS_OWN_FLAG)
            {
                preloadThumbnailTask.execute(DbHelper.getUserShots(SpotShotApplication.getCurrentUser()));
            } else
            {
                preloadThumbnailTask.execute(SpotShotApplication.getAllShotsFromDatabase(sortByFlag, SpotShotApplication.getSharedPreferences().getInt("pageNum", 0)));
            }
        }
    }

    @Override
    public void updateShots()
    {
        shotList.clear();
        if (sortByFlag == USERS_OWN_FLAG)
        {
            shotList.addAll(DbHelper.getUserShots(SpotShotApplication.getCurrentUser()));
            Collections.sort(shotList, new Shot.TimeCreatedComparator());
        } else
        {
            shotList.addAll(SpotShotApplication.getAllShotsFromDatabase(sortByFlag, pageNum, true));
        }
        arrayAdapter.notifyDataSetChanged();

    }

    public void refreshShots()
    {
        if (sortByFlag == USERS_OWN_FLAG)
        {
            serverShotUtil.getShotsFromUser(SpotShotApplication.getCurrentUser());
        } else
        {
            serverShotUtil.getShots(LocationHelper.getLastLocation(), SpotShotApplication.getSharedPreferences().getInt("maxRadius", 50), sortByFlag);
        }
    }

    @Override
    public void onShotCreatedComplete(Shot shot) {
        shotList.clear();
        shotList.addAll(DbHelper.getUserShots(SpotShotApplication.getCurrentUser()));
    }

    /** START http://stackoverflow.com/questions/14
     * 929907/causing-a-java-illegalstateexception-error-no-activity-only-when-navigating-to */
    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try
        {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e)
        {
            Log.e(TAG, "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }

    @Override
    public void onDetach () {
    super.onDetach();
        if (sChildFragmentManagerField != null)
        {
            try
            {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e)
            {
                Log.e(TAG, "Error setting mChildFragmentManager field", e);
            }
        }
    }

    /** END */

    @Override
    public void onRefreshStarted(View view) {
        Log.d(TAG, "onRefreshStarted...");
        refreshShots();
    }
}
