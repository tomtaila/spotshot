package tmtm.SpotShot.activities.shot;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.location.Location;
import android.media.CamcorderProfile;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.activities.chat.ServerChatUtil;
import tmtm.SpotShot.model.Message;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.services.UploadMediaService;

/**
 * Created by ttaila on 2/28/15.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "CameraPreview";

    public static final int PREVIEW_STATE = 0;
    public static final int BUSY_STATE = 1;
    public static final int FROZEN_STATE = 2;

    private int state;
    private OnShotCreatedListener awsListener;
    private Camera camera;
    private SurfaceHolder surfaceHolder;
    private String filePathExtension;
    public MediaRecorder mediaRecorder;
    private boolean isBackCameraOn;
    private PictureTakenListener pictureTakenListener;
    private String latestFilePath;

    private Bitmap storedBitmap;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        this.camera = camera;
        state = PREVIEW_STATE;

        /* Install a SurfaceHolder.Callback so we get notified when the
         underlying surface is created and destroyed.*/
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public void setAwsListener(OnShotCreatedListener listener){
        this.awsListener = listener;
    }

    public void setPictureTakenListener(PictureTakenListener pictureTakenListener) {
        this.pictureTakenListener = pictureTakenListener;
    }

    /** The Surface has been created, now tell the camera where to draw the preview.*/
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if(camera != null) {
                camera.setPreviewDisplay(holder);
                Camera.Parameters p = camera.getParameters();
                if(isBackCameraOn)
                {

                } else {

                }

                int exposure = p.getMaxExposureCompensation()/2;
                p.setExposureCompensation(exposure);
                p.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

                Camera.Size previewSize = getOptimalPreviewSize(p.getSupportedPreviewSizes(), 16 ,9);
                p.setPreviewSize(previewSize.width, previewSize.height);
                Camera.Size picSize = getOptimalPreviewSize(p.getSupportedPictureSizes(), 16 ,9);
                p.setPictureSize(picSize.width, picSize.height);

                camera.setParameters(p);
                camera.startPreview();
            }
        } catch (IOException e) {
            Log.e(TAG, "Error setting camera preview " + e.getMessage());
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.05;
        double targetRatio = (double) w/h;

        if (sizes==null) return null;

        Camera.Size optimalSize = null;

        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Find size
        for (Camera.Size size : sizes) {
            if(size.height <2000 || size.width<2000)
            {
                double ratio = (double) size.width / size.height;
                if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
                if (Math.abs(size.height - targetHeight) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    /** If your preview can change or rotate, take care of those events here.
      * Make sure to stop the preview before resizing or reformatting it.
      */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if(holder.getSurface() == null) {
            // preview surface does not exist
            return;
        }
        // stop preview before making changes
        try {
            if(camera != null) {
                camera.stopPreview();
            }
        } catch (Exception e) {
            Log.e(TAG, "Trying to stop a non existing preview");
        }

        // TODO: set preview size and make any resize, rotate or reformatting changes here

        // start preview with new settings
        try {
            if(camera != null) {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            }
        } catch (IOException e) {
            Log.e(TAG, "Error setting camera preview " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    private Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.d(TAG, "data = " + data);
            state = FROZEN_STATE;

            File pictureFile = getOutputMediaFile(false);

            if (pictureFile == null) {
                Log.d(TAG, "picture file is null");
                return;
            }
            try {
                // Rotate to correct
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = false;
                opts.inPreferredConfig = Bitmap.Config.RGB_565;
                opts.inDither = true;
                storedBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
                Matrix matrix = new Matrix();
                if(isBackCameraOn)
                {
                    matrix.postRotate(90);
                } else {
                    matrix.postRotate(270);
                }
                storedBitmap = Bitmap.createBitmap(storedBitmap, 0, 0, storedBitmap.getWidth(), storedBitmap.getHeight(), matrix, true);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                storedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                // TODO: call pictureTakenListener.onBitmapReady();


                data = byteArrayOutputStream.toByteArray();

                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Log.i(TAG, "Wrote file " + pictureFile.getName() + " in path " + pictureFile.getPath());

                if(pictureTakenListener != null) {
                    pictureTakenListener.onPictureTaken();
                }
            } catch (FileNotFoundException e) {
                Log.e(TAG, "File not found exception thrown: " + e.getMessage());
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            }
        }
    };

    public Camera.PictureCallback getPictureCallback() {
        return pictureCallback;
    }

    public void sendPicture(boolean anonymous, boolean isShotActivity, Message message){

        // Create shot object
        Location location = new Location("");
        if(LocationHelper.getLastLocation() != null) {
            location = LocationHelper.getLastLocation();
        }

        //Create shot
        Shot shot = new Shot( UUID.randomUUID().toString(), SpotShotApplication.getCurrentUser().getUuid(), location.getLatitude(), location.getLongitude(), filePathExtension, System.currentTimeMillis(), "caption default", false);
        shot.setAnonymous(anonymous);
        shot.setNsfwTag(0);
        shot.setMediaDownloaded(true);
        shot.setUsername(SpotShotApplication.getCurrentUser().getName());
        if(isShotActivity)
        {
            message.setBody(shot.getUrl());
            ServerChatUtil.createMessage(message, null);
        }

        // Upload image
        Intent intent = new Intent(getContext(), UploadMediaService.class);
        intent.putExtra("filePath", filePathExtension);
        intent.putExtra("shot", shot);
        intent.putExtra("isPrivate", isShotActivity);
        getContext().startService(intent);
    }

    public int getState() {
        return state;
    }

    private File getOutputMediaFile(boolean isVideo) {
        File mediaStorageDir = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }
        if(isVideo){
            filePathExtension = "vid-" + System.currentTimeMillis() + ".mp4";
        } else {
            filePathExtension = "img-" + System.currentTimeMillis() + ".jpg";
        }
        // Create a media file name
        return new File(mediaStorageDir.getPath(), filePathExtension);
    }

    public String getFilePathExtension() {
        return filePathExtension;
    }

    public void setCameraSide(boolean isBackCameraOn){
        this.isBackCameraOn = isBackCameraOn;
    }

    public boolean prepareVideoRecorder(boolean isBackCamera){

        mediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        camera.unlock();

        mediaRecorder.setCamera(camera);
        // Step 2: Set sources
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        if(isBackCamera)
        {
            mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));
            mediaRecorder.setOrientationHint(90);
        }
        else {
            mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));
            mediaRecorder.setOrientationHint(270);
        }

        // Step 4: Set output file
        mediaRecorder.setOutputFile(getOutputMediaFile(true).toString());

        // Step 5: Set the preview output
        mediaRecorder.setPreviewDisplay(getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    public void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            camera.lock();           // lock camera for later use
        }
    }

    public void releaseCamera(){
        if (camera != null){
            camera.release();        // release the camera for other applications
            camera = null;
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    public void toggleFlash(boolean isFlashOn){
        if(isFlashOn)
        {
            Camera.Parameters p = camera.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            int exposure = p.getMinExposureCompensation()/2;
            p.setExposureCompensation(exposure);
            ;camera.setParameters(p);
            camera.startPreview();
        } else {
            Camera.Parameters p = camera.getParameters();
            int exposure = p.getMaxExposureCompensation()/2;
            p.setExposureCompensation(exposure);
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(p);
            camera.startPreview();
        }
    }

    public void videoFlashOn(){
        Camera.Parameters p = camera.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        p.setExposureCompensation(0);
        camera.setParameters(p);
        camera.startPreview();
    }

    public void videoFlashOff(){
        Camera.Parameters p = camera.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        p.setExposureCompensation(0);
        camera.setParameters(p);
        camera.startPreview();
    }


    public void setState(int state) {
        this.state = state;
    }
}
