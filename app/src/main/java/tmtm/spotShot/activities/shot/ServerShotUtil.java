package tmtm.SpotShot.activities.shot;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.ServerUtil;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.Like;
import tmtm.SpotShot.model.Shot;
import tmtm.SpotShot.model.User;


/**CreateShotTask
 * Created by ttaila on 3/29/15.
 */
public class ServerShotUtil extends ServerUtil
{

    private static final String TAG = "ServerShotUtil";

    private Context context;
    private GetShotsTaskListener getShotsTaskListener;
    private OnShotCreatedListener onShotCreatedListener;

    public ServerShotUtil(Context context, GetShotsTaskListener getShotsTaskListener) {
        this.context = context;
        this.getShotsTaskListener = getShotsTaskListener;
    }

    public ServerShotUtil(Context context, OnShotCreatedListener onShotCreatedListener, int test) {
        this.context = context;
        this.onShotCreatedListener = onShotCreatedListener;
    }

    public ServerShotUtil(Context context) {
        this.context = context;
    }

    // Create shot db record for server side db
    public void createShot(Shot shot) {
        new CreateShotTask().execute(shot);
    }

    public void getShotsFromUser(User user) {
        new GetShotsForUserTask(user).execute();
    }

    // Get all shots from server side db within radius of location
    public void getShots(Location location, Integer radius, Integer sortBy) {
        new GetShotsTask(sortBy).execute(location, radius);
    }

    // Like shot db record for server side db
    public void likeShot(Shot shot, Like like) {
        new LikeShotTask(shot, like).execute();
    }

    // AsyncTask for creating shot db record for server side db
    private class CreateShotTask extends AsyncTask<Shot, Void, Shot> {
        Shot shot = null;
        @Override
        protected Shot doInBackground(Shot... params) {
            shot = params[0];
            boolean success = false;

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "CreateShotOperation.doInBackground() called");

                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.CREATE_SNAP_URL));

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    JSONObject jsonShot = new JSONObject();
                    jsonParam.put("shot", jsonShot);
                    jsonShot.put("user_uuid", shot.getUserUuid());
                    jsonShot.put("user_name", shot.getUsername());
                    jsonShot.put("url", shot.getUrl());
                    jsonShot.put("thumbnail_url", shot.getThumbnailUrl());
                    jsonShot.put("latitude", shot.getLocation().getLatitude());
                    jsonShot.put("longitude", shot.getLocation().getLongitude());
                    jsonShot.put("time_created", shot.getTimeCreated());
                    jsonShot.put("is_video", shot.isVideo());
                    jsonShot.put("anonymous", shot.isAnonymous());
                    jsonShot.put("score", shot.getScore());
                    jsonShot.put("shot_uuid", shot.getUuid());

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    success = jsonObject.getBoolean("success");
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            Log.i(TAG, "Creating shot record onto server db was successful = " + success);
            shot.setSuccessfullyUploaded(success);
            return shot;
        }

        @Override
        protected void onPostExecute(Shot shot) {
            List<Shot> shotList = new ArrayList<>();
            shotList.add(shot);
            SpotShotApplication.getShotsUploading().remove(shot);

            DbHelper.commitOrUpdateShots(shotList);
            if (onShotCreatedListener != null) {
                onShotCreatedListener.onShotCreatedComplete(shot);
            }

        }
    }

    // AsyncTask for creating shot db record for server side db
    private class GetShotsForUserTask extends AsyncTask<Void, Void, List<Shot>> {

        private User user;

        public GetShotsForUserTask(User user) {
            super();
            this.user = user;
        }

        @Override
        protected List<Shot> doInBackground(Void... params) {
            List<Shot> shotList = new ArrayList<>();

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "GetShotsTask.doInBackground() called");
                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.GET_SNAPS_FOR_USER_URL));
                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("user_uuid", String.valueOf(user.getUuid()));

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONArray jsonArray = new JSONArray(sb.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jboy = jsonArray.getJSONObject(i);
                        Shot s = new Shot();
                        s.setUrl(jboy.getString("url"));
                        s.setThumbnailUrl(jboy.getString("thumbnail_url"));
                        s.setUsername(jboy.getString("user_name"));
                        s.setUserUuid(jboy.getString("user_uuid"));
                        s.setUuid(jboy.getString("shot_uuid"));
                        Double latitude = jboy.getDouble("latitude");
                        Double longitude = jboy.getDouble("longitude");
                        Long timeCreated = jboy.getLong("time_created");
                        Boolean isVideo = jboy.getBoolean("is_video");
                        Integer score = jboy.getInt("score");
                        Location l = new Location("");
                        Boolean deleted = jboy.getBoolean("deleted");
                        l.setLatitude(latitude);
                        l.setLongitude(longitude);
                        s.setTimeCreated(timeCreated);
                        s.setScore(score);
                        s.setIsVideo(isVideo);
                        s.setSuccessfullyUploaded(true);
                        s.setLocation(l);
                        s.setIsDeleted(deleted);
                        shotList.add(s);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            return shotList;
        }

        @Override
        protected void onPostExecute(List<Shot> shotList) {
            if(getShotsTaskListener != null) {
                getShotsTaskListener.doOnComplete(shotList);
            }

        }
    }

    // AsyncTask for creating shot db record for server side db
    private class GetShotsTask extends AsyncTask<Object, Void, List<Shot>> {

        private int sortBy;

        public GetShotsTask(int sortBy) {
            super();
            this.sortBy = sortBy;
        }

        @Override
        protected List<Shot> doInBackground(Object... params) {
            Location location = (Location) params[0];
            int radius = (Integer) params[1];
            List<Shot> shotList = new ArrayList<>();

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "GetShotsTask.doInBackground() called");

                    String url = null;
                    if(sortBy == ShotListFragment.SORT_BY_TOP_FLAG) {
                        url = buildFullUrl(URLHelper.GET_TOP_SNAPS_URL);
                        Log.d(TAG, "Order snaps by score");
                    } else if(sortBy == ShotListFragment.SORT_BY_NEWEST_FLAG) {
                        url = buildFullUrl(URLHelper.GET_NEWEST_SNAPS_URL);
                        Log.d(TAG, "Order snaps by newest");
                    }

                    HttpURLConnection conn = setupHttpURLConnection(url);

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("latitude", String.valueOf(location.getLatitude()));
                    jsonParam.put("longitude", String.valueOf(location.getLongitude()));
                    jsonParam.put("radius", String.valueOf(radius));

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONArray jsonArray = new JSONArray(sb.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jboy = jsonArray.getJSONObject(i);
                        Shot s = new Shot();
                        s.setUrl(jboy.getString("url"));
                        s.setThumbnailUrl(jboy.getString("thumbnail_url"));
                        s.setUsername(jboy.getString("user_name"));
                        s.setUserUuid(jboy.getString("user_uuid"));
                        s.setUuid(jboy.getString("shot_uuid"));
                        Double latitude = jboy.getDouble("latitude");
                        Double longitude = jboy.getDouble("longitude");
                        Long timeCreated = jboy.getLong("time_created");
                        Boolean isVideo = jboy.getBoolean("is_video");
                        Boolean anonymous = jboy.getBoolean("anonymous");
                        Integer score = jboy.getInt("score");
                        Boolean deleted = jboy.getBoolean("deleted");
                        s.setLat(latitude);
                        s.setLng(longitude);
                        s.setTimeCreated(timeCreated);
                        s.setScore(score);
                        s.setSuccessfullyUploaded(true);
                        s.setIsVideo(isVideo);
                        s.setIsDeleted(deleted);
                        s.setAnonymous(anonymous);
                        shotList.add(s);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            return shotList;
        }

        @Override
        protected void onPostExecute(List<Shot> shotList) {
            if(getShotsTaskListener != null) {
                getShotsTaskListener.doOnComplete(shotList);
            }
        }
    }

    // AsyncTask for liking shot db record for server side db
    private class LikeShotTask extends AsyncTask<Void, Void, Boolean> {

        private Shot shot;
        private Like like;

        public LikeShotTask(Shot shot, Like like) {
            this.shot = shot;
            this.like = like;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean success = false;

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "LikeShotTask.doInBackground() called");

                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.LIKE_SNAP_URL));

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("like_uuid", like.getLikeUuid());
                    jsonParam.put("user_uuid", SpotShotApplication.getInstance().getCurrentUser().getUuid());
                    jsonParam.put("shot_uuid", shot.getUuid());

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    success = jsonObject.getBoolean("success");
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            Log.i(TAG, "Liking shot was successful = " + success);
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(!success) {
                SpotShotApplication.getInstance().removeLike(like);
            }
        }
    }

    public static void flagShotDeleted(final Shot s) {
        RequestParams params = new RequestParams();
        params.put("shot_uuid", s.getUuid());
        SpotShotRestClient.post(URLHelper.FLAG_SHOT_DELETED, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject
                try {
                    if(response.getBoolean("success")) {
                        DbHelper.removeShot(s);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // Like shot db record for server side db
    public void dislikeShot(Shot shot, Like like) {
        new DislikeShotTask(shot, like).execute();
    }

    // AsyncTask for liking shot db record for server side db
    private class DislikeShotTask extends AsyncTask<Void, Void, Boolean> {

        private Shot shot;
        private Like like;

        public DislikeShotTask(Shot shot, Like like) {
            this.shot = shot;
            this.like = like;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean success = false;

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                // fetch data
                try {
                    Log.d(TAG, "DislikeShotTask.doInBackground() called");

                    HttpURLConnection conn = setupHttpURLConnection(buildFullUrl(URLHelper.DISLIKE_SNAP_URL));

                    // Starts the query
                    conn.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("like_uuid", like.getLikeUuid());
                    jsonParam.put("user_uuid", SpotShotApplication.getInstance().getCurrentUser().getUuid());
                    jsonParam.put("shot_uuid", shot.getUuid());

                    sendJsonRequest(jsonParam, conn);

                    StringBuilder sb = getJsonResponse(conn);

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    success = jsonObject.getBoolean("success");
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                // display error
                Log.e(TAG, "Must have internet connectivity to perform this action");
            }

            Log.i(TAG, "Disliking shot was successful = " + success);
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(!success) {
                SpotShotApplication.getInstance().storeLike(like);
            }
        }
    }

}
