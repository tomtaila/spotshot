package tmtm.SpotShot.activities.settings;


import android.app.Fragment;

import tmtm.SpotShot.activities.SingleFragmentActivity;

/**
 * Created by mwszedybyl on 4/26/15.
 */
public class SettingsActivity extends SingleFragmentActivity
{
    @Override
    public Fragment createFragment()
    {
        return new SettingsFragment();
    }
}
