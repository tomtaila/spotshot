package tmtm.SpotShot.activities.settings;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.bugsnag.android.Bugsnag;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import tmtm.SpotShot.R;
import tmtm.SpotShot.Settings;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.activities.users.JumpInActivity;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.http.SpotShotRestClient;

/**
 * Created by mwszedybyl on 4/26/15.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener
{
    private static final String TAG = "SettingsFragment";

    private View rootView;
    private Button logOutButton;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //Hides keyboard at the beginning
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ContentResolver mContentResolver = getActivity().getContentResolver();
        Cursor cursor = mContentResolver.query(
                ContactsContract.RawContacts.CONTENT_URI,
                new String[]{ContactsContract.RawContacts._ID, ContactsContract.RawContacts.ACCOUNT_TYPE},
                ContactsContract.RawContacts.ACCOUNT_TYPE + " <> 'com.anddroid.contacts.sim' "
                        + " AND " + ContactsContract.RawContacts.ACCOUNT_TYPE + " <> 'com.google' " //if you don't want to google contacts also
                ,
                null,
                null);
        Cursor cur = cursor;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.settings_fragment, parent, false);
        logOutButton = (Button) rootView.findViewById(R.id.log_out_button);
        setupViews();
        return rootView;
    }

    private void setupViews(){
        logOutButton = (Button) rootView.findViewById(R.id.log_out_button);
        logOutButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == logOutButton) {
            Bugsnag.leaveBreadcrumb("Logout button clicked");
            RequestParams params = new RequestParams();
            params.put("reg_uuid", SpotShotApplication.getSharedPreferences().getString(Settings.REG_ID_KEY, ""));
            SpotShotRestClient.post(URLHelper.DELETE_DEVICE_URL, params, new JsonHttpResponseHandler());
            SpotShotApplication.clearLoginDetailsFromSharedPreferences();
            SpotShotApplication.getSharedPreferences().edit().putBoolean(Settings.IGNORE_PUSH_NOTIFICATIONS_KEY, true).commit();
            Intent i = new Intent(getActivity(), JumpInActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }


}
