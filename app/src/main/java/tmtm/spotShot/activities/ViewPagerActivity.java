package tmtm.SpotShot.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import tmtm.SpotShot.MyPagerAdapter;
import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.activities.users.UserProfileFragment;
import tmtm.SpotShot.ui.CustomViewPager;

/**
 * Created by ttaila on 4/6/15.
 */
public class ViewPagerActivity extends FragmentActivity{

    private static final String TAG = "ViewPagerActivity";
    public MyPagerAdapter pagerAdapter;
    private CustomViewPager viewPager;

    public static final int USER_FRAGMENT_POS = 0;
    public static final int SNAP_FRAGMENT_POS = 1;
    public static final int SNAP_LIST_FRAGMENT_POS = 2;

    public static int currentFragment = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()...");
        setContentView(R.layout.activity_main);

        pagerAdapter = new MyPagerAdapter(getFragmentManager());
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == SNAP_FRAGMENT_POS) {
                    Tracker tracker = SpotShotApplication.getInstance().getDefaultTracker();
                    tracker.setScreenName("Camera Screen");
                    tracker.send(new HitBuilders.ScreenViewBuilder().build());
                }
                currentFragment = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()...");
        viewPager.setCurrentItem(currentFragment);
    }

    public void changeFragment(int position) {
        viewPager.setCurrentItem(position, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()...");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()...");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }

    public void setPagingEnabled(boolean enabled) {
        viewPager.setPagingEnabled(enabled);
    }

}
