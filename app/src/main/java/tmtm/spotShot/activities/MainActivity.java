package tmtm.SpotShot.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;
import com.loopj.android.http.AsyncHttpRequest;
import com.loopj.android.http.RequestParams;

import tmtm.SpotShot.Settings;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.URLHelper;
import tmtm.SpotShot.activities.users.JumpInActivity;
import tmtm.SpotShot.gcm.GCMClientManager;
import tmtm.SpotShot.database.DbHelper;
import tmtm.SpotShot.gcm.GCMUtil;
import tmtm.SpotShot.http.SpotShotRestClient;
import tmtm.SpotShot.model.User;

/**
 * Created by mwszedybyl on 4/26/15.
 */
public class MainActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SpotShotApplication.getSharedPreferences().edit().putInt("pageNum", 0).commit();

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }
        else
        {
            if(SpotShotApplication.loggedIn()){
                String name = SpotShotApplication.getSharedPreferences().getString(Settings.USER_NAME_KEY,"");
                if(!name.equals(""))
                {
                    Bugsnag.leaveBreadcrumb("Already logged in as: " + name);
                    new GetUserTask().execute(name);
                }
            } else {
                Intent i = new Intent(getBaseContext(), JumpInActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        } 
    }

    private class GetUserTask extends AsyncTask<String, Void, User>
    {
        User user;

        @Override
        protected User doInBackground(String... params) {
            String name = params[0];
            return DbHelper.getUser(name);
        }

        @Override
        protected void onPostExecute(User user) {
            Log.d("hey user", "user  = " + user);
            GCMUtil.registerOrUpdateGCM(MainActivity.this, user);
            SpotShotApplication.setCurrentUser(user);
            SpotShotApplication.getInstance().pullCurrentUserLikes();
            Intent i = new Intent(getBaseContext(), SplashScreenActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }

}
