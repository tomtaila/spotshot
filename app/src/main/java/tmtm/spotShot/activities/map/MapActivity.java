package tmtm.SpotShot.activities.map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.bugsnag.android.Bugsnag;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melnykov.fab.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import tmtm.SpotShot.R;
import tmtm.SpotShot.SpotShotApplication;
import tmtm.SpotShot.TimeUtil;
import tmtm.SpotShot.activities.LocationHelper;
import tmtm.SpotShot.activities.shot.DisplayMediaActivity;
import tmtm.SpotShot.model.Shot;

/**
 * Created by mwszedybyl on 3/25/15.
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback
{
    private static final String TAG = "MapActivity";
    private List<Shot> shotList;
    private FloatingActionButton listViewFab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.map_activity);
        shotList = new ArrayList<>();
        shotList.addAll(SpotShotApplication.getAllShotsFromDatabase(0));

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setupViews();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onMapReady(GoogleMap map)
    {
        map.setMyLocationEnabled(true);
        LatLng currentLatLng = new LatLng(LocationHelper.getLastLocation().getLatitude(), LocationHelper.getLastLocation().getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12));
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Shot shot;
                Log.d(TAG, "marker clicked");
                for (Shot s : shotList) {
                    if (s.getFullUrl().equals(marker.getSnippet())) {
                        Log.d(TAG, "shot found");
                        shot = s;
                        Intent i = new Intent(getBaseContext(), DisplayMediaActivity.class);
                        String timeAgo = TimeUtil.generateTimeAgoString(shot.getTimeCreated());
                        float distanceInMeters = LocationHelper.getLastLocation().distanceTo(shot.getLocation());
                        double distanceInMiles = distanceInMeters/1609.34;
                        i.putExtra("mediaUrl", shot.getUrl());
                        i.putExtra("thumbnailUrl", shot.getThumbnailUrl());
                        i.putExtra("username", ""+ shot.getUsername());
                        i.putExtra("anonymous", shot.isAnonymous());
                        i.putExtra("lat", shot.getLocation().getLatitude());
                        i.putExtra("long", shot.getLocation().getLongitude());

                        i.putExtra("distance", String.format("%.2f", distanceInMiles));
                        i.putExtra("timeElapsed", timeAgo);
                        startActivity(i);
                    }
                }
                return true;
            }
        });

        for(Shot s : shotList){
            LatLng location = new LatLng(s.getLocation().getLatitude(), s.getLocation().getLongitude());
            Marker marker = map.addMarker(new MarkerOptions().position(location).snippet(s.getFullUrl()));

            File thumbnailFile = new File(SpotShotApplication.APP_ROOT_MEDIA_PATH, s.getThumbnailUrl());
            Bitmap bitmap = null;
            if(thumbnailFile.exists()) {
                try {
                    FileInputStream fos = new FileInputStream(thumbnailFile);
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(thumbnailFile));
                    fos.close();
                    marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
                } catch (Exception e) {
                    Log.e(TAG, "Exception thrown whilst setting thumbnail markers", e);
                }
            }

        }
    }

    private void setupViews() {
        listViewFab = (FloatingActionButton) findViewById(R.id.list_view_fab);
        listViewFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Takes you back to previous activity (identical to using back button)
                Bugsnag.leaveBreadcrumb("List view button clicked");
                finish();
            }
        });
    }
}
