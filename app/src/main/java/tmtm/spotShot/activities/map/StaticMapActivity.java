package tmtm.SpotShot.activities.map;

import android.app.Fragment;

import tmtm.SpotShot.activities.SingleFragmentActivity;
import tmtm.SpotShot.activities.shot.StaticMapShotFragment;

/**
 * Created by ttaila on 6/16/15.
 */
public class StaticMapActivity extends SingleFragmentActivity {
    @Override
    public Fragment createFragment() {
        return new StaticMapShotFragment();
    }
}
