package tmtm.SpotShot;

import android.content.Context;

/**
 * Created by ttaila on 3/10/15.
 */
public class MathUtil {

    public static int metresToMiles(double metres) {
        return (int) (metres/Constants.METRES_IN_A_MILE);
    }

    public static int metresToKm(double metres) {
        return (int) (metres/Constants.METRES_IN_A_KM);
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

}
