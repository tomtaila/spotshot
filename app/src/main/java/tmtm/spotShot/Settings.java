package tmtm.SpotShot;

/**
 * Created by ttaila on 4/18/15.
 */
public class Settings {

    public static final String FILE_NAME = "settings";

    public static final String LOGGED_IN_KEY = "loggedIn";
    public static final String USER_NAME_KEY = "name";
    public static final String USER_PASSWORD_KEY = "password";
    public static final String APP_VERSION_KEY = "appVersion";
    public static final String REG_ID_KEY = "registration_id";
    public static final String IGNORE_PUSH_NOTIFICATIONS_KEY = "ignorePush";
    public static final String SHOT0 = "shot0";
    public static final String SHOT1 = "shot1";
    public static final String SHOT2 = "shot2";
    public static final String SHOT3 = "shot3";
    public static final String SHOT4 = "shot4";
    public static final String SHOT5 = "shot5";
    public static final String SHOT6 = "shot6";
    public static final String SHOT7 = "shot7";
    public static final String SHOT8 = "shot8";
    public static final String SHOT9 = "shot9";
    public static final String SHOT_POSITION = "shot_position";

}
